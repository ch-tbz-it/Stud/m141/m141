![](../x_res/tbz_logo.png)√

# M141 - DB-Systeme in Betrieb nehmen (1.Tag)

*Autoren: Gerd Gesell, Kellenberger Michael 2025*

![](../x_res/Cartoon.png)

**Inhalt**

[TOC]

# Einführung (z.T. Repetition des Moduls M164)

![](../x_res/Learn.png)

## Was ist eine Datenbank ?

Im üblichen Sprachgebrauch wird der Begriff Datenbank oft verwendet für die eigentlichen Daten, die Datenbankdateien, das Datenbanksystem oder den Datenbank-Klient. Entsprechend gross kann die Verwirrung sein, wenn sich zwei Personen über Datenbanken unterhalten. Hier die korrekten Begriffe:

![](../x_res/Aufbau.png)

Abb. 1: Aufbau von Datenbanksystemen (Server & Klienten)

Aufgabe eines DB-Systems ist nicht nur die sichere Speicherung der Daten, sondern auch die Verarbeitung von Befehlen zur Abfrage, Sortierung und Analyse vorhandener Daten, zum Eingeben, Ändern und Löschen von Daten.

-   **Datenbank** (DB) strukturierte Sammlung von Daten
-   **DBS**: Datenbank System oder DB-Server: Logische Einheit zur Verwaltung von Daten
-   **DBMS**: Database Management System, Programm für die Verwaltung der Datenbank(en), DB-Engine
-   **Datenbasis**: Die eigentlichen Daten

## DB-Server und DB-Client

Einfache Datenbanksysteme laufen standalone lokal auf einem Rechner für einen Benutzer. Andere werden gleichzeitig von Tausenden von Benutzern beansprucht und verteilen die Daten über mehrere Rechner und Festplatten und genügen zum Thema Sicherheit und Verfügbarkeit hohen Ansprüchen. Die Rechner, die Dienstleistungen eines Datenbanksystems zur Verfügung stellen, werden Datenbank-Server genannt. Die Programme, die mit dem Datenbanksystem in Verbindung stehen, werden Datenbank-Klient genannt.

Der Datenbank-Klient ist für den Anwender die Schnittstelle zum Datenbankserver. Die Klient-Software kann vom einfachen Konsolenfenster bis zum komfortablen Dialogprogramm mit Formularen, Eingabehilfen und grafischer Unterstützung reichen, um nach Daten zu suchen, neue Daten einzugeben oder Systemparameter zu verwalten und zu kontrollieren.

![](../x_res/Architekturen.png)

Abb. 2: DB-Systemarchitekturen

In professionellen Applikationsumgebungen steht nicht nur ein Datenbankserver zur Verfügung, sondern werden mehrere spezialisierte (dedicated) Servertypen verwendet: Webserver, Netzwerkserver, Applikationsserver usw..

Aufteilung der Aufgaben in der Client/Server-Architektur (*verteilte Intelligenz*):

-   Client (Frontend): am Arbeitsplatz, für anwendernahe Dialog- und Präsentationsaufgaben
-   Server (Backend): zentral, für gemeinsame Dienste, z.B. DB-Dienste

## Datenbank-Modelle (DB-Engines)


Es gibt grundsätzlich verschiedene Ansätze oder Arten, um eine Datenstruktur aufzubauen:. 

| DB Art            | Struktur / Beispiel                                                                    |
|-----------------------------|----------------------------------------------------------------------------------------------------------------|
| **Hierarchische** DB            | hierarchische Baumstruktur, z.B. IMS (IBM)                                                                     |
| **Netzwerk**-DB                 | netzwerkförmige Struktur; z.B. IDMS (General Electric)                                                         |
| **Relationale** DB <br> (Abk. **RDBMS**)        | DB-Typ mit Tabellen und Beziehungen, z.B. Oracle, DB2, MySQL, MariaDB ; SQL-Server, Access, Ingres, PostgreSQL |
| **Objektorientierte** DB <br> (Abk. OODB) | enthält Informationen direkt in Form von Objekten, z.B. Object-Store, O2, Caché                                |
| **XML**-Datenbank               | speziell für XML-Dokumente, z.B. Tamino                                                                        |
| **NoSQL**  <br> (Not only SQL)    | siehe unten                                                                                                     |

Tab. 1.1: Datenbankarten

Es ist noch offen, wie sich der Markt für nichtrelationale Datenbanken entwickeln wird. Aber seit einigen Jahren gibt es eine ganze Reihe an Produkten unter der Überschrift **NoSQL** entstanden, die sich in Bezug auf ihre Speichertechnologie in 4 Kategorien einteilen lassen:

-   **Dokumentenorientierte** Datenbanken (z.B. MongoDB, CouchDB, Cassandra)
-   **Graf**datenbanken (z.B. Neo4j)
-   **Big-Table**-Datenbanken (z.B. Doug Judd von Hypertable Inc., Big Table von Google)
-   **Key-Value**-Datenbanken (z.B. Redis, Dynamo, Codmos)

Das heute am häufigsten verwendete Datenbankmodell ist die relationale Datenbank. Rein objektorientierte Datenbanksysteme sind immer noch die Ausnahme. Allerdings bemühen sich die meisten Anbieter relationaler Datenbanksysteme darum, die OO-Welt in der relationalen Umgebung einzubetten. Das Ergebnis sind dann sogenannte objektrelationale Datenbanken. Eine gute Übersicht, was aktuell auf dem Markt passiert, finden Sie hier: <https://db-engines.com/de/ranking> - interessant ist auch die Methode, die zur Statistik führt. Diese ist ebenfalls auf der Seite beschrieben.

---

## MySQL oder MariaDB oder andere? 


### Entstehung MariaDB und MySQL

Um den Unterschied zwischen den beiden relationalen DB-Systemen MariaDB und MySQL zu verstehen, sind ein paar Kenntnisse hilfreich. Wer sich für detaillierte Entwicklungsschritte interessiert, kann dies zum Beispiel unter <https://de.wikipedia.org/wiki/MySQL> nachlesen. Seit einigen Jahren läuft übrigens auch Wikipedia unter MariaDB und nicht mehr unter MySQL.

Entwicklung von **MySQL** ca. ab 1994 unter Michael Widenius. Im Februar 2008 wird es dann von Sun Microsystems übernommen (Kaufpreis ca. 1 Mrd. Dollar). Anfang 2010 wird Sun dann von Oracle aufgekauft. Logischerweise ist die Open-Source-Entwicklerszene davon nicht riesig begeistert. Michael Widenius beginnt bereits kurz vorher, einen **Fork** von MySQL Version 5.5 mit dem Namen **MariaDB** zu starten und unter der **GPL** zu entwickeln.

Zu Beginn haben sich die beiden Systeme nicht wesentlich in der Entwicklung unterschieden. In letzter Zeit ist MariaDB jedoch in vielen Bereichen vorangezogen. Einen gut lesbaren Bericht zu den Neuentwicklungen findet ihr unter anderem hier: <https://www.informatik-aktuell.de/betrieb/datenbanken/mariadb-und-mysql-vergleich-der-features.html>. °


## MariaDB 

Für dieses Modul ist der Unterschied zwischen MariaDB und MySQL unwesentlich. Erst für ein grösseres Projekt sind die Software-Architektur, Hochverfügbarkeit und Leistungsoptimierung (und Kosten) dann relevant. 

Auch in den aktuellen Installationen von XAMPP (siehe weiter unten) werden die Bezeichnungen beider DBMS recht locker parallel verwendet. Schon beim Installationsprozess gibt es eine Checkbox `MySQL` und gemeint ist MariaDB. Auch in Dokumentationen, Anleitungen und im XAMPP-Controlpanel werden diese beiden Namen nicht immer klar getrennt. Für den Beginner ist es da fast wichtiger, auf eine lauffähige PHP-Version zu achten. 

Wir können MySQL als eigenständigen Server installieren: z.B. Oracle's [MySQL](https://www.mysql.com/)  <br> (![](../x_res/caution.png) Was wir im Moment aber noch nicht tun ...)

---

# DBMS Erhebung

![](../x_res/Train_R1.png)

Im folgenden wollen wir uns mit den gängigen MySQL Datenbanken auseinandersetzen ...

> Führen Sie die zwei Aufträge so aus, wie wenn sie benotet werden würden! <br> Zeitbedarf ca. 1h.

## Relationale DBMS: Vergleich MySQL vs MariaDB vs PostgreSQL

Nutzen Sie dazu die Vergleichsfunktion von <https://db-engines.com/de/systems/>, indem Sie mehrere DBMS anwählen:

![](../x_res/Comparison.png)

![](../x_res/ToDo.png) Erstellen Sie in Ihrem **Lernportfolio** eine Kurzfassung mit den wichtigsten Merkmalen, Vor- und Nachteilen. Was sind die typ. Anwendungsfälle?

| Produkt|  MariaDB |  MySQL (Oracle) oder MS-SQL | PostgreSQL |
|---|---|---|---|
| Merkmale ... | | | |
| Vorteile ... | | | |
| Nachteile ... | | | |
| Anwendungsfälle | | | |
| ... | | | |


## Andere DBMS

![](../x_res/ToDo.png) Oben wurden neben den RDBMS weitere DBMS-Modelle erwähnt.
Erstellen Sie in Ihrem **Lernportfolio** eine weitere Zusammenstellung je eines typ. Vertreters (DBMS) mit den wichtigsten Merkmalen, Vor- und Nachteilen. Was sind die typ. Anwendungsgebiete? Ergänzen Sie mit weiteren Infos ...

Benutzen Sie die Website <https://db-engines.com/de/systems/> für diese Aufgaben. (Die Website ist auch in Englisch abrufbar!)

| Datenbankmodell <br> (mit Erklärung zum Modell) | Produkt(e) | Vor-/Nachteile | typ. Anwendungen | Bild, Bsp, Link., etc |
|---|---|---|---|---|
| **Document** <br> (...) | | | | |
| **Key-Value** <br> (...) |  | | | |
| **Wide column** <br> (...) | | | | |
| **Search** <br> (...) | | | | |
| **Graph** <br> (...) | | | | |
| **Time Series** <br> (...)|  | | | |
| **Spatial** <br> (...) | | | | |
| ... | | | | |

![Video](../x_res/Video.png)[Exkurs: More Infos (engl.)](https://www.youtube.com/watch?v=VfcRxtBKI54)

---

# XAMPP Arbeitsumgebung installieren

![](../x_res/Train_D1.png)

Auch wenn Sie im Modul 164 eine andere MySQL-Variante installiert haben, sollten Sie für dieses Modul **XAMPP** (notfalls in einer VM) installieren und die **andere Variante stilllegen**! 

![](../x_res/ToDo.png) Installationsanleitung [XAMPP](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/1.Tag/XAMPP) (Sprich 'Schamp')

*Hinweis: Auch wenn Sie folgende Schritte im Modul 162/164 schon gemacht haben, lesen Sie sie trotzdem durch. Es gibt einige vertiefte Infos ... !*

> ![](../x_res/caution.png) **Achtung:** XAMPP ist zur Entwicklung konstruiert. Daher sind die Sicherheitsvorkehrungen bewusst ausgeschaltet, um bei der Entwicklung und beim Testen (gute Informatiker machen das!) ohne zusätzliche Hürden arbeiten zu können. Ein produktiver Server sollte niemals mit dem XAMPP-Bundle aufgesetzt werden. Wir werden den Server später noch für den produktiven Betrieb anpassen (siehe Tag 6).
> 
> Im Juli 2011 wurde das Observationssystem „PATRAS“ der deutschen Bundespolizei vom Hacker-Netzwerk „no name crew“ (kurz „nn-Crew) gehackt. Ziemlich peinlich, denn dieses System enthielt sensible Daten und wurde mittels eines XAMPP-Bundles auf einem Server betrieben.

![](../x_res/ToDo.png) Sie benötigen auch die funktionierende **MySQL-Workbench** aus den Modulen 162/164 und **phpMyAdmin** (letzteres ist im XAMPP-Bundle bereits enthalten).

---

## Der Datenbankserver-Daemon mysqld.exe

Hinter dieser Datei verbirgt sich das Datenbankmanagementsystem. Der Server muss laufen, damit Sie von irgendeinem Klient mit der Datenbank kommunizieren können – sei es als Prozess oder als Service. Suchen Sie die Datei im Verzeichnis `C:\xampp\mysql\bin`. 


## Start via cmd – Command Line Tool

Der Server kann direkt von der Betriebssystem-Konsole gestartet werden. Mit der Eingabe: [siehe hier](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/1.Tag/XAMPP#xampp-mysql-server-starten-via-command-line-tool)!

![](../x_res/Wichtig.png) Merken Sie sich den Befehl für später: 

`c:\xampp\mysql\bin\mysqld --skip-grant-tables`

## XAMPP MySQL-Server starten via Control Panel

Der Server kann mit dem Control Panel von XAMPP gestartet werden:  [siehe hier!](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/1.Tag/XAMPP#xampp-mysql-server-starten-via-control-panel)


## Kommandozeilen-Klient mysql.exe  (Repetition von Modul 164)

Für den Zugriff auf den Datenbankserver gibt es viele Klient-Programme (siehe Abb.1). Das einfachste Klient-Programm ist die Kommandozeilen-Applikation *mysql.exe*. Dabei handelt es sich um ein einfaches Konsolenfenster, in dem SQL-Befehle und verschiedene Kommandos abgesetzt werden können. Diese rudimentäre Methode wird durch eine Vielzahl an grafischen Tools vereinfacht. Für das Verständnis der grafischen Tools sollten Sie zu Beginn die Übungen mit dem Konsolenfenster durchführen. Die Grafiktools machen ja nichts anderes, als SQL-Kommandos aus einer animierten Umgebung abzusetzen und deren Resultate optisch ansprechend darzustellen.

![](../x_res/cmd.png)

Abb. 4: cmd Konsolenfenster

![](../x_res/PowerShell.png)

Abb. 5: PowerShell als Commandline-Tool

Zum Start des MySQL-Konsolen-Klients führen Sie das Programm mysql.exe von der Betriebssystem-Konsole oder irgendeinem anderen Commandline-Tool aus. Dazu öffnen wir zum Beispiel das Commandline-Fenster «cmd» des Betriebssystems. In den Abbildungen sehen Sie, wie Sie in das richtige Verzeichnis navigieren können und dort das Programm aufrufen. Wenn Sie nur MySQL ohne weitere Parameter aufrufen, starten Sie den Klient mit stark eingeschränkten Rechten (Gastrechte) und sehen nur wenige Datenbanken. Wenn Sie die Parameter

-   `–u `(steht für User – also zu Beginn mal als `root` anmelden) und
-   `–p` (steht für Passwort – ist beim User `root` standardmässig leer – also `enter` drücken oder `-p` weglassen)

hinzufügen, haben Sie mehr Rechte und sehen alle vorhandenen Datenbanken. Verwenden Sie für den Test das SQL-Statement `SHOW DATABASES;`

Ergebnisse von SQL-Statements, die einen grösseren Output generieren, sind im Konsolenfenster nicht sehr übersichtlich dargestellt und es gibt auch nicht viele Formatierungsmöglichkeiten – es ist ja kein GUI. Hilfreich ist in dem Zusammenhang der Modifier `\G`. Ersetzen Sie das Semikolon `;` am Ende eines Statements mit diesem Modifier und die Ausgabe ist deutlich übersichtlicher formatiert bei breiten Tabellen (vertikal statt horizontal).

Diese Graphik-Tools werden laufend weiterentwickelt und es kommen immer wieder neue Produkte auf den Markt. In den letzten Jahren hatten sich die MySQL GUI Tools als zuverlässige Werkzeuge etabliert und sind vor einiger Zeit in der MySQL-Workbench zusammengefasst worden. Im Kern verwenden diese Tools natürlich auch nur die SQL-Statements und .exe-Dateien, die wir genauso gut manuell im Konsolenfenster aufrufen können.

## MySQL-Workbench (Repetition von Modul 164)

Die MySQL Workbench hat sich in den letzten Jahren als die zentrale Plattform durchgesetzt, die viele 'alte' Tools vereint. Sie beinhaltet alle Möglichkeiten vom grafisch unterstützten Entwurf, über die DB-Erstellung und die Serveradministration. Für die Arbeit in diesem Modul ist die Workbench optional. Die Erklärungen in diesem Skript möchte ich aber so gering wie möglich halten. Denn es gibt eine sehr gute Dokumentation im Internet: <http://www.mysql.de/products/workbench/> und ausserdem wechseln Benutzeroberflächen bei jedem neuen Release ihr Erscheinungsbild. Online-Dokumentationen sind daher viel zuverlässiger. Je nach Installation und Betriebssystem gibt es zusätzlich Unterschiede. Also nicht verwirren lassen, sondern ausprobieren, Manuals und Foren verwenden und immer mal wieder zu den Nachbarn schauen.

![](../x_res/Workbench.png)

Abb. 6a: Startfenster der Workbench Version 6.x CE

Die Möglichkeiten zur „Server Administration“ werden wir näher betrachten. Bereits kennengelernt haben wir im M164  die Möglichkeiten der Datenmodellierung. Sie sind aber immer wieder relevant, um in der Workbench ein Datenmodell grafisch zu erstellen und dieses Modell via *forward-engineering*, *backward-engineering* oder *synchronization* mit der realen Datenbank abzugleichen. (siehe Modul 164!)

> ![](../x_res/caution.png) Nur wenn der DB-Server als Service läuft, kann er via MySQL-Workbench gestartet und gestoppt werden. siehe: MySQL als Service installieren

![](../x_res/Workbench_Server_Start_Stop.png)


Von der Workbench aus können Sie auch direkt den Konsolen-Klienten starten:

![](../x_res/Workbench_to_MySQL.png)


## phpMyAdmin

Ein Tool mit sehr hoher Verbreitung ist phpMyAdmin. Die Bezeichnung `php` im Namen bedeutet nicht, dass wir damit PHP administrieren, sondern lediglich, dass dieses Tool in PHP realisiert wurde. Der häufigste Einsatz ist die Erstellung und Verwaltung von Datenbanken. Viele Web-Provider, die Webspace inklusive MySQL-Datenbank zur Verfügung stellen, bieten ihren Kunden dieses Tool zur Datenbankverwaltung an. Daher ist es trotz der Workbench sinnvoll, von der phpMyAdmin-Oberfläche arbeiten zu können.

![phpMyAdmin_Start](../x_res/phpMyAdmin_Start.png)

Abb. 7a:  phpMyAdmin starten

So sind auch im phpMyAdmin umfangreiche Serverüberwachungsfunktionen enthalten (siehe eingekreiste Funktionen in Abb. 7: Serverfunktionen von phpMyAdmin). Mit welchen Tools und Komponenten Sie am Ende arbeiten, hängt von Ihren Berechtigungen und Ihrer Rolle ab. Je nach Rechtesituation sind bestimmte Funktionen deaktiviert. Sie können zum Beispiel bei den meisten Webprovidern mit fertig installierter Datenbank die DB Namen nicht selber definieren.

![](../x_res/phpMyAdmin.png)

Abb. 7b: Serverfunktionen von phpMyAdmin

Die Erstellung der Datenbanken und Tabellen und deren Pflege sind im phpMyAdmin ohne Kenntnis von SQL möglich. Es können auch Daten erfasst und modifiziert werden (Funktion „Einfügen“ – wenn eine konkrete Tabelle ausgewählt wurde). In der Praxis wird das aber meistens über spezielle Applikationen gemacht.

![](../x_res/Datenbankpflege.png)

Abb. 8: Funktionen zur Datenbankpflege

Arbeiten Sie jetzt mit **Workbench** und/oder **phpMyAdmin**, um das System kennen zu lernen. Nutzen Sie dabei auch die Online-Hilfsmöglichkeiten!

---

## MySQL Dokumentation

Die Dokumentation von MySQL und MariaDB ist umfangreich, verständlich und teilweise auch auf Deutsch vorhanden. Online finden Sie die Informationen unter: <https://www.mysql.com/> und <https://mariadb.com>.


### DB-Server Manuals
[MariaDB Server Documentation](https://mariadb.com/kb/en/documentation/)

[MySQL Server Documentation](https://dev.mysql.com/doc/)

### phpMyAdmin Manual & Tutorials:
Die Erstellung der Datenbanken und Tabellen und deren Pflege sind im phpMyAdmin ohne Kenntnis von SQL möglich. Es können auch Daten erfasst und modifiziert werden (Funktion „Einfügen“ – wenn eine konkrete Tabelle ausgewählt wurde).

[phpMyAdmin Dokumentation](https://docs.phpmyadmin.net/de/latest/)

[Youtube phpMyAdmin Tour and Beginner's Guide](https://www.youtube.com/watch?v=XEGxz1Undgw)
 

---

## Server-Status abfragen

Als Administrator ist immer der erste Blick auf den Status des Servers!
Im **mysql**-Klient kann dies über das SQL-Kommando `Status;` erfolgen:

![](../x_res/status.png)

Abb. 14: Der Befehl "status" im mysql.exe - Konsolenfenster

| Status-Informationen |                                              |
|----------------------|----------------------------------------------|
| Current database     | Aktuell gewählte Datenbank, z.B. mybooks     |
| Current user         | User- und Hostname (admin: root@localhost)   |
| Server Version       | Version der Server-Software,                 |
| Server characterset  | Zeichensatz des DB-Servers, z.B. utf8mb4     |
| TCP port             | vom Server verwendetes Port (Default: 3306)  |
| Uptime               | Betriebszeit seit dem letzten Serverstart    |

| Server-Informationen |                                                          |
|----------------------|----------------------------------------------------------|
| Host Name            | Name des Server-Rechners,                                |
| OS Plattform         | Betriebssystem des Server-Rechners, z.B. Windows NT      |
| Local IP Address     | Netzwerkadresse des Server-Rechners, z.B. 127.0.0.1      |
| Server Info          | Version des Servers mysqld, z.B. 4.11.10                 |


![](../x_res/ToDo.png) Untersuchen Sie die Statusanzeigen in den anderen beiden Klienten! Setzen Sie auch dort den SQL-Befehl `Status` ab.




---

# ![](../x_res/CP.png) Checkpoint

[Checkpoint](./1T_CheckPoint.md)

---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)