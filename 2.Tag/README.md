![](../x_res/tbz_logo.png)√

# M141 - DB-Systeme in Betrieb nehmen  (2.Tag)

*Autoren: Gerd Gesell, Kellenberger Michael 2025*

**Inhalt**

[TOC]

> ([Lösung 1.Tag](../Loesungen/1T_CP_Loes.md))


# Die Optionsdateien my.ini (oder my.cnf)

![](../x_res/Learn.png)

Beim Start des MySQL Servers (oder Klienten), werden diverse Verzeichnisse nach Optionseinstellungen in einer Datei `my.ini` (oder die Datei *my.cnf* bei Linux/Unix) durchsucht:

| Optionsdateien Suchreihenfolge  (WINDOWS) | Zweck                                                                                                                                                                                          |
|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `System Windows Directory\my.ini` | Global |
| `Windows Directory\my.ini` | 	Global | 
| `C:\my.ini`	 | Global | 
| `INSTALLDIR\my.ini`	 | Server | 
| `INSTALLDIR\data\my.ini`	 | Server | 
| `%MARIADB_HOME%\my.ini` | 	Server (from MariaDB 10.6) | 
| `%MYSQL_HOME%\my.ini`	 | Server
| defaults-extra-file	 | File specified with --defaults-extra-file, if any

- *Quelle*: <https://mariadb.com/kb/en/configuring-mariadb-with-option-files/> <br> 
  *Siehe auch* <https://dev.mysql.com/doc/refman/8.0/en/option-files.html>

*Hinweis:  Bei Installation XAMPP MySQL-DBMS: `INSTALLDIR` zeigt auf `C:\xampp\mysql\`, `%MARIADB_HOME%` zeigt auf `C:\xampp\mysql\bin`

Wer wissen möchte, welche Optionsdateien in seinem System gefunden und verwendet werden, kann im CMD-Fenster Folgendes eingeben: 
 
`C:\> mysqld --verbose --help`

(im richtigen Verzeichnis natürlich) und dann ganz schnell lesen! Gleiches geht auch für andere MySQL-Programme (z.B. `mysql.exe`). Beim Einlesen der `.ini`-Dateien werden dann die `[Abschnitte]` ausgegeben, die ausgeführt werden, denn nicht alle Programme benötigen die gesamte Optionsdatei.

Bei fehlenden `.ini`-Dateien wird keine Fehlermeldung ausgegeben. Mind. eine Optionsdatei muss es aber geben ...

![](../x_res/Train_R1.png)

![2Do](../x_res/ToDo.png) Untersuchen Sie Ihr System und finden Sie heraus, welche `.ini`-Dateien nach der Installation tatsächlich existieren! 

- Legen Sie dazu eine Kopie/Screenshot (*die ca. ersten 25 Zeilen sind interessant*) des angezeigten Startprozesses (`verbose`) für `mysql.exe` und `mysqld.exe` in ihr Lernportfolio. 
- Lesen Sie den Kommentar in den Optionsdateien, die sie gefunden haben.
- Listen Sie die `[Abschnitte]` aus einer `.ini`-Datei heraus und begutachten Sie die Inhalte. Wir werden Einträge darin bearbeiten...

<br><br><br>

---

# Daten importieren (Repetition und Vertiefung)

![](../x_res/Learn.png)

Beim Einlesen von Daten in eine Datenbasis aus verschiedenen (Datei-)Quellen, müssen wir einige Dinge wissen und entsprechend Vorbereitungen treffen oder Anpassungen machen. Drei Dinge schauen wir uns jetzt an ...


## Zeichenkodierung (Siehe M114)

Im Modul 114 haben wir (oder werden wir noch!) Zeichenkodierungen kennengelernt, darum hier nur eine Zusammenfassung. Zeichensatzkodierungen sind Tabellen, die bestimmten Byte-Werten konkrete Zeichen zuordnen:

-   **ASCII** (Veraltet, American Standard Code for Information Interchange): <br>   Ist 7-Bit codiert, hat ca. 32 Steuerzeichen und 96 druckbare Zeichen, ist auf die englische Sprache ausgerichtet.  
-   **ANSI und weitere ISO-8859 Codepages**: Der Zeichensatz [ISO-8859-1](https://de.wikipedia.org/wiki/ISO_8859-1) (**Latin1**) ist der erste von insgesamt 16 ländereigenen Zeichensätzen und ist für Westeuropa gemacht, beinhaltet also z.B. deutsche, französische oder skandinavische Sonderzeichen. ANSI deckt (fast) alle ASCII Zeichen (mit demselben Code) ab und codiert, mit dem höchstwertigen Bit (8.Bit) gesetzt (>=80h), weitere 96 internationale Zeichen via wählbare Codepages.
-   **Unicode** (Universal Character Set, UCS): Entstanden Ende der 80er Jahre mit dem Ziel, alle Sprachen der Welt in einem Zeichensatz zu vereinen, ist der Unicode der grösste und umfassendste Zeichensatz. Durch die 32-Bit-Speichergrösse pro Zeichen kann Unicode ca. 100'000 verschiedene Zeichen einem eindeutigen Code zuordnen. 
- **UTF-8** (Standard, UCS Transformation Format 8-Bit): UTF-8 ist die häufigste verwendete Kodierung für Unicode-Zeichen mit hohem ASCII-Anteil – die ersten 128 Zeichen entsprechen der ASCII-Tabelle. Aktuell sind über 95% aller Websites mit UTF-8 codiert. Siehe [History](https://w3techs.com/technologies/history_overview/character_encoding) <br> Der Vorteil von UTF-8 liegt in der [angepassten Speichergrösse](https://de.wikipedia.org/wiki/UTF-8#Algorithmus) eines einzelnen Zeichens, das von einem Byte (ASCII) bis zu vier Bytes reicht (Emoji): in 8-Bit Sprüngen.
- **UTF-16, UTF-32**: Gegenüber UTF-8 ist die angepasste Speichergrösse bei UTF-16 16 Bit (&#8594; Java), und bei UTF-32 fix 32 Bit, was einen grossen Speicherbedarf bei den häufigsten Zeichen (ASCII) bedeutet!


### Byte Order Mark - BOM

Wenn ein Zeichen mit mehr als einem Byte codiert werden muss, spielt die Byte-Reihenfolge eine Rolle: [Big- oder Little-Endian](https://de.wikipedia.org/wiki/Byte-Reihenfolge). Bsp: '`ä`' &#8594; `C3A4` (=Big-Endian) oder `A4C3` (=Little-Endian)?

Beim Einsatz von UTF-16 oder UTF-32 *MUSS* die Reihenfolge der Bytes angegeben werden. Dazu gibt es ein "BOM" ([Byte Order Mark](https://de.wikipedia.org/wiki/Byte_Order_Mark)) ganz am Anfang der Datei. Das BOM besteht aus der Bytesequenz  `FE FF` (=Big-Endian) oder `FF FE` (=Little-Endian).

Bei UTF-8 *KANN* das BOM `EF BB BF` eingesetzt werden. Es ändert die Bytefolge aber NICHT. Einige ältere Texteditoren und MySQL interpretieren leider diese Zeichenfolge meist als ISO-8859-Zeichen `ï»¿` (je nach Codepage!) und sind für Kompatibilitätsprobleme verantwortlich. Es ist daher ratsam, das BOM bei UTF-8 zu entfernen!

Gemeinerweise ist diese Markierung in normalen Texteditoren nicht sichtbar, da den Editoren bekannt ist, dass es sich nur um Metadaten handelt. Erst wenn Sie die Datei mit einem Hex-Editor anschauen, erkennen Sie den Unterschied zwischen den Dateien mit und ohne BOM.

![](../x_res/oBOM.png)

Abb. 9: Datei ohne BOM im Notepad++ Hex-View

![](../x_res/mBOM.png)

Abb. 10: Datei mit BOM im Notepad++ Hex-View

Einfache Editoren erlauben keine detaillierten Angaben zur Kodierung bei der Speicherung. Beim Notepad++ finden wir die nötigen Einstellungen im Ordner „Kodierung“. Wir müssen die Dateien für unsere Anwendung ohne BOM abspeichern.

![UTF-8 ohne BOM in Notepad++](../x_res/oBOM_NP.png)

Abb. 11: Datei ohne BOM im Notepad++: Kodierungseinstellungen für die Speicherung


## Sortierfolgen im Allgemeinen

(entnommen dem MySQL-Handbuch – Version 5.1)

Eine *Sortierfolge* ist ein Regelsatz für den Vergleich von Zeichen in einem Zeichensatz. Folgendes Beispiel, welches einen imaginären Zeichensatz verwendet, soll den Unterschied verdeutlichen.

Angenommen, wir haben ein Alphabet mit vier Buchstaben: ‘**A**’, ‘**B**’, ‘**a**’, ‘**b**’. Jedem Buchstaben weisen wir nun eine Zahl zu: ‘**A**’ = 0, ‘**B**’ = 1, ‘**a**’ = 2, ‘**b**’ = 3. Der Buchstabe ‘**A**’ ist ein Symbol, die Zahl 0 die **Kodierung** für ‘**A**’. Die Kombination aller vier Buchstaben und ihrer Kodierungen bildet den **Zeichensatz**.

Angenommen, wir wollen zwei String-Werte ‘**A**’ und ‘**B**’ miteinander vergleichen. Die einfachste Möglichkeit, dies zu tun, besteht in einem Blick auf die Kodierungen: 0 für ‘**A**’ und 1 für ‘**B**’. Da 0 kleiner als 1 ist, sagen wir, dass ‘**A**’ kleiner als ‘**B**’ ist. Was wir gerade getan haben, war die Anwendung einer Sortierung für unseren Zeichensatz. Die Sortierfolge ist eine Menge von Regeln – wenn auch in diesem Fall nur eine Regel: „Vergleiche die Kodierungen“. Diese einfachste aller möglichen Sortierfolgen nennen wir *Binärsortierung*.

Was aber, wenn wir ausdrücken wollen, dass Klein- und Grossbuchstaben äquivalent sind? Dann hätten wir schon zwei Regeln: (1) Betrachte die Kleinbuchstaben ‘**a**’ und ‘**b**’ als äquivalent zu den entsprechenden Grossbuchstaben ‘**A**’ und ‘**B**’. (2) Vergleiche die Kodierungen. Dies ist eine *Sortierung ohne Unterscheidung der Gross-/Kleinschreibung*. Sie ist ein kleines bisschen komplexer als eine Binärsortierung.

Im wirklichen Leben umfassen die meisten Zeichensätze viele Zeichen: nicht nur ‘**A**’ und ‘**B**’, sondern ganze Alphabete – manchmal sogar mehrere Alphabete oder fernöstliche Schreibsysteme mit Tausenden von Zeichen – sowie viele Sonder- und Interpunktionszeichen. Auch für Sortierungen gibt es im wirklichen Leben viele Regeln, die nicht nur die Behandlung der Gross-/Kleinschreibung angeben, sondern auch, ob Akzente und Tremata (die Punkte etwa auf den deutschen Umlauten Ä, Ö und Ü) unterschieden und wie Folgen aus mehreren Zeichen zugeordnet werden (beispielsweise die Regel, dass bei einer der beiden deutschen Sortierfolgen die Gleichung ‘**Ö**’ = ‘**OE**’ gilt).

MySQL erlaubt Ihnen ...

-   das **Speichern** von Strings in einer Vielzahl von Zeichensätzen,
-   das **Vergleichen** von Strings unter Verwendung einer Vielzahl von Sortierfolgen,
-   das **Mischen** von Strings verschiedener Zeichensätze oder Sortierfolgen auf demselben Server, in derselben Datenbank oder sogar derselben Tabelle,
-   die **Angabe** von Zeichensatz und Sortierfolgen (= Kollation) auf beliebiger Ebene . 

In dieser Hinsicht ist MySQL den meisten anderen Datenbanksystemen weit überlegen. Allerdings müssen Sie, um diese Funktionen effizient nutzen zu können, wissen, welche Zeichensätze und Sortierfolgen verfügbar sind, wie Sie die Standardeinstellungen ändern und wie diese das Verhalten von String-Operatoren und -Funktionen beeinflussen:

### Kollation (engl. Collation)

Die Angabe einer Kollation definiert die **Sortierung innerhalb der Spalten**. Also eine unterschiedliche Sortierung des gewählten Alphabets nach Gross- und Kleinschrift, Umlauten, Sonderzeichen, Akzenten usw. innerhalb der Spalte(n).

Schauen wir mal, was standardmässig eingestellt ist:

![](../x_res/KollationStandard.png)

Theoretisch können wir innerhalb einer Datenbank die Tabellen individuell konfigurieren und innerhalb der Tabellen sogar die Spalten mit einer individuellen Kollation versehen. Allerdings empfiehlt es sich bei produktiven Systemen davon abzusehen, denn es müssen bei Beziehungen (JOIN-Verknüpfungen) charset-encodings durchgeführt werden.

Öffnen Sie das obige Dropdown-Menü in phpMyAdmin, um zu sehen, wieviele Kollationen angeboten werden ....

*Hinweis*: Mit dem SQL-Befehl `show collation;` werden die geladenen Sortierungen im SQL-Editor angezeigt.


Die deutsche Norm DIN 5007-1 beschreibt unter dem Titel *„Ordnen von Schriftzeichenfolgen (ABC-Regeln)“* das Sortieren.

| **DIN 5007 Variante 1** <br> (für Wörter verwendet, etwa in Lexika)  | **DIN 5007 Variante 2** <br> (spezielle Sortierung für Namenslisten, etwa in Telefonbüchern)   |
|----------|----|
|  - *ä* und *a* sind gleich <br> - *ö* und *o* sind gleich  <br> - *ü* und *u* sind gleich <br> - *ß* und *ss* sind gleich | - *ä* und *ae* sind gleich <br> - *ö* und *oe* sind gleich  <br> - *ü* und *ue* sind gleich  <br> - *ß* und *ss* sind gleich  |

Wie eine Kolationseinstellung sortiert, erkennen Sie ansatzweise im Infofenster. 
Nun gibt es keine richtigen oder falschen Kollationen. Die [Auswahl](https://mariadb.com/docs/server/sql/features/localization/collations/) hängt immer vom Einsatz ab. Für unsere Zwecke sind `utf8mb4_general_ci` (Default), `utf8mb4_unicode_ci`, `latin1_german1_ci` oder `latin1_german2_ci` empfohlen. 

Ob Gross- und Kleinschrift beachtet wird, ist an der Endemarkierung `_cs` oder `_ci` zu erkennen: `c`ase-`s`ensitive oder `c`ase-`i`nsensitive. 

| Kollation (Gross/Klein wird angezeigt) | Zeichensatz <br> Sortierung  |
|----------|----|
| ![](../x_res/utf8mb4_general_ci.png) | UTF-8 (4B) <br> Unicode Standard Sortierung <br> Alternative: [utf8mb4\_unicode\_ci](https://www.pixelfriese.de/unterschied-zwischen-utf8_general_ci-und-utf8_unicode_ci/) |
| ![](../x_res/utf8mb4_german2_ci.png) | UTF-8 (4B) <br> Deutsch Variante 2 |
| ![](../x_res/latin1_general_ci.png) | ANSI Latin-1 <br> Unicode Standard Sortierung |
| ![](../x_res/latin1_german1_ci.png) | ANSI Latin-1 <br> Deutsch Variante 1 |
| ![](../x_res/latin1_german2_ci.png) | ANSI Latin-1 <br> Deutsch Variante 2 |

![](../x_res/Train_R1.png)

![2Do](../x_res/ToDo.png) Importieren Sie das SQL-Script [`kollation.sql`](../Daten/kollation.sql) in eine neu erstellte Datenbasis `kollation`. Die Nachnamen der Personen werden in verschiedenen Spalten unterschiedlich sortiert. 

- *Zuerst*: Untersuchen Sie das SQL-Script, wie die Kollation (und der Zeichensatz) im SQL-Befehl gesetzt werden muss.
- Testen Sie die Sortierungen spaltenweise
- Erstellen Sie neue Nachnamen (Müller, Mueller, Muller, Straus, Strauss, Strauß, Über, Ueber, Uber, ...)
- Ändern Sie die Kollation einer Spalte (in phpMyAdmin und Workbench)

<br><br><br>

---

## Auffrischen der SQL-Befehle aus M106(ÜK)/M164

Alle SQL-Befehle lassen sich einer der fünf Gruppen zuordnen:

**Datenbanksprache SQL und Befehlsgruppen** 
                                                                                              
| SQL    | Datenbanksprache; Structured Query Language   |
|----------------------------|--------------------|
| **DDL** <br> (Data Definition  Language)         | DB-Objekte definieren, z.B.  <br> - CREATE db-objekt <br> - ALTER db-objekt ADD/MODIFY/CHANGE/DROP <br> - RENAME db-objekt <br> - DROP db-objekt |
| **DML** <br>(Data Manipulation  Language)   | Daten einfügen, ändern, löschen, z.B.: <br> - INSERT <br> - UPDATE <br> - DELETE  <br> - TRUNCATE                   |
| **DQL (DRL)** <br>(Data Query / Retrieval Language)   | Daten abfragen, z.B.: <br> - SELECT <br> - SHOW <br> - DESCRIBE                    |
| **DCL** <br> (Data Control  Language)            | Zugriffsrechte verwalten, z.B.   <br> - GRANT <br> - REVOKE                                     |
| **TCL** <br> (Transaction  Control  Language)    | <br> - TRANSACTION / BEGIN <br> - COMMIT <br> - ROLLBACK                                                                     |

*Hinweis: DQL wird manchmal der Gruppe DML zugeordnet.*

![ToDo](../x_res/ToDo.png)Tragen Sie für jede der unten aufgeführten Tätigkeiten den passenden Befehl ein. Geben Sie rechts die entsprechende Befehlsgruppe an (DDL, DML, DCL). Markieren Sie in der letzten Spalte die "gefährlichen" Befehle.

| **Tätigkeit**                         | **SQL-Befehl**                                               | **Grp** | **![Achtung](../x_res/caution.png)** |
|---------------------------------------|--------------------------------------------------------------|---------|-----------------------------------------------------|
| 1) alle Daten einer Tabelle anzeigen  |                                   |      |                                                     |
| 2) Datenbank auswählen                |                                                   |         |                                                     |
| 3) eine neue Datenbank erstellen      |                                         |      |                                                     |
| 4) eine neue Tabelle erstellen        |        |      |                                                     |
| 5) eine Tabelle löschen               |                                        |      |                                                  |
| 6) Tabellenstruktur **kontrollieren**     |                                         |      |                                                     |
| 7) Datenbanken anzeigen               |                                  |      |                                                     |
| 8) Tabellen einer DB anzeigen         |                                    |      |                                                     |
| 9) Daten in eine Tabelle eintragen    |  |      |                                                     |
| 10) Daten in einer Tabelle ändern     |         |      |                                                     |
| 11) Daten in einer Tabelle löschen    |                           |      |                                                                                                     |
| 12) Spalte in einer Tabelle löschen   |                            |      |                                                  |

### SQL-Skripte (Batch Mode)

Wichtige Befehle im Zusammenhang mit Batch-Verarbeitung (**CMD**):

| CMD Befehl (evtl. Pfad angeben)| Kommentar |
|----|---|
| `mysql -u root -p db < pfad\script.sql` | Skript mit SQL-Statements auf die angegebene DB ausführen <br> (Dateiendung ist egal!) | 
| `mysql -u root -p db < pfad\script.sql > pfad\ausgabe.txt` | Skriptausführung mit Ausgabe in eine Textdatei |

> Achtung bei Verwendung der Windows Powershell Konsole: WPS kennt `<` nicht: Verwenden Sie den Cmdlet `GET-CONTENT`:
<br>   `gc pfad\script.sql | mysql -u root -p db > pfad\ausgabe.txt`

Wenn Sie bereits im Konsolenklient der Datenbank arbeiten (mysql.exe), können Sie die genannten Umleitungen nicht anwenden. Sie können dennoch Skriptdateien vom SQL-Prompt ausführen:

| MySQL Befehl | Kommentar |
|----|---|
| `mysql> source pfad\script.sql` | `script.sql` muss SQL-Statements enthalten |
| `mysql> \. pfad\script.sql` | **\\.** ist identisch mit **source** … |



> Lösung SQL-Befehle [hier](../Loesungen/Repetition_SQL_Loes.md).



---

## Die Datenbasis "Firma"

![](../x_res/Train_D1.png)

Wir wollen folgende Punkte repetieren und vertiefen:

-   Anwendung des Clients phpMyAdmin
-   Anwendung des Konsolenclients mysql.exe
-   Datenimport von SQL-Statements
-   Datenimport von CSV-Dateien
-   Auswirkungen von Codierungen und Kollationen

Es ist das folgende ERM gegeben:

![ERM_Firma2](../x_res/firma.png)

| Tabelle: tbl\_Projekte |              |        |        |
|-----------------------|--------------|--------|--------|
| **Feldname**          | **Datentyp** | **PS** | **FS** |
| ID\_Projekt            | INT/AI       |    X    |        |
| Projektbezeichnung    | VARCHAR(30)         |        |        |
| Projektleiter\_MA\_FS   | INT          |        |    X   |

| Tabelle: tbl\_Mitarbeiter |              |        |        |
|--------------------------|--------------|--------|--------|
| **Feldname**             | **Datentyp** | **PS** | **FS** |
| ID_Mitarbeiter           | INT/AI       |   X     |        |
| FamName                  | VARCHAR(30)  |        |        |
| Vorname                  | VARCHAR(20)  |        |        |
| FS\_Abteilung             | INT          |        |    X    |

| Tabelle: tbl\_TT\_Mitarbeiter\_Projekte |              |        |        |
|--------------------------------------|--------------|--------|--------|
| **Feldname**                         | **Datentyp** | **PS** | **FS** |
| MitarbeiterIDFS                      | INT          |   [X    |    X    |
| ProjekteIDFS                         | INT          |    X]    |    X    |


| Tabelle: tbl\_Abteilungen |              |        |        |
|--------------------------|--------------|--------|--------|
| **Feldname**             | **Datentyp** | **PS** | **FS** |
| ID\_Abteilung             | INT/AI       |   X     |        |
| AbtBezeichnung           | VARCHAR(30)         |        |        |


SQL-Code:

```SQL
CREATE TABLE `Firma`.`tbl_Mitarbeiter` (

`ID_Mitarbeiter` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,  
`FamName` VARCHAR( 50)NOT NULL ,  
`Vorname` VARCHAR( 30)NOT NULL ,  
`FS_Abteilung` INT NOT NULL  
) ENGINE= InnoDB;

CREATE TABLE `Firma`.`tbl_Abteilungen` (

`ID_Abteilung` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,  
`AbtBezeichnung` VARCHAR( 30)NOT NULL

) ENGINE= InnoDB;

CREATE TABLE `Firma`.`tbl_Projekte` (

`ID_Projekte` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,  
`ProjektBezeichnung` VARCHAR( 50)NOT NULL ,  
`FS_Mitarbeiter` INT NOT NULL  
) ENGINE= InnoDB;

CREATE TABLE `Firma`.`tbl_TT_Mitarbeiter_Projekte` (

`FS_Mitarbeiter` INT NOT NULL ,  
`FS_Projekte` INT NOT NULL ,  
PRIMARY KEY (`FS_Mitarbeiter` ,`FS_Projekte`)

) ENGINE= InnoDB;
```

> Dateien zu den folgenden Aufgaben sind hier: [Datenpool!](../Daten)

-   **Erstellen** Sie die Datenbank (neue Datenbank anlegen: `Firma`), die Tabellen und die Attribute in MySQL: Erstellen Sie ein oder zwei Tabellen über `phpMyAdmin` und starten Sie dann den Konsolenklient (`mysql.exe`) und erzeugen per SQL-Code die restlichen Tabellen. Den Quellcode zur Erzeugung finden Sie auch im Datenpool: `DDL_V2.txt`. Sie sollten ihn aber nicht einfach kopieren, sondern möglichst verstehen.

**\__________________________\_**

-   **Füllen** Sie die Tabelle `tbl_Abteilungen` mit Werten aus dem Textfile `Abteilungen.txt` ab (Importieren "CSV mit LOAD DATA INFILE" …). <br> Es ist eine `CSV`-Datei. Das bedeutet zwar **C**omma**S**eparated**V**alue, die Werte in dieser Datei sind aber mit Semikolon (;) getrennt (einstellbar). <br> Experimentieren Sie mit den Werten des Primärschlüssels (vorgeben, nicht vorgeben, überschreiben…). Achten Sie auf die Umlaute, importieren Sie die Datei mit verschiedenen Einstellungen mehrmals, löschen Sie den gesamten Inhalt der Tabelle (schauen Sie sich immer die entstandenen SQL-Befehle an). <br> Öffnen Sie die Textdatei in einem Editor. Sie ist ANSI-codiert. Speichern Sie sie im `utf8`-Format ab und importieren Sie sie neu. Jetzt sollten die Umlaute korrekt dargestellt werden. <br> Es gibt noch die Datei *Abteilungen2.txt* (utf-8 mit BOM). Mit dem HEX-Editor (Hexed.it) kann der Inhalt genauer untersucht werden.

**\__________________________\_**

-   Als nächstes **importieren** wir eine Tabelle, die als SQL-Code vorliegt: `tbl_plz_ort.sql`. Also wieder "Importieren", aber diesmal nicht wie oben `CSV`, sondern `SQL`. Damit haben wir ohne grossen Aufwand eine Tabelle mit allen Schweizer Ort- und Postleitzahlkombinationen erhalten.

**\__________________________\_**

-   Bevor wir weiterfahren, **korrigieren** wir noch eine Kleinigkeit: Beim Import sind zwei *Indizes* generiert worden (warum auch immer). <br> ![](../x_res/indizes.png) <br> Wir nutzen die Gelegenheit und schauen uns die Grösse der Datei `tbl_plz_ort.MYI` im Datenverzeichnis der Datenbanken an (`.../data/firma` – oder was auch immer in der `my.ini` definiert ist). Derzeitige Grösse vermutlich ca. 100 KB. Ein Index ist überflüssig – löschen Sie den Index `plz_ort_id` und kontrollieren Sie die Veränderung in der Grösse der `MYI`-Datei.

**\__________________________\_**

-   ![](../x_res/Wichtig.png) Wenn Sie sich jetzt die Tabellentypen anschauen, werden Sie sehen, dass wir 4 `InnoDB`-Tabellen und eine `MyISAM`-Tabelle haben. <br> ![](../x_res/myisam.png) <br> Sie haben im Datenverzeichnis bereits die 3 Dateien der `MYISAM`-Tabelle gefunden. Für die `InnoDB`-Tabellen gibt es aber nur zwei Dateien: `*.frm` und `*.ibd`. Jetzt ändern Sie auch für die Tabelle `tbl_plz_ort` den Tabellentyp auf `InnoDB` (Tabelle auswählen und den Reiter *Operationen* selektieren). Jetzt halt nochmal bei den Dateien nachschauen – irre, was!

**\__________________________\_**

-   Wenn wir uns die Daten anschauen, sehen wir, dass die Textattribute in doppelten Anführungszeichen eingeschlossen sind. Das war so nicht geplant. Also leeren wir die Tabelle (ACHTUNG: leeren und löschen ist ein Unterschied!) und verwenden für den nächsten Import eine aktuellere Version als `CSV`-Import: `PLZ_ORT.csv`. Anschliessend kontrollieren Sie noch, ob die **französischen und deutschen Sonderzeichen** korrekt dargestellt werden. Alternativ können Sie auch selbst die **aktuellsten Daten von der Schweizer Post** verwenden. Es handelt sich dabei um eine gezippte Textdatei, die Sie zum Beispiel via Tabellenkalkulation bearbeiten können: überflüssige Spalten raus, Speicherung als `csv`-Text-Datei mit dem richtigen Zeichensatz (z. B. utf-8) und so weiter.

**\__________________________\_**

-   Bis jetzt haben diese Daten mit unseren bisherigen Daten keinen Zusammenhang. Ziel ist es, die Tabellen `tbl_mitarbeiter` mit einem **Fremdschlüssel** zu erweitern, der auf den jeweiligen Wohnort der Mitarbeiter verweist. Erfassen Sie mindestens zwei oder drei Mitarbeiter, bevor Sie weitermachen. Anschliessend erweitern Sie die Tabellenstruktur `tbl_mitarbeiter` um das Attribut `FS_Wohnort` mit dem Datentypen `INT` (integer). Falls Sie keine weiteren Angaben gemacht haben, sind die Zellen automatisch mit der Ziffer `Null` initialisiert worden.

**\__________________________\_**

-   Um nicht alle Daten manuell erfassen zu müssen, können Sie jetzt wieder ein **`SQL`-Skript** verwenden, dass Ihnen etwas über tausend Personendatensätze zur Verfügung stellt: `tbl_mitarbeiter.sql`. Nutzen Sie wieder mal den Befehl `SOURCE` in der MySQL-Konsole um die Datei einzulesen. Falls Sie schon Daten erfasst haben, die Sie behalten möchten, müssen Sie diese vorher sichern. 

**\__________________________\_**

- ![](../x_res/Wichtig.png)**Untersuchen** Sie nun alle Tabellenstrukturen ihrer Datenbank mit den SQL-Befehlen <br> `DESCRIBE tabellenname;`       *oder* <br> `SHOW CREATE TABLE tab_name;`      *oder* <br> `SELECT * FROM tab_name PROCEDURE ANAYLSE()`;

**\__________________________\_**

-   **Erzeugen** Sie eine weitere Tabelle (z.B. Personen) nach eigenen Vorstellungen und erstellen Sie eine Textdatei mit passenden Daten. Lesen Sie die Datei von der Betriebssystem-Konsole mit `mysqlimport` ein und ausserdem aus der MySQL-Konsole mit `LOAD DATA INFILE.` 

    In etwa so ...

    ```SQL
    LOAD DATA INFILE 'personen.txt' INTO TABLE personen
    FIELDS TERMINATED BY ','; SHOW WARNINGS;
    ```
    ```BATCH
    > mysqlimport.exe db personen.txt –v –u root –p -–fields-terminated-by ',';
    ```
**\__________________________\_**

-  Erstellen Sie jetzt von der Konsole einen **Dump**, der vor jedem `CREATE TABLE` ein `DROP TABLE IF EXISTS` einführt.

    In etwa so ...

    `> mysqldump.exe –u root –p -–add-drop-table db > db.sql`

**\__________________________\_**

-   Wer bis hierher gekommen ist und noch Mumm in den Knochen hat, kann mal ausprobieren, wie man `JSON`-Daten in die Tabelle `firma.kunden` reinkriegt: *(Nicht mit phpMyAdmin umsetzen!)*

	```SQL
	CREATE TABLE kunden (ID INT, CUSTOMER_ID INT, CUSTOMER_NAME VARCHAR(255));
	```
	Die Daten kommen nun aus einem **JSON-Array**. Wir speichern das Array in die User Variable `@json_data` und geben sie zur Kontrolle aus:
	
	```SQL
	SET @json_data := '
	[  { "ID": 1,
	     "CUSTOMER_ID": 1088020,
	     "CUSTOMER_NAME": "My customer 1"
	   },
	   { "ID": 2,
	     "CUSTOMER_ID": 523323,
	     "CUSTOMER_NAME": "My customer 2"
	   },
	   { "ID": 3,
	     "CUSTOMER_ID": 1934806,
	     "CUSTOMER_NAME": "My customer 3"
	   }
	]
	';
	
	Select @json_data;
	```
	
	Mit der **Funktion** [`JSON_TABLE()`](https://mariadb.com/kb/en/json_table/) lassen sich die Objekt-Werte `$[*]` den Spalten zuordnen:
	
	
	```SQL
	INSERT INTO kunden
	SELECT *
	FROM JSON_TABLE(@json_data,
	                "$[*]" COLUMNS( ID INT PATH "$.ID",
	                                CUSTOMER_ID INT PATH "$.CUSTOMER_ID",
	                                CUSTOMER_NAME VARCHAR(255) PATH "$.CUSTOMER_NAME")
	               ) AS jsontable;
	
	Select * FROM kunden;
	```
	     
	*Hinweise:*
     - Die [User-Variable](https://mariadb.com/kb/en/user-defined-variables/) `@json_data`, wird mit dem JSON String gefüllt.
     - Alle User-Variablen anzeigen: `SELECT * FROM information_schema.USER_VARIABLES ORDER BY VARIABLE_NAME;`
     - Weitere [Details](https://mariadb.com/resources/blog/json-with-mariadb-10-2/#:~:text=Here%20I%E2%80%99ll%20describe%20JSON%20and%20the,MariaDB%20JSON%20functions%20and%20their%20uses.&text=Here%20I%E2%80%99ll%20describe%20JSON,functions%20and%20their%20uses.&text=describe%20JSON%20and%20the,MariaDB%20JSON%20functions%20and)
     

<br><br><br>



---

# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./2T_CheckPoint.md)

[Repetition Tools Tag 1 & 2](./Repetition_Kap2_3.md)


---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)