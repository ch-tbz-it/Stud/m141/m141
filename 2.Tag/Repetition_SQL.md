![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen

## Repetition zu SQL - M164

Tragen Sie für jede der unten aufgeführten Tätigkeiten den passenden Befehl ein. Geben Sie rechts die entsprechende Befehlsgruppe an (DDL, DML, DCL). Markieren Sie in der letzten Spalte die "gefährlichen" Befehle.

| **Tätigkeit**                         | **SQL-Befehl**                                               | **Grp** | **![Achtung](../x_res/caution.png)** |
|---------------------------------------|--------------------------------------------------------------|---------|-----------------------------------------------------|
| 1) alle Daten einer Tabelle anzeigen  |                                   |      |                                                     |
| 2) Datenbank auswählen                |                                                   |         |                                                     |
| 3) eine neue Datenbank erstellen      |                                         |      |                                                     |
| 4) eine neue Tabelle erstellen        |        |      |                                                     |
| 5) eine Tabelle löschen               |                                        |      |                                                  |
| 6) Tabellenstruktur kontrollieren     |                                         |      |                                                     |
| 7) Datenbanken anzeigen               |                                  |      |                                                     |
| 8) Tabellen einer DB anzeigen         |                                    |      |                                                     |
| 9) Daten in eine Tabelle eintragen    |  |      |                                                     |
| 10) Daten in einer Tabelle ändern     |         |      |                                                     |
| 11) Daten in einer Tabelle löschen    |                           |      |                                                  |
| 12) einen Index löschen               |                              |      |                                                  |
| 13) einen Index erstellen             |                     |      |                                                     |
| 14) Spalte in einer Tabelle löschen   |                            |      |                                                  |
