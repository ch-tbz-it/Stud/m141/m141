![](../x_res/tbz_logo.png)√

# M141 - DB-Systeme in Betrieb nehmen


# ![](../x_res/CP.png) Checkpoint 4.Tag


## DB-Server im LAN

1.  Welcher Befehl testet die Verbindung zum Server-Rechner mit Adresse 139.79.124.97?

    - [ ] mysql -h 139.79.124.97

    - [ ] ipconfig

    - [ ] ping 139.79.124.97

    - [ ] mysqladmin -h 139.79.124.97 -u root -p ping

2.  Wozu wird der Parameter -h bei MySQL verwendet?

    - [ ] bewirkt die Abfrage des Passworts

    - [ ] bewirkt die Verbindung als bestimmter Benutzer

    - [ ] Angabe der Adresse des Client-Rechners

    - [ ] Angabe der Adresse des Server-Rechners

3.  Was bewirkt der Befehl 'mysqldump -h 139.79.124.97 hotel \> datei.txt'?

    - [ ] Backup der DB hotel in die Datei datei.txt auf Adresse 139.79.124.97

    - [ ] Backup der angegebenen DB auf dem Server mit der IP-Adresse 139.79.124.97

    - [ ] Restore der Datenbank hotel auf dem Server mit der Adresse 139.79.124.97

    - [ ] Ausführen des SQL-Skripts datei.txt auf Adresse 139.79.124.97 auf die DB hotel

4.  Welche Aufgabe hat der ODBC-Driver?

    - [ ] passt die SQL-Befehle dem entsprechenden DB-Server an

    - [ ] ermöglicht das Erstellen und Konfigurieren von ODBC-Datenquellen (DSN)

    - [ ] ermöglicht den einheitlichen Zugriff einer Applikation auf verschiedene Datenbanken

    - [ ] ermöglicht den Zugriff einer Applikation auf eine bestimmte DB

5.  Wie greifen Sie vom Konsolenfenster auf einen DB-Server mit Adresse 139.79.124.97 zu?

    - [ ] mysqladmin -h 139.79.124.97

    - [ ] mysql -h 139.79.124.97 hotel \< hotel.bkp

    - [ ] mysql -h 139.79.124.97 -u root -p

    - [ ] ping 139.79.124.97

1.  Welche Aufgaben hat der DB-Server im Gegensatz zum DB-Client?

    __   
      

2.  Weshalb benutzt man MS Access z.B. zusammen mit einem MySQL-Server?

    __   
      

3.  Wie bestimmen Sie die IP-Adresse des Server-Rechners?

    __   
      

4.  Wie prüfen Sie, ob der DB-Server auf Adresse 139.79.124.97 läuft?

    __   
      

5.  Welcher Befehl führt das SQL-Skript xy.sql auf die DB hotel auf Adresse 139.79.124.97 aus?

    __   