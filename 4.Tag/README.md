![](../x_res/tbz_logo.png)√

# M141 - DB-Systeme in Betrieb nehmen  (4.Tag)

*Autoren: Gerd Gesell, Kellenberger Michael 2024*

**Inhalt**

[TOC]

> ([Lösung 3.Tag](../Loesungen/3T_CP_Loes.md))



# Hackerangriffe

![Angriffe](./media/Angriffe.png)


- Im Juli 2011 wurde das Observationssystem „PATRAS“ der deutschen Bundespolizei vom Hacker-Netzwerk „no name crew“ (kurz „nn-Crew) gehackt. Ziemlich peinlich, denn dieses System enthielt sensible Daten und wurde mittels eines XAMPP-Bundles auf einem Server betrieben.

- Ein öffentlicher TBZ-Raspi mit einem MySQL-Server für Übungen wurde 2023 angegriffen. Es gab einen User `'admin'@'%'` ohne Passwort: ![No PW](./media/XAMPP-Admin-noPW.png) <br> ![Verschlüsselt](./media/XAMPP-RansomeWare.png)

# Datenbank-Sicherheit

![](../x_res/Learn.png)

> ![](../x_res/caution.png) **Achtung:** XAMPP ist zur Entwicklung konstruiert. Daher sind die Sicherheitsvorkehrungen bewusst ausgeschaltet, um bei der Entwicklung und beim Testen (gute Informatiker machen das!) ohne zusätzliche Hürden arbeiten zu können. D.h. es gibt einen Standard-Admin Benutzer namens `root` *OHNE* Passwort. Auch der typische Name `admin` wird bei der Installation oft erstellt.

Bei den bisherigen Aufgaben sind Sie kaum mit der Benutzerverwaltung von MySQL in Berührung gekommen. Wenn Sie den MySql-Klient als normaler Benutzer gestartet haben, hatten Sie für bestimmte Datenbanken nicht genügend Rechte, daher mussten wir `mysql –u root` mit der Benutzerkennung für den root-User eingeben. 

![](../x_res/Hinweis.png) Auch bei der Einrichtung des Datenbankverzeichnisses auf dem eigenen Laufwerk waren Aktionen notwendig, die die Benutzerverwaltung betreffen: die **`mysql`-Datenbasis** muss im `datadir`-Verzeichnis vorhanden sein, sonst lässt sich der Server gar nicht aufstarten.

![](../x_res/System-Tabellen.png)


Abb. 17: MySQL System-Tabellen in phpMyAdmin

Der `GRANT`-Befehl vom SQL, den Sie bereits bei einer Übung eingesetzt haben, nimmt die entsprechenden Einträge in den Berechtigungs-Tabellen (**\*_priv**) vor. Die Einschränkungen können bis auf Attributsebene vorgenommen werden. Je nach Berechtigung kann ein Benutzer also nicht alle Spalten einer Tabelle sehen oder verändern. Die für uns wichtigen Tabellen sind: **global\_priv, db, tables\_priv und colums\_priv**. (Vergl. Abb.17)

## Zugangskontrolle

Die Zugangskontrolle führt MySQL in zwei Phasen durch. Die erste Phase - der Klient baut die Verbindung zum Server auf – nennt sich **Authentifizierung**. Sie überprüft drei Angaben, nämlich Benutzername, Passwort und die Rechner IP-Adresse, von dem aus zugegriffen wird. Diese Informationen findet MySQL in der  Ansicht (View)  `user`. Im positiven Fall werden anschliessend bei jedem Request an die Datenbank die Berechtigungen überprüft: die **Autorisierung**.

![](../x_res/View_User.png)


###   Authentifizierung (Identitätsprüfung: Wer?): 

Der Server prüft, ob sich der Benutzer mit dem Server verbinden darf. Identifiziert wird jeder Benutzer durch seinen Benutzer- und Hostnamen. (Dieser Benutzername gilt für den DB-Server und hat nichts mit dem Benutzernamen des Betriebssystems zu tun.) Das Sicherheitssystem von MySQL beruht auf folgenden Informationen:

| Sicherheitsinformationen |                                                   |
|--------------------------|---------------------------------------------------|
| **`Benutzername`**       | Name für die DB-Anmeldung (Default: leer)         |
| **`Passwort`**           | Wird verschlüsselt abgespeichert (Default: leer)  |
| **`Hostname`**           | Rechnername/IP des Benutzers  <br> <br>  - `localhost`: User darf auf dem *Server* einloggen <br> - `'%'` (Default): User darf von *überall*, ausser vom Server einloggen  <br> -` 172.16.17.111`: User darf von Klient mit *IP* einloggen <br> - `'name.local'`:  User darf von Klient mit *Hostnamen* einloggen |

Tab. 6.3: Die 3 Informationen des Sicherheitssystems

Ein Benutzer wird also folgendermassen angelegt (mit und ohne Passwort):

```
DROP USER IF EXISTS 'user'@'hostname';
CREATE USER 'user'@'hostname' IDENTIFIED BY 'Passw0rt';  -- User with password 'Passw0rt'

CREATE USER 'user2'@localhost;                           -- User with no password
``` 
Einloggen gemäss Einstellungen (localhost):

```
>C:\XAMPP\mysql\bin\mysql.exe -p -u user_localhost
PASSWORD: ****
``` 
Einloggen auf Remote-Server gemäss Einstellungen (%):

```
>C:\XAMPP\mysql\bin\mysql.exe -p -u user_remote -h ip-mysql-server
PASSWORD: ****
```

[Manual CREATE USER](https://mariadb.com/kb/en/create-user/)

### Passwort (-verschlüsselung)

Zum Verschlüsseln stellt MySQL die Funktion *password()* zur Verfügung, die einen 41-Bytes Hash erzeugt. Sie können den Hashwert erzeugen und anzeigen mit: 

`SELECT PASSWORD('TBZforever');`

> Beachten Sie den Stern **\*** am Anfang des Hash-Wertes! D

Das Passwort eines Users wird folgendermassen gesetzt (oder geändert) und gleich aktiviert:

```
SET PASSWORD FOR 'user'@'%'  = password('TBZforever'); 
SET PASSWORD FOR 'user2'@localhost = '*74B1C21ACE0C2D6B0678A5E503D2A60E8F9651A3');
FLUSH PRIVILEGES;                     -- Aktivierung -- nie vergessen!
```

`FLUSH PRIVILEGES` macht die Änderungen sofort wirksam (statt erst nach dem Server-Neustart).

> Das Passwort darf aus Sicherheitsgründen nicht unverschlüsselt abgespeichert werden. **Auch sollte der obige Befehl mit Passwort im Klartext nicht in einem Script erscheinen.**


![note](../x_res/note.png) Gelöscht wird das Passwort durch Eingabe eines leeren Passwortes.

[Manual SET PASSWORD](https://mariadb.com/kb/en/set-password/)

---

![](../x_res/Train_R1.png)

![](../x_res/ToDo.png) **User erstellen:**

1. Erstellen Sie ein paar Benutzer mit verschiedenen Passwörtern, IPs und Hostnamen.
2. Untersuchen Sie die Einträge in der Ansicht `mysql.user` und der Datenbasis `mysql.global_priv`.
3. Testen Sie die Logins. (Welche gehen nicht?)
4. Ändern Sie einzelne Passwörter.
5. Löschen Sie die Benutzer wieder.
6. Wie können Sie einen lokalen User (localhost) zu einem "remote" User machen? Testen Sie es aus!

<br><br><br>

---

# DB-Server im LAN

![](../x_res/Learn.png)

Die Informatik ist geprägt von der *Dezentralisierung*. Im Zusammenhang mit DB-Applikationen bedeutet dies Verteilung der Aufgaben und/oder der Datenbasis auf mehrere Rechner.

| Warum Dezentralisierung ? |                                                            |
|---------------------------|------------------------------------------------------------|
| Datenverfügbarkeit        | Sicherer bei mehreren Servern, z.B. falls eine DB ausfällt |
| Datensicherheit           | Zugriffssteuerung einfacher bei kleineren Datenbeständen   |
| Flexibilität              | Einfachere Änderung von Teilsystemen                       |
| Performance               | Besser bei mehreren Rechnern                               |
| Kosten                    | Tiefer als bei teuren Grosscomputern                       |

Tab. 7.1: Gründe für die Dezentralisierung

### Zugriff über das Netz

![](../x_res/DB-Client.png)

Abb. 21: Beispielkonfiguration mit einem DB-Klient und einem DB-Server

Der Zugriff vom Klient auf den DB-Server erfolgt wie bisher, es ist nur jeweils zusätzlich die *Netzwerkadresse (IP-Adresse)* bzw. der *Rechnername (Hostname)* des Servers anzugeben. Befindet sich zwischen DB-Klient und DB-Server eine Firewall, so muss *Port 3306* geöffnet werden, andernfalls ist eine Kommunikation nicht möglich.

**a) Verbindung zum DB-Servertesten**:

Der erste Befehl prüft, die Verbindung zum Server steht, und der zweite, ob der MySQL-Server auf dem angegebenen Server läuft. Mit dem Parameter `-h` ist der Server-Rechner anzugeben. (Testen Sie die Notwendigkeit, einen User anzugeben ...)

```batch
> ping 172.16.17.4
> mysqladmin -h 172.16.17.4 ping
C:\xampp\mysql\bin\mysqladmin.exe: connect to server at 'm122-server.local' failed
error: 'Access denied for user 'max'@'fe80::ba6c:9d58:e43d:581%eth0' (using password: NO)'

> mysqladmin -h 172.16.17.4 -u remote -p ping
mysqld is alive
```

**b) Über das Netz auf die DB zugreifen**:

Auch der Monitor wird mit dem Parameter *-h* gestartet.

`> mysql -h 172.16.17.4`

Zugriff mit Benutzername und Passwort:

```SQL
> mysql -h 172.16.17.4 -u remote -p

Enter password: ******
```

Bei erstellter Verbindung, sind die Befehle gleich wie im lokalen Betrieb.

**c) Backup und Restore über das Netz ausführen**:

Um ein Backup einer DB auf einem anderen Rechner zu erstellen, ist die Rechner-Adresse anzugeben.

```SQL
> mysqldump -h 172.16.17.4 -u remote -p firma > H:\backup.sql
Enter password: ******
```

Beim Restore ist ebenfalls die Adresse des Servers anzugeben:

```SQL
> mysql -h 172.16.17.4 -u remote -p firma < H:\backup.sql

Enter password: *****
```

**d) Netzwerkzugriff auf den DB-Server zeitweise verbieten**:

Mit dem Eintrag `skip-networking` in der Konfigurationsdatei `C:\...\my.ini` des Servers ist ein Zugriff via TCP/IP (auch lokal) nicht mehr möglich.

```
[mysqld]

skip-networking
```

Ein Verbindungsversuch ergibt folgende Fehlermeldung:

`ERROR 2003: Can't connect to MySQL server on '172.16.17.4' (10061)
`

**e) phpMyAdmin extern aufrufen**

Rufen Sie das Admin-Tool über Ihren Remote-Browser auf:

URL:  `IP-Hostname/phpMyAdmin`

![](../x_res/Remote_pma.png)

Geben Sie beim Login einen *User* mit *PW* an. (User mit Hostname='*%*')

Falls der Zugriff nicht funktioniert, mal grundsätzlich den Zugriff auf den Apache-Webserver testen (URL:  `IP-Hostname/Dashboard`) oder [hier](https://kinsta.com/de/wissensdatenbank/fehler-403-xampp/) schauen:

> `httpd-xampp.conf` Abschnitt `phpmyadmin`: <br> Text `Require local` löschen und ihn durch `Require all granted` ersetzen.

---

![](../x_res/Train_R1.png) 

![](../x_res/ToDo.png) **Praxisarbeit: Remote Setup**

1. Benutzen Sie einen [Netzwerkscanner](https://www.advanced-ip-scanner.com/de/), um Ihr Netz zu analysieren.

2.  Netzverbindung zum Server-Rechner testen:

    Verifizieren Sie mit dem entsprechenden Befehl, dass der Server-Rechner vom Klient-Rechner aus über das Netz erreichbar ist. Verwenden Sie zur Adressierung des Server-Rechners die vorher bestimmte IP-Adresse.

3.  Verbindung zum DB-Server testen

    Testen Sie mit dem entsprechenden Befehl die Verbindung zum DB-Server. Das Resultat *mysqld is alive* zeigt zudem, dass der DB-Server läuft.

4.  Über das Netz auf den DB-Server zugreifen

    Starten Sie auf dem Klient-Rechner den MySQL-Monitor als Benutzer `remote`.

    Geben Sie die IP-Adresse Ihres DB-Server-Rechners an.

    Fragen Sie den Status des Servers ab und untersuchen Sie die Angaben für *Current user* und *Connection*.

    Testen Sie, ob Sie vom Klient aus Zugriff auf alle Tabellen der DB `morebooks` haben und eine beliebige SQL-Abfrage darauf ausführen können.

5.  Backup und Restore über das Netz ausführen

    Erstellen Sie als Benutzer `remote` ein Backup der Datenbank `morebooks` in der Datei `C:\\temp\\backup.sql`.

    Überschreiben Sie den ersten Autorennamen mit dem eigenen Namen.

    Spielen Sie das Backup auf den Server zurück und kontrollieren Sie mit einer SQL-Abfrage, dass der erste Autor Ihren Namen hat.

6.  Netzzugriff auf den DB-Server vorübergehend verbieten

    Tragen Sie auf dem Server-Rechner in die Konfigurationsdatei die Option ein, welche den Zugriff über das Netz verbietet. Starten Sie den Server erneut, damit der Eintrag wirksam wird.

    Versuchen Sie, vom Klient aus, eine DB-Abfrage auszuführen. Dies sollte nun nicht mehr möglich sein.

    Lassen Sie auf dem Server den Netzzugriff wieder zu und prüfen Sie, dass Sie nun auf dem Klient die DB-Abfrage ausführen können.

    Überlegen Sie, in welchen Fällen das temporäre Sperren des Netzzugriffs sinnvoll ist.
    

<br> <br> <br>

---

# DB-Server absichern - Default-Sicherheitseinstellungen

![caution](../x_res/caution.png) Nach der Default-Installation von MySQL gilt eine sehr unsichere Einstellung ohne Passwort-Absicherung. Jeder kann sich ohne Passwort vom lokalen Rechner oder von jedem externen Rechner aus als `root` oder sogar ohne Benutzernamen anmelden!

> `root`: Wie unter Unix/Linux spielt `root` bei MySQL die Rolle des *Administrators* (*Superuser*) mit unbeschränkten Rechten. Nach der Installation kann man sich als `root` ohne Passwort anmelden. Die Defaulteinstellungen sind in der Systemdatenbank *mysql* abgespeichert und werden durch das MySQL-Setup-Programm eingetragen.

Die folgenden Massnahmen schränken den Zugang ein. Sie sollten direkt nach der Installation eines DB-Servers erfolgen.

**`root`-Passwort für lokalen Zugang festlegen**: Mit folgendem Befehl wird für den Superuser root ein Passwort für den Zugriff vom Server-Rechner aus festgelegt.

```SQL
C:\>mysql -u root

SET PASSWORD FOR root@localhost = PASSWORD('superpasswort');
FLUSH PRIVILEGES;     -- nie vergessen!

```

![caution](../x_res/caution.png) Passen Sie auf, dass Sie dem Benutzer `root` nicht die nötigen Privilegien wegnehmen, sonst haben Sie u.U. keinen Zugriff mehr auf den eigenen Server !

![note](../x_res/note.png) Von jetzt an muss man sich als root mit -u root mit der Option -p anmelden und das Passwort eingeben.

```
C:\>mysql -u root -p
Enter password: *****
```

Falls Sie diese Änderung über phpMyAdmin durchgeführt haben, haben Sie jetzt ein Problem: Die Verbindung zum Server kann nicht hergestellt werden. Logisch! Wir sind ja als root ohne Passwort angemeldet. Die notwendigen Änderungen müssen in der Datei `config.inc.php` durchgeführt werden. Diese finden Sie in `C:/xampp/phpMyAdmin`. Weitere Informationen finden Sie in der Dokumentation: <https://docs.phpmyadmin.net/en/latest/config.html#basic-example>.


![](../x_res/Anpassung.png)

Abb. 19: notwendige Anpassung in der config.inc.php für root-Passwort

Sie können den auth-type auch auf *http* oder *cookie* (siehe Kommentartext im config-File) setzen und werden dann beim Start von phpMyAdmin nach Name und Passwort gefragt. User- und Passworteinträge können in diesem Fall leer bleiben. Das hat den Vorteil, dass die Anmeldung im phpMyAdmin mit verschiedenen Benutzern erfolgen kann.

**`root`-Zugang von fremden Rechnern aus verhindern**: Löschen des Users`root` von anderen Rechnern (`%`) aus:

```SQL
DROP USER 'root'@'%';  -- oder mindestens REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'root'@'%';
FLUSH PRIVILEGES;
```

Nach dem Löschen ist der Zugang als `root` mit Passwort nur von `localhost` aus möglich;  <br> oder wir setzen zwingend ein Passwort:

`SET PASSWORD FOR 'root'@’%’ = PASSWORD('superpasswort')`

**Kein Zugang ohne Passwort von externen Rechnern**: Auch alle anderen Benutzer von extern müssen ein Passwort haben.

**Lokalen Zugang ohne Passwort ermöglichen**: Vielleicht ist die Absicherung des Servers nun zu einschränkend und man möchte für den lokalen Rechner einen generellen MySQL-Zugang ohne Benutzernamen und Passwort doch wieder zulassen:

```SQL
GRANT USAGE ON *.* TO ''@localhost;
FLUSH PRIVILEGES;
```

![](../x_res/Hinweis.png)`USAGE` erlaubt nur das Anmelden ohne Privilegien.

Die Einträge in der Ansicht `user` in der `mysql`-Datenbank sind vom Administrator sehr kritisch zu untersuchen und den Bedürfnissen entsprechend anzupassen. Diese Schritte sind für eine professionelle Umgebung nur der Anfang.

<br><br><br>

---

# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./4T_CheckPoint.md)


---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md) 