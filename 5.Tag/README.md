![](../x_res/tbz_logo.png)√
# M141 - DB-Systeme in Betrieb nehmen  (5.Tag)

*Autoren: Gerd Gesell, Kellenberger Michael 2024*

**Inhalt**

[TOC]

> ([Lösung 4.Tag](../Loesungen/4T_CP_Loes.md))

---

![](../x_res/Learn.png)

# Zugriffsberechtigung / Autorisierung (Was?)
   
Die Autorisierung kann entweder *global* oder *lokal* auf Tabellen, usw. vergeben werden. 


![](../x_res/Zugriffsberechtigung.png)

Abb. 18: Zugriffsberechtigung für verschiedene Benutzer einer Datenbank


## Das MySQL-Zugriffssystem

**Begriffe**   

| PRIVILEG              | Berechtigung auf eine DB, Tabelle oder Spalte  |
|:--------------------|:-----|
| **Globales** Privileg | Berechtigung für alle DB, Tabellen und Spalten |
| **SELECT**-Privileg   | Benutzer darf Daten lesen                      |
| **UPDATE**-Privileg   | Benutzer darf Daten ändern                     |
| **GRANT**-Privileg    | Benutzer darf Zugriffsrechte festlegen u. Benutzer erstellen    |
| **FILE**-Privileg \*    | Werden benötigt, um eine Reihe von Datei-Befehlen auszuführen: `LOAD FILE ()`, `LOAD DATA INFILE …`   |
| **All**               | Alle Privilegien (ausser GRANT OPTION)        |
| **Usage**             | keine Privilegien (nur Connect auf den Server)  |

Tab. 6.3: Begriffe betreffend Privilegien


*) ![](../x_res/caution.png) Das FILE-Privileg sollte mit Vorsicht eingesetzt werden, da dieser User alle Dateien lesen kann, die öffentlich sind oder vom MySQL-Server gelesen werden können – unabhängig vom Datenbankverzeichnis!

## Benutzer einrichten und Zugriffsrechte festlegen

Die Befehle `GRANT` und `REVOKE` (Data Control Language = DCL) werden eingesetzt. Manual [GRANT](https://mariadb.com/kb/en/grant/) / [Revoke](https://mariadb.com/kb/en/revoke/)

![caution](../x_res/caution.png) Voraussetzung: Der Benutzer benötigt das `GRANT`-Privileg. Entweder erstellen Sie einen User mit dem Zusatz „`WITH GRANT OPTION`“ oder Sie führen die Beispiele als `root`-User aus.

### Zugriffsrechte ändern mit GRANT und REVOKE

Mit den Befehlen `GRANT` und `REVOKE` werden Privilegien erteilt bzw. gelöscht. Beachten Sie in der Syntax: `GRANT TO` und `REVOKE FROM` – ist aber ja logisch!

```SQL
GRANT privileg1 [, privileg2, ...]
ON [datenbank.]tabelle
TO user@host [IDENTIFIED BY 'passwort'] [WITH GRANT OPTION] ;

REVOKE privileg1 [, privileg2, ...]
ON [datenbank.]tabelle
FROM user@host ;
```

| Privilegien               |                                                         |
|---------------------------|---------------------------------------------------------|
| **ON** \*.\*              | globale Privilegien (alle DB mit allen Tabellen)        |
| **ON** db.\*              | DB-Privilegien (alle Tabellen der DB)                   |
| **ON** db.tb              | Tabellen-Privilegien (alle Spalten der Tabelle)         |
| (att1, att2) **ON** db.tb | Spalten-Privilegien (Spalten att1 und att2 der Tabelle) |

Tab. 6.4: Die verschiedenen Ebenen der Privilegien

**Zugriffsrechte auf alle Tabellen einer DB festlegen**:

Der folgende Befehl erteilt dem Benutzer `hotel_admin` alle Privilegien (inklusive Grant). Ein bestehendes Passwort wird dabei nicht verändert.

`GRANT ALL ON hotel.* TO hotel_admin@localhost WITH GRANT OPTION;`

Der Befehl, um einem User Dateizugriffsberechtigungen zu geben, lautet:

`GRANT FILE ON *.* To Username@‘%‘`

**Lesen und Ändern:**

Der folgende Befehl gibt `hotel_user` das Recht, die Daten in der DB hot``el zu lesen und zu ändern:

`GRANT SELECT, INSERT, UPDATE, DELETE ON hotel.* TO hotel_user@localhost;
`

![note](../x_res/note.png) Mit `REVOKE` kann der Zugriff auf einzelne Tabellen oder Spalten nicht verboten werden, sondern nur auf die ganze DB.

**Zugriffsrechte betrachten:**

Prüfung der Zugriffsrechte einzelner Benutzer.

` SHOW GRANTS FOR hotel_admin@localhost;
`

<br> 

## Strategie zur Rechtvergabe:

- **Globale Rechte** 

  werden für Admin-Accounts '`root`' bzw. '`admin`' auf alle DBs (\*.\*) vergeben:
  
  ```
  GRANT ALL PRIVILEGES ON *.* TO 'admin'@localhost;
  FLUSH PRIVILEGES;                                  -- Aktivierung -- nie vergessen!
  SHOW GRANTS FOR 'admin'@localhost;
  ```

-  **Lokale Rechte**

   werden für User-Accounts vergeben. Die Rechte werden in **Gruppenrollen** (ab MariaDB Version 10.x) definiert und die Benutzer den Gruppen zugeordnet:
   
   ```
   CREATE ROLE rolle;
   GRANT SELECT, INSERT, UPDATE, DELETE ON db.tbl TO rolle;  -- Rolle wird erzeugt
   GRANT SELECT, ... ON db.tbl TO user@hostname IDENTIFIED BY 'Passw0rd'; -- User wird erzeugt!
   GRANT rolle TO user@hostname;  -- Rolle wird User übertragen                     
   FLUSH PRIVILEGES;                                  -- Aktivierung -- nie vergessen!
   
   SELECT CURRENT_ROLE;  -- Aktive Rollen für aktuellen Benutzer werden angezeigt (Standard NULL - also keine)
   SET ROLE rolle;       -- Rollen dem aktuellen User zuordnen

   ```
   
   [Manual Roles](https://mariadb.com/kb/en/roles_overview/)

   Der Server prüft für jeden einzelnen SQL-Befehl (z.B. `SELECT`, `UPDATE`, `DELETE`, `DROP`), ob der Benutzer ihn ausführen darf. Wenn nicht, bricht die Ausführung des Befehls mit einer Fehlermeldung ab. 

### Zugriffsmatrix

Für jeden Benutzer bzw. Benutzertyp (Gruppe, Rolle) muss genau festgelegt werden, auf welche Daten er in welcher Form zugreifen darf. Dies kann in Form einer **Zugriffsmatrix** erfolgen. 

| Zugriffsmatrix  *DB kunden* |           |       |       |    |   |               |       |       |       |
|-----------------------------|-----------|-------|-------|----|---|---------------|-------|-------|-------|
| *Benutzergruppe =\>*        | *Verkauf* |       |       |    | |  *Management* |       |       |       | 
| Tabellen - Attribute        | S         | I     | U     | D  | | S             | I     | U     | D     |   
| produkte                    | **x**     |       |       |    | | **x**         | **x** | **x** | **x** |   
| personal                    | --        |       |       |    | |               |       |       |       |   
| **-** lohn                  |           |       |       |    | | **x**         |   --  | **x** |  --   |   
| **-** restliche Attribute   |           |       |       |    | | **x**         |   --  | **x** |  --   |   
| rechnungen                  | **x**     |       |       |    | | **x**         | **x** | **x** | **x** |   
| kunden                      | **x**     | **x** | **x** |    | | **x**         | **x** | **x** | **x** |   

*S = Select, I = Insert, U = Update, D = Delete, -- = nicht möglich, - = nicht mehr möglich*

Tab. 6.2: Zugriffsmatrix für eine Datenbank

Die Berechtigungen können für jede Tabelle und sogar für jedes Attribut einzeln vergeben werden. So ist z.B. in Tab. 6.2 das Attribut `lohn` getrennt von den restlichen Attributen eingetragen. I(nsert)- und D(elete)-Berechtigungen können nur für ganze Datensätze vergeben werden.

```
CREATE ROLE verkauf, management;
GRANT SELECT ON kunden.produkte TO verkauf;  
GRANT SELECT (lohn, ..., ...) ON kunden.personal TO verkauf;
...
GRANT SELECT, INSERT, UPDATE, DELETE ON kunden.produkte TO management;
...
GRANT verkauf TO user@host;   --- @user: SET ROLLE nicht vergesssen!
...
```

[YT Erklärvideo zu Rollen](https://www.youtube.com/watch?v=LWWuFMhI6FY)

Einloggen auf freigegebene DB (localhost):

```
>c:\XAMPP\mysql\bin\mysql.exe -p -u user_localhost DB
PASSWORD: ****
``` 

---


![](../x_res/Train_r1.png)

### ![](../x_res/ToDo.png) Zugriffsmatrix umsetzen

[DB Kunden](../Daten/kunden.sql)

1. Erstellen Sie die zwei Rollen `verkauf` und `management`.
2. Erstellen Sie pro Gruppe einen Benutzer
3. Fügen Sie die Rechte gemäss Zugriffsmatrix den Gruppen zu und übertragen sie diese den entsprechenden Benutzern.
4. Untersuchen Sie die Einträge in der Ansicht `mysql.user` und der Datenbasis `mysql.global_priv`.
5. Testen Sie den Zugriff beider Benutzer (SET Role x nicht vergessen!)

6. Löschen Sie die Gruppen und Benutzer wieder.


---

![](../x_res/Train_D1.png)

### ![](../x_res/ToDo.png) Fallbeispiel (ca. 1 Lekt.)

Vertiefung: Rechte auf Tabellen und Spalten (ohne Rollen) 


   [CREATE TABLES für Tutorial Blogeinträge](../Daten/Blogeintraege.sql)
   
   [Tutorial Blogeinträge](https://gridscale.io/community/tutorials/mysql-feinabstimmung-benutzerrechte/)
    
   [(Lösung SQL Befehle)](../Loesungen/Blogeintraege_Loes.sql)
   
<br><br><br>
   
---

# Der pma-User für phpMyAdmin

Nach der Installation von phpMyAdmin wird ein zusätzlicher Benutzer erzeugt „`pma`“. Er hat zwar nur eingeschränkte Rechte, sollte aber ebenfalls mit Passwort versehen werden. 

```
SHOW GRANTS FOR pma@localhost;
+--------------------------------------------------------------------------------------+
| Grants for pma@localhost                                                             |
+--------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `pma`@`localhost`                                              |
| GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON `phpmyadmin`.* TO `pma`@`localhost` |
+--------------------------------------------------------------------------------------+
2 rows in set (0.000 sec)
```

**Auch dieses Passwort muss natürlich in der User-Tabelle eingetragen** und in der `config.inc.php` abgelegt werden. Sie können phpMyAdmin nur dann aufstarten, wenn Sie einen gültigen Benutzernamen mit Passwort haben **und** wenn die Angaben des „`pma`“-Controlusers korrekt sind!

Wer diesen Benutzer löscht, kann phpMyAdmin nicht mehr verwenden. Wenn dies aus Versehen geschehen ist, muss nicht alles neu installiert werden, sondern Sie können den User neu anlegen. Weitere Informationen finden Sie in der Dokumentation: <https://docs.phpmyadmin.net/en/latest/config.html#example-for-signon-authentication>.

`mysql> SET PASSWORD FOR pma@localhost = PASSWORD('irgendwas');`

![](../x_res/pma.png)

Abb. 20: Damit ist auch der Benutzer "pma" passwortgeschützt

Hinweis: Wenn das Passwort für den `pma`-User gesetzt wurde, kann man MySQL evtl. nicht mehr über das XAMPP-Control-Panel herunterfahren. Bei mir hat auch eine Anpassung in der Datei `mysql_stop.bat` im `xampp`-Verzeichnis keinen Erfolg gebracht. Man muss den MySQL-Server also durch Eingabe folgender Zeile manuell herunterfahren:

`C:\...\mysql\bin\mysqladmin --user=pma --password=irgendwas shutdown`

Alternativ kann man natürlich auch die `mysql_stop.bat` modifizieren und danach aufrufen (z. B. über eine Verknüpfung auf dem Desktop).  


 
<br> 
    
---    

# TIPP für Admins: phpMyAdmin Rechteverwaltung

Zugang zu den Benutzerkonten:

![](../x_res/pmaRechte_1.png)

Globale Privilegien setzen / löschen:

![](../x_res/pmaRechte_2.png)

Lokale Privilegien auf DBs setzen / löschen:

![](../x_res/pmaRechte_3.png)

Lokale Privilegien auf Tabellen (oder auch Spalten) setzen / löschen:

![](../x_res/pmaRechte_4.png)

![](../x_res/Hinweis.png) Anhand dieser Aufstellung ist ersichtlich, dass die Rechte auf jede der *'Ebenen'* individuell voneinander gesetzt werden können. Die Rechte sind also nicht in einer hierarchischen Struktur angeordnet, sondern jeder Ebene ist unabhängig. Z.B. könnte eine freigegebene Tabelle benutzt werden. Die nicht freigegebene Datenbasis wird aber nicht angezeigt.

<br> <br> <br> 

---

# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./5T_CheckPoint.md)


---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)