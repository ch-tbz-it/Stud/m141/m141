![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


# ![](../x_res/CP.png) Checkpoint 6.Tag


## Server konfigurieren 

1.  Auf welche Arten können Konfigurationsparameter definiert werden?

    - [ ] mit einem INSERT-Befehl

    - [ ] durch Eintrag auf der Kommandozeile

    - [ ] durch Eintrag in einer Konfigurationsdatei

    - [ ] durch Eintrag in einem Logfile

2.  Welcher Konfigurationsparameter legt fest, wo die Log-Dateien abgelegt werden?

    - [ ] basedir

    - [ ] datadir

    - [ ] log-bin

    - [ ] logdir

3.  Mit welchem Eintrag beginnen die Server-Parameter in der Konfigurationsdatei?

    - [ ] [mysql]

    - [ ] [WinMySQLadmin]

    - [ ] [mysqldump]

    - [ ] [mysqld]

4.  Wozu kann der DB-Client mysqlshow verwendet werden?

    - [ ] Backup erstellen

    - [ ] DB-Schema anzeigen

    - [ ] Verbindung zum DB-Server testen

    - [ ] Inhalt einer Protokolldatei anschauen

5.  Mit welchem Log-File bestimmen Sie den letzten Start des MySQL-Servers?

    - [ ] Error Log

    - [ ] Update Log

    - [ ] Query Log

	 - [ ] Transaction Log
	 
6.  Welcher Eintrag im Konfigurationsfile schaltet die Protokollierung aller User-Login ein?

    - [ ] log-bin

    - [ ] log-slow-queries

    - [ ] log

    - [ ] log-error=C:/log/err.log

7.  Wie restaurieren Sie nach einem Server-Ausfall eine DB vollständig?

    - [ ] Einlesen des letzten Backup

    - [ ] Verwenden der Option --opt beim Erstellen des Backup

    - [ ] Einlesen des Query-Log

    - [ ] Einlesen aller Update-Logs in der richtigen Reihenfolge (mit Hilfe von mysqlbinlog)


8.  Wie erreichen Sie, dass Änderungen in der Konfigurationsdatei wirksam werden?

    ___   
      

9.  Durch welche Daten wird der von einer DB benötigte Speicherplatz bestimmt?

    ___   
      

10.  Wozu wird das Logging (Protokollierung) verwendet?

    ___   
      

11.  In welcher Log-Datei finden Sie, den Anwender, der bestimmte Daten löschte?

    ___   
      

12.  Welche Informationen finden Sie im Slow Query Log?

    ___   
      

13.  Geben Sie für jede Protokolldatei an, wie Sie deren Inhalt kontrollieren.

    ___   
      

14.  Wie beeinflusst der Parameter --opt beim Erstellen eines Backup das Tabellenlocking?

    ___   
      

15. Beschreiben Sie das Vorgehen, um Daten von MySQL nach ORACLE zu migrieren.

    ___   
      

16. Beschreiben Sie eine praktische Anwendung für den READ-Lock.

    ___   
    
    
---



# Optimierung

1.  Welche Möglichkeiten können die Geschwindigkeit eines DB-Server verbessern?

    - [ ] Indexe möglichst vermeiden

    - [ ] Serverparameter einstellen

    - [ ] Transaktionen verwenden

    - [ ] Locks verwenden

2.  Wie werden Daten schneller in eine DB-Tabelle geladen?

    - [ ] durch Komprimieren der Daten vor der Übertragung

    - [ ] durch Verwenden des Parameters --opt beim Erstellen des Backup-Skripts

    - [ ] durch Importieren der Daten aus einer Textdatei

    - [ ] durch Verwenden von vielen INSERT-Befehlen

3.  Was trifft auf den Befehl OPTIMIZE TABLE zu?

    - [ ] entfernt nicht genutzten Speicherplatz aus MyISAM-Tabellendateien

    - [ ] ist auf MyISAM- und InnoDB-Tabellen anwendbar

    - [ ] wird angewendet bei Tabellen, die häufig abgefragt werden

    - [ ] defragmentiert DB-Dateien

4.  Wie finden Sie langsame DB-Abfragen?

    - [ ] mit EXPLAIN SELECT

    - [ ] im Query Log

    - [ ] im Slow Query Log

    - [ ] im Error Log

5.  Welche Aussagen betreffend DB-Optimierung sind korrekt?

    - [ ] Abfragen, die LIKE enthalten, können immer optimiert werden

    - [ ] Indexe beschleunigen Abfragen

    - [ ] Indexe werden allgemein auf Schlüsselattribute gelegt

    - [ ] durch Indexe werden DB-Einträge und -änderungen schneller
    
7.  Wann verwenden Sie den Befehl EXPLAIN?

    - [ ] um Daten schneller in die DB zu laden

    - [ ] immer im Zusammenhang mit SELECT

    - [ ] um langsame Abfragen zu finden

    - [ ] um zu erkennen, wie sich ein Index auf die Geschwindigkeit einer Abfrage auswirkt

7.  Welches sind Gründe für die Verwendung eines Index?

    - [ ] um das Eintragen von Daten in Tabellen bei Unique-Attributen zu beschleunigen

    - [ ] um DB-Abfragen zu beschleunigen

    - [ ] um das Ändern von Daten zu verlangsamen

    - [ ] um einmalige Werte zu gewährleisten
    
8.  Nennen Sie Ziele der DB-Optimierung?

    ___   
      

9.  Was wird optimiert, um die Geschwindigkeit eines DB-Servers zu verbessern?

    ___   
      

10.  Mit welchen 2 prinzipiellen Massnahmen werden DB-Abfragen beschleunigt?

    ___   
      

11.  Beschreiben Sie kurz, wie Sie den Befehl EXPLAIN verwenden.

    ___   
      

12.  Wozu wird der Befehl OPTIMIZE TABLE angewendet?

    ___   
      

13.  Wie werden SELECT-Befehle optimiert?

    ___   
      

14.  Wie viele DB-Tabellen können standardmässig gleichzeitig geöffnet sein?

    ___   
      

15.  Wie schalten Sie den Query Cache ein bzw. aus?

    ___   
      
16. Im Verzeichnis `.\XAMPP\masql\bin` gibt es einige CLI-Tools `mysql*.exe` (oder auch `mariadb*.exe`). Welche Funktion / Verwendungszweck haben diese. Machen Sie eine Liste. <br> (*Tipp: Prompt: =* `Erkläre mir folgende cli-Tools des Mariadb-Servers:
mysql.exe
mysqladmin.exe
mysqlbinlog.exe
mysqlcheck.exe
mysqld.exe
mysqldump.exe
mysqlimport.exe
mysqlshow.exe
mysqlslap.exe
mysql_install_db.exe
mysql_plugin.exe
mariabackup.exe
myrocks_hotbackup`)