![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen  (6.Tag)

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*

**Inhalt**

[TOC]

> ([Lösung 5.Tag](../Loesungen/5T_CP_Loes.md))


---

# Server-Administration

![](../x_res/Learn.png)

## Server-Konfiguration

Nach der Standardinstallation funktioniert der DB-Server mysqld in den meisten Fällen problemlos. 

![Status](./media/Status.png)

Für besondere Wünsche (z.B. andere Sprache oder Zeichensatz, bessere Performance) können beim Start des Servers (mysqld) sowie für das Ausführen von Admin-Programmen (z.B. mysql) Parameter (Optionen) angegeben werden.

Konfigurationsparameter werden auf der Kommandozeile oder in Konfigurationsdateien festgelegt.

**a) Parameter auf der Kommandozeile angeben**:

Der folgende CMD-Befehl startet den DB-Server mit der Option für Fehlermeldungen in Deutsch.

`C:> mysqld --language=german`

Der folgende Befehl startet den Monitor mit Benutzernamen und weiteren Parametern.

`C:> mysql --user=kunde --i-am-a-dummy --silent`

**b) Parameter in die Konfigurationsdateien eintragen (my.ini, my.cnf)**:

Da der DB-Server meistens als Dienst gestartet wird, ist es nicht möglich, direkt Parameter anzugeben. Darum werden Server-Parameter fast immer in einer Konfigurationsdatei angegeben. Lese-Reihenfolge siehe Tag 2!

`C:\...\mysql\data\my.ini`   *oder*  `C:\...\my.cnf`

Es gilt der zuletzt gelesene Wert. Werte auf der Kommandozeile haben Vorrang vor Einträgen in den Konfigurationsdateien. Es gelten die gleichen Angaben wie auf der Eingabezeile, aber ohne `--`.

```
# Server-spezifische Einträge

[mysqld]

language=german
```

Statt beim Ausführen von Administrationsprogrammen immer wieder die gleichen Optionen anzugeben, können diese ebenfalls in eine der Konfigurationsdateien eingetragen werden.

```
# Monitor-spezifische Einträge

[mysql]

user=meier

silent
```

### MySQL-Konfigurationsparameter

Die Konfigurations-Optionen werden mit einem Texteditor oder mit der Workbench in die Konfigurationsdatei eingetragen. Angezeigt werden die Serverparameter mit dem Befehl

`mysqld --help`

Änderungen in den Konfigurationsdateien werden erst nach einem Neustart des betroffenen Programms wirksam. Für Server-spezifische Optionen muss der Server neu gestartet werden. **Auch unter Windows sind in den Konfigurationsdateien Pfade wie bei UNIX mit / anzugeben (statt mit \\).**

![caution](../x_res/caution.png) Bei einem falschen Eintrag in einer Konfigurationsdatei (z.B. Tippfehler, - statt \_) kann der Server nicht gestartet werden!

### Systemvariablen

Diverse Statusinformationen des DB-Servers sind als Systemvariablen zugänglich. Angezeigt werden diese mit dem Befehl `SHOW VARIABLES`, z.B. Loggingeinstellungen:

```
mysql> SHOW VARIABLES LIKE '%log%';
```

Mit phpMyAdmin lassen Sie sich auch anzeigen:
![Variables](./media/Variables.png)

![](../x_res/Train_R1.png)

Falls Sie folgende Aufgaben oben nicht schon gemacht haben ...

1.  Konfigurationsparameter anzeigen

    Führen Sie im DOS-Fenster den Befehl mysqld --help aus. Bestimmen Sie aus der Vielzahl der angezeigten Server-Parameter vier Ihnen wichtig erscheinende. Notieren Sie die gewählten Parameter mit einer kurzen Beschreibung.

2.  Sprache der Fehlermeldungen ändern

    Schauen Sie im Verzeichnis `mysql\share\` nach, welche Sprachen unterstützt werden und tragen Sie mit einem Texteditor eine davon mit dem entsprechenden Parameter in der Konfigurationsdatei my.ini ein.

    Stoppen Sie den Server und starten Sie ihn erneut. Prüfen Sie im Monitor und im Control Center, dass Eingabefehler eine Fehlermeldung in der gewählten Sprache ergeben.

3.  Systemvariablen anzeigen

    Lassen Sie sich mit dem entsprechenden Befehl alle Systemvariablen, die etwas mit dem Logging zu tun haben anzeigen.

    Bestimmen Sie für jede Variable die Bedeutung. Benutzen Sie dazu das MySQL-Manual.


---

## Logging

![](../x_res/Learn.png)

Logging bezeichnet die Protokollierung von Änderungen in der Datenbank.

| |  Wozu Logging ?                                                                                 |
|----------------|----------------------------------------------------------------------------------|
| **Monitoring**     | Protokoll von Start, Shutdown und Fehlern.  Wer hat wann welche Daten verändert? |
| **Sicherheit**     | Datenwiederherstellung zwischen regulären Backups                                |
| **Optimierung**    | Aufzeichnung von aufwendigen DB-Abfragen                                         |
| **Replikation**    | Logging des Master-Rechners für die DB-Synchronisierung                          |
| **Transaktionen**  | Durchführen von Transaktionen nach einem DB-Absturz                              |

Tab. 8.1: Ziele des Logging

MySQL kann Änderungen in der Datenbank wahlweise im Textmodus oder binär protokollieren. Die resultierenden Logging-Dateien können beispielsweise für inkrementelle Backups oder zur Replikation mehrerer Datenbank-Server verwendet werden. 

Wenn Sie mit Transaktionen arbeiten, müssen Sie unbedingt binäres Logging verwenden, weil nur diese Variante Transaktionen korrekt behandelt. Wenn Sie das Logging dagegen im Textmodus durchführen, werden auch solche Kommandos protokolliert, die durch ROLLBACK widerrufen werden. Binäres Logging wird durch die Option `log-bin` in der Optionsdatei `my.ini` aktiviert.

Nachteile des Logging sind **Verlangsamung** des DB-Servers und zusätzlicher **Platzverbrauch** auf der Festplatte. Deshalb sind in der Standardeinstellung ausser dem Error-Log alle Protokolle ausgeschaltet.

![](../x_res/Log-Dateien.png) 

Abb. 29: Log-Dateien mit Konfigurationsparametern und Verarbeitungs-Tools

**Logdateien bei XAMPP Hostname:\<WIN10_64\>:** <br>
![Log Dateien XAMPP](./media/Logs.jpg)


![note](../x_res/note.png) Änderungen der Logging-Parameter werden erst nach dem Neustart des Servers wirksam.

![Hinweis](../x_res/Hinweis.png)Für maximale Geschwindigkeit und Sicherheit sollten die Log-Dateien auf einer anderen Festplatte gespeichert werden als die DB-Dateien.

### Binärdateien lesen mit mysqlbinlog.exe

Die meisten Logging-Protokolle werden in Binärdateien gespeichert. Sie können diese zwar mit einem normalen Editor öffnen, der Inhalt ist aber unverständlich, da binär codiert. Im MySQL-Binärverzeichnis gibt es ein Programm zur Anzeige der Binärlogdateien: mysqlbinlog.exe. Starten Sie dieses Programm und geben die Datei als Parameter mit und der Dateiinhalt wird Ihnen auf dem Konsolenfenster ausgegeben.

### Error Log (mysql_error.log)

Das Fehlerprotokoll protokolliert jeden *Start* und *Shutdown* des MySQL-Servers sowie alle *Fehlermeldungen* des Servers in Textform.

**a) Error Log neu setzen**:

```
# Error Log-Datei festlegen (Default: <basedir>\bin\mysql_error.log)

log-error=C:/log/mysql_error.log
```

**b) Error Log kontrollieren**:

Jeder Eintrag ist mit Datum yymmdd und Zeit hh:mm:ss versehen.

`C:\>TYPE C:\..\mysql\bin\mysql_error.log`

![note](../x_res/note.png) Das Error Log kann nicht ausgeschaltet werden.

### Binäres update Logging

Der Parameter `#log-bin=mysql-bin` ist in der Konfigurationsdatei von MySQL standardmässig auskommentiert. Wenn Sie das Kommentarzeichen entfernen, können Sie den Dateinamen anpassen und ab dem nächsten Serverstart wird eine Datei mit der Endung `*.000001` angelegt, die alle Anweisungen enthält, die Daten verändern. Normale Select-Anweisungen werden also nicht abgespeichert.

Bei folgenden Bedingungen wird eine neue Logdatei erstellt und die Extension um eins hochgezählt:

-   beim Neustart des DB-Servers
-   beim Erreichen der Maximalgrösse, die mit dem Parameter `max_binlog_size` definiert wird
-   durch den Befehl `FLUSH LOGS`

Zusätzlich wird eine Indexdatei mit der Endung `.index` erzeugt, die alle Dateinamen der bisherigen Binärlogdateien aufzeichnet.

Seit der MySQL-Version 5.0 ersetzt das Binärlogging das vorherige Update-Logging.

Gemäss MySQL-Manual 5.1 ist die Leistungseinbusse des Datenbankservers durch das aktivierte Binär-Logging nur 1%. Für einige Anwendungen ist es ohnehin essentiell nötig: **Transaktionen**, **Replikationen**, **Recovery** nach Systemabstürzen …

### Binärlog-Datei anzeigen:

Die Einträge in der Log-Datei sind binär dargestellt. Das heisst, dass wir mit einem gewöhnlichen Text-Editor keine lesbare Darstellung bekommen. Um den Inhalt der Datei anzuzeigen benötigen Sie den speziellen Client mysqlbinlog, der sich im mysql/bin-Verzeichnis befindet.

`C:\...\mysql\bin>mysqlbinlog -u root -p C:\...\MySQL\data\mysql-bin.000001`

### General Query Log (\<host\>.log)

Das Login- und Operationssprotokoll zeichnet auf, welcher Benutzer welche Daten liest oder ändert. Dazu wird *jeder Verbindungsaufbau und jeder Befehl* an den DB-Server aufgezeichnet. Dieses Protokoll kann nicht für die Wiederherstellung von Daten verwendet werden und wird sehr schnell sehr gross!

`mysql\> SET GLOBAL general_log=1;`

### Slow Query Log (\<host\>-slow.log)

Dieses Protokoll zeigt Datenbankabfragen, die den grössten Aufwand verursachen. Die Analyse dieser Abfragen kann Hinweise für die Verbesserung der *Server-Performance* geben.

### Transaktions-Log (ib\_logfile1, -2, ...)

In den InnoDB-Logging-Dateien wird jede Änderung in *InnoDB-Tabellen* aufgezeichnet. (s. Tag 3 ° ). Dies ermöglicht grosse Transaktionen sowie die Wiederherstellung nach einem Server-Absturz.

Mit `COMMIT` wird eine Transaktion vorerst nur ins Transaktions-Log geschrieben. Die geänderten Tablespace-Seiten werden aus Geschwindigkeitsgründen erst nach und nach auf die Festplatte übertragen. Kommt es in der Zwischenzeit zu einem Absturz, wird der Tablespace mit Hilfe der Log-Dateien wiederhergestellt.  

Hinweis: `maria_log` (mariaDB-Engine für Transaktionen!)

**Transaktions-Log konfigurieren**:

Die Log-Dateien werden der Reihe nach befüllt. Wenn die letzte Datei voll ist, beginnt der *InnoDB-Tabellentreiber*, wieder Daten in die erste Log-Datei zu schreiben.

![note](../x_res/note.png) Transaktions Log-Dateien können nicht mit dem Texteditor kontrolliert werden. Sie sind nur im Server-Betrieb erforderlich und werden nach dem korrekten Server-Shutdown nicht mehr benötigt.

![](../x_res/Train_R1.png)

Falls Sie folgende Aufgaben oben nicht schon gemacht haben ...

1.  Error Log benutzen

    Suchen Sie auf Ihrem Rechner das Fehlerprotokoll. Öffnen Sie es mit einem Texteditor und untersuchen und dokumentieren Sie die letzten 4 Einträge.

2.  Update Log konfigurieren

    Schalten Sie in der Konfigurationsdatei die Protokollierung in binärer Form ein (ohne Pfadangabe). Starten Sie den Server erneut, damit die Änderung wirksam wird. Ändern Sie dann in einer Tabelle der hotel-DB mit einem SQL-Befehl einen Datensatz und kontrollieren Sie den entsprechenden Eintrag in der Log-Datei. Verwenden Sie dazu das Programm mysqlbinlog.

3.  Query Log benutzen

    Schalten Sie in der Konfigurationsdatei diese Protokollierung ein.

    Führen Sie nach dem Neustart des Servers im Monitor einige Befehle aus und kontrollieren Sie die Einträge in der Log-Datei.

4.  Transaktions-Logdateien konfigurieren

    Aus Geschwindigkeitsgründen sollen die InnoDB-Logdateien vom Standardverzeichnis C:\\mysql\\data ins neue Verzeichnis D:\\log verlegt werden.

    Erstellen Sie das neue Verzeichnis und stoppen Sie den Server.

    Machen Sie den entsprechenden Konfigurationseintrag und löschen Sie die alten Logdateien.

    Starten Sie den Server und kontrollieren Sie den entsprechenden Start-Eintrag im Error-Log.

    Prüfen Sie, dass die Log-Dateien im neuen Verzeichnis erstellt wurden.

<br><br><br>

---

## Das Konzept von Backup und Restore

![](../x_res/Learn.png)

Während des Betriebs eines DB-Servers wird regelmässig ein Backup erstellt. Durch einen Crash können sämtliche Daten verloren gehen.

![](../x_res/Backup.png)

Abb. 30: Backup, Logging und Restore

**Restore** (Recovery): Zuerst wird das letzte Backup eingelesen, anschliessend der Reihe nach alle seitdem erstellten Update Log-Dateien. Damit lässt sich der Zustand vor dem Crash wieder herstellen.

| Backup- und Restore-Befehle                     |         |                         |
|-------------------------------------------------|---------|-------------------------|
| `mysqldump [optionen] db [tb1 tb2 ...] > datei` | Backup  | allgemeine Syntax       |
| `mysqldump --help`                              | "       | Anzeigen aller Optionen |
| `mysqldump hotel > backup.sql`                  | "       | ganze Datenbank         |
| `mysqldump hotel person buchung > bkp.sql `     | "       | 2 Tabellen              |
| `mysqldump --opt ...`                           | "       | optimale Einstellung    |
| `mysql [optionen] db < datei`                   | Restore | allgemeine Syntax       |

Tab. 8.2: Beispiele von Befehlen für Backup und Restore

**Backup mit mysqldump erstellen**:

Das Programm erzeugt pro Tabelle einen CREATE TABLE-Befehl um die Tabelle samt Indizes zu erzeugen, sowie die INSERT-Befehle, um die Daten einzutragen. Mit mysqldump können einzelne Tabellen oder ganze DBs gesichert werden.

Damit bei der Ausgabeumleitung in eine Datei die Enter password-Zeile nicht in die Zieldatei umgeleitet wird, kann das Passwort mit --password=x direkt eingegeben werden.

Die Option `--opt` fasst mehrere Optionen zusammen und bewirkt eine *optimale Einstellung*.

`C:\>mysqldump --password=pwd --opt firma > backup.sql`

**Integrität des Backup mit READ Lock gewährleisten**:

Während des Backups dürfen die Daten nicht durch einen anderen Client verändert werden. Ein READ LOCK für die betroffenen Tabellen wird mit dem Parameter `--lock-tables` von mysqldump erreicht.

`C:\>mysqldump --lock-tables firma > backup.sql`

![note](../x_res/note.png) Ist auch nur eine Tabelle der DB von einem anderen Client mit einem `WRITE LOCK` blockiert, so wird das Backup erst nach der Freigabe (`UNLOCK TABLES`) gestartet.

**Datenbank wiederherstellen (Restore)**:

Dafür wird der Monitor verwendet.

`C:\>mysql.exe -u ... firma < backup.sql`

**Datenbank aus einem Update-Log wiederherstellen**:

Die Rekonstruktion der DB beginnt mit dem Einlesen des letzten vollständigen Backup. Anschliessend werden die Update Log-Dateien benötigt, die seit dem letzten Backup entstanden sind. Die darin enthaltenen SQL-Befehle werden mit Hilfe von `mysqlbinlog` gelesen und mit **\|** (=pipe) an mysql übergeben.

```
C:\>mysqlbinlog <host>-bin.000 | mysql.exe -u ...

C:\>mysqlbinlog <host>-bin.001 | mysql.exe -u ...
```

![note](../x_res/note.png) Die älteste Log-Datei wird zuerst ausgeführt.

## Import und Export

Import und Export bezeichnen die Übertragung von Daten zwischen Textdatei und DB-Tabelle.

![](../x_res/Export.png) 

Abb. 31: Export und Import für die Übertragung von Daten

Damit die Datenstruktur bei der Umwandlung in die DB-Tabelle richtig interpretiert wird, müssen die einzelnen Felder und Zeilen markiert werden. Die **Trennzeichen (Delimiter)** sind so zu wählen, dass keine Verwechslung mit dem Textinhalt möglich ist.

| Import- und Export-Befehle u. Parameter |                                                       |
|-----------------------------------------|-------------------------------------------------------|
| `SELECT .. INTO OUTFILE`                  | exportiert das Abfrage-Ergebnis in eine Text-Datei    |
| `TRUNCATE TABLE tb;`                      | löscht den gesamten Tabelleninhalt der Tabelle        |
| `LOAD DATA INFILE `                       | importiert Inhalt einer Textdatei in eine DB-Tabelle  |
| `FIELDS TERMINATED BY ';'`                | z.B. ;, trennt einzelne Felder (Feldtrennzeichen)     |
| `ENCLOSED BY '"' `                        | z.B. ", umschliesst einzelne Textfelder               |
| `LINES TERMINATED BY '\\r\\n' `           | Zeilentrennzeichen für Windows-Textdateien            |

Tab. 8.3: Beispiele von Import- und Export-Befehlen und Definition von Trennzeichen (Delimiter)

![note](../x_res/note.png) Textimport ist oft problematisch, z.B. wegen Datum-Formatierungen. Warnings \> 0 zeigen *Importfehler*, die dann evt. mit Hilfe des `SELECT`-Befehls gefunden werden. Es ist darauf zu achten, dass die importierten Daten die referenzielle Integrität der Datenbank erfüllen.

**Export mit SELECT ... INTO OUTFILE**:

Es handelt sich um einen gewöhnlichen `SELECT`-Befehl mit Ausgabe in eine Textdatei. Die Trennzeichen werden mit den *Exportoptionen* vor dem `FROM`-Teil angegeben.

Der `SELECT`-Befehl kann zusätzlich beliebige Auswahl- und Sortieranweisungen enthalten.

## Speicherbedarf abschätzen

Bei der Planung eines DB-Servers ist der voraussichtlich benötigte Speicherplatz zu berücksichtigen. Neben den eigentlichen Benutzerdaten (Tabellendaten) wird auch Speicherplatz für Indexe (Indextabellen) und die Systemverwaltung (DB- und Tabellenbeschreibungen, Systemkatalog, User- und Zugriffsverwaltung) benötigt.

Das Abschätzen des benötigten Speicherplatzes ist ein ungenaues Unterfangen wegen Tabellenspalten variabler Länge (`VARCHAR`), nicht bekannter Anzahl und Grösse der Log-Dateien, temporären Bereichen für bestimmte SQL-Befehle (z.B. `ORDER BY`), Datenträgerfragmentierung etc.

*Speicherbedarf pro Tabelle (MyISAM)*

| Nutzdaten (\*.MYD)   | Anzahl\_Datensätze \* [SUM (Bytes\_pro\_Attribut1..n) + 5] |
|----------------------|---------------------------------------------------------|
| Indizes (\*.MYI)     | Anzahl\_Schlüssel \* [(Schlüssellänge + 4) / 0.67]       |
| Systemdaten (\*.FRM) | 8500 + (Anzahl\_Attribute \* 40)                         |

Tab. 8.1: Formeln für die ungefähre Berechnung des Speicherverbrauchs pro Tabelle

`Bytes_pro_Attribut` und Schlüssellänge werden dem MySQL-Manual entnommen. Die Indextabellen werden in Blöcken (d.h. Mehrfachen) von 1024 Byte abgespeichert. Für `Anzahl_Schlüssel` kann die Anzahl Datensätze eingesetzt werden.

Die Belegung des variablen Datentyps `VARCHAR` ergibt sich als Mittelwert einer Anzahl Test-Einträge.

[Anleitng zur Ermittung des Speicherbedarfs](https://404media.de/sql/wie-du-die-groesse-einer-datenbank-und-tabelle-in-mysql-sql-ermittelst)

**Anzeige des aktuellen Speicherbedarfs:**

![](./media/Speicher_Optimieren.png)

<br> 

![](../x_res/Train_R1.png)

Falls Sie folgende Aufgaben nicht schon gemacht haben ...

1.  Blockiertes Backup freigeben

    Blockieren Sie als DB-Client A im Monitor eine Tabelle der DB mybooks mit einem WRITE LOCK. Starten Sie als Client B in einem zweiten DOS-Fenster ein Backup der gleichen DB. Wie wird erreicht, dass das Backup ausgeführt wird?

2.  DB nach Crash wiederherstellen
	-   Mit Hilfe des vorhandenen Backup und der Update-Log soll nach einem Totalverlust die DB wiederhergestellt werden.
	-   Prüfen Sie, dass das Update-Log eingeschaltet ist; andernfalls schalten Sie es ein und starten Sie den Server erneut.
	-   Erstellen Sie ein Backup der gesamten DB hotel und eröffnen Sie neue Log-Dateien mit dem SQL-Befehl FLUSH LOGS.
	-   Tragen Sie in die DB je einen neuen Autor und Verlag ein.
	-   Simulieren Sie den Totalverlust der DB, indem Sie den Server stoppen und das Verzeichnis der Datenbank, d.h. C:\\mysql\\data\\hotel löschen.
	-   Starten Sie den Server und regenerieren Sie die DB mittels Backup und Update-Log. Bestimmen Sie dazu die Update-Logdatei, die seit dem Backup bis zum Crash entstanden ist. Nur diese ist für das Restore zu verwenden. Prüfen Sie vor dem Ausführen deren Inhalt.
	-   Kontrollieren Sie, ob in der regenerierten DB die von Ihnen eingetragenen 2 Datensätze ebenfalls vorhanden sind.

3.  mysqldump –opt

    Untersuchen Sie mit *mysqldump --help* oder mit Hilfe des MySQL-Manuals, warum der Parameter *--opt* eine optimale Einstellung für Backups ergibt.

<br> <br> <br> <br> 

---

# Optimierung

![](../x_res/Learn.png)

Die Optimierung des Betriebs einer Datenbank ist eine komplizierte Aufgabe, weil sie ein umfassendes Verständnis des gesamten DB-Systems voraussetzt.

| Wozu Optimierung ?       |                                               |
|--------------------------|-----------------------------------------------|
| **Performance** verbessern   | schnellere Ausführung von SQL-Befehlen        |
| **Speicherplatz** einsparen  | schnellere Übertragung von/auf die Festplatte |
| **Portabilität** ermöglichen | Übertragen der DB auf einen anderen Server    |

Tab. 9.1: Ziele der Optimierung

Das wichtigste, um ein System schnell zu machen, ist das grundlegende Design. Ausserdem ist es wichtig zu wissen, was das System macht und welches mögliche *Flaschenhälse* sind. Engpässe sind z.B.:

-   *Suchvorgänge auf der Festplatte*: Die Festplatte benötigt Zeit, um die angeforderten Daten zu finden. Pro Sekunde können etwa 1000 Suchvorgänge durchgeführt werden. Eine Möglichkeit der Optimierung besteht darin, Daten auf mehrere Platten zu verteilen.

-   *Lesen von / Schreiben auf Festplatte*: In der richtigen Position kann die Festplatte etwa 10 bis 20 MB pro Sekunde übertragen. Dies ist leichter zu optimieren als Suchvorgänge, da von mehreren Festplatten parallel gelesen werden kann.

| Was wird optimiert? |                                                    |
|---------------------|----------------------------------------------------|
| **Datenbankstruktur**   | minimaler Speicherplatz, Index-Verwendung          |
| DB-**Abfragen**         | Abfragen mit EXPLAIN analysieren                   |
| **Locks**               | Geschwindigkeit erhöhen durch Sperren von Tabellen |
| DB-**Server**           | Serverparameter einstellen                         |

Tab. 9.2: Objekte der Optimierung


## Laden von Daten, Tabellenspeicherplatz

### Laden von Daten optimieren

1.  **SQL-Skripts optimieren mit --opt**:

Diese `mysqldump`-Option ergibt ein Skript für das schnellstmögliche Einlesen in einen MySQL-Server (s.a. Kap. 8.3, Seite 46). Der folgende Befehl erstellt ein SQL-Skript für eine einzelne Tabelle der DB mybooks.

`C:\> mysqldump --opt hotel person > backup.sql`

Der Parameter `--opt` umfasst die Optionen

| Parameter           | Kommentar |
|-----------------|--------------------------------------------------------------------------|
| `quick`           | beschleunigt das Erstellen durch Vermeiden des Zwischenspeicherns im RAM |
| `add-drop-table`  | fügt vor jedes CREATE TABLE einen DROP TABLE-Befehl ein                  |
| `add-locks`       | fügt für schnelles Einlesen LOCK und UNLOCK TABLE-Befehle ein            |
| `extended-insert` | erzeugt für schnelles Einlesen wenige INSERT mit mehreren Datensätzen    |
| `lock-tables`     | führt ein LOCK TABLE READ aus (Datenintegrität)                          |

In den meisten Fällen ist dies eine optimale Einstellung.

**a) Schnelleres Laden von Daten mit LOAD DATA INFILE**:

Viel schneller als das Laden von Daten mit vielen INSERT-Befehlen ist das Importieren der Daten aus einer Textdatei (s.a. Kap. 8.4, Seite 48), z.B.

```
mysql> LOAD DATA INFILE 'c:/person.txt' INTO TABLE person
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';
```

### Tabellenspeicherplatz optimieren (nur MyISAM)

**b) OPTIMIZE TABLE verwenden**:

Werden aus einer Tabelle Datensätze gelöscht oder Änderungen durchgeführt, so ist die Datenbankdatei grösser als notwendig und die Daten sind über die ganze Datei verstreut, was den DB-Zugriff verlangsamt.

`OPTIMIZE TABLE` entfernt nicht genutzten Speicherplatz aus einer Tabellendatei und sorgt dafür, dass zusammengehörende Daten eines Datensatzes auch zusammen gespeichert werden.

`mysql> OPTIMIZE TABLE person;`

Bei Tabellen, deren Inhalt sich häufig ändert (viele DELETE- und UPDATE-Befehle), sollte der Befehl regelmässig ausgeführt werden. Damit wird die Datei verkleinert und der Zugriff beschleunigt. 

(Siehe auch oben **phpMyAdmin** Speicherplatz-Fenster oder weiter unten Index-Fenster: `--> Optimiere Tabelle`)


## Einsatz von Indextabellen

Eine Suche ist grundsätzlich nur dann effizient, wenn sie in einem *sortierten Suchraum* arbeitet. Das beste Beispiel ist das Telefonbuch. Wären die Personen nicht alphabetisch sortiert, bliebe uns bei der Suche nach einer Telefonnummer nicht anderes übrig, als das Telefonbusch **sequentiell** durchzulesen, bis wir auf die gesuchte Person stossen – im schlimmsten Fall also bis zum Ende.

Um in grossen *unsortierten Listen* einen gewünschten Wert zu finden müssen wir also die lineare oder sequentielle Suche anwenden. Auch wenn wir nach einem Wert suchen, der gar nicht vorhanden ist, können wir das erst am Ende des Suchraumes feststellen. Aus diesem Grund ist es einfacher, wenn wir Listen – oder konkret in Datenbanken dann Attribute – sortieren, um schnellere Verfahren anwenden zu können.
  

Bei der **binären Suche** wird eine sortierte Liste vorausgesetzt. Damit halbiert sich bei jedem Zugriff der Suchraum. Wir benötigen bei N Elementen 2log N Zugriffe. Wenn unser Suchraum aus 16 (=24)Werten besteht, benötigen wir also maximal 4 Zugriffe (vorherige „Glückstreffer“ ausgenommen). Mit nur einem Zugriff mehr, können wir bis zu 32 Werte abdecken. Allerdings benötigen wir auch für 17 Werte maximal 5 Zugriffe. Für dieses Suchverfahren wird auch noch der Begriff „logarithmische Suche“ verwendet.

Eine Erweiterung der binären Suche ist die **Interpolationssuche**. Bei der binären Suche wird der Suchraum jedes Mal genau in der Mitte geteilt. Die Interpolationssuche berücksichtigt noch die Grösse des gesuchten Wertes in Bezug auf den Suchraum. Es vergleicht also den grössten und den kleinsten Wert, geht von einer gleichmässigen Verteilung der Daten aus und teilt den Suchraum jetzt im Verhältnis zu dem Suchwert. Konkret greift es bei einem sehr hohen Wert bereits im „wahrscheinlicheren“ hinteren Bereich des Suchraumes zu. Jeder erfolgreiche Zugriff schränkt den Suchraum also um mehr als die Hälfte ein. Mit der zusätzlichen Berechnung des Zugriffselementes geht aber ebenfalls Rechenzeit verloren. Daher ist die Interpolationssuche nur bei grossen Suchräumen effizienter.

Ein leicht verständliches Zahlenbeispiel finden Sie bei Wikipedia: [Interpolationssuche](https://de.wikipedia.org/wiki/Interpolationssuche).

Wenn wir aber Tabellen mit mehreren Attributen haben, auf die wir häufig zugreifen, können wir ja trotzdem nur nach einem Attribut sortieren. Die Lösung ist die Verwendung von **Indextabellen**, die wir pro Attribut definieren können. Für jedes indizierte Attribut wird eine zusätzliche „Hilfstabelle“ angelegt, die nur das – jetzt *sortierte Element* – und einen Schlüssel auf die Originaltabelle enthält. 

![Index](./media/Index.png)

Diese Indextabellen werden vom Datenbankmanagementsystem verwaltet. Zugriffe und Updates liegen also weder beim Anwender noch beim Datenbankentwickler.


![Index_Zeit_Search_Sort](../x_res/Index_Zeit_Search_Sort.png)

Abb.: Zeitdauer von Such- und Sortieroperationen mit und ohne Index

![Index_Zeit_Search_Sort_klein](../x_res/Index_Zeit_Search_Sort_klein.png)

Abb.: Zeitdauer von Such- und Sortieroperationen mit und ohne Index bei kleinen Mengen

Bei wenigen Datensätzen ist die Zeitdauer gleich oder sogar grösser und daher nicht 'spürbar'. 

Dadurch wird die Datenbank etwas grösser (auch die Indextabellen müssen gespeichert werden). 

![Index_Insert](../x_res/Index_Insert.png)

Die Einfügeoperationen (INSERT) sind etwas aufwendiger, da auch die Indextabellen aktualisiert werden müssen. Aus dem gleichen Grund werden Aktualisierungen, die indizierte Attribute betreffen aufwendiger. Der Zeitvorteil liegt aber bei den Abfragen. Da aber in der Praxis die Antwortzeit bei Abfragen viel kritischer ist als die Antwortzeit von Einfügeoperationen, sind die Nachteile aktzeptierbar.


## Abfrage- und Indexoptimierung

Die Performance einer Datenbank lässt sich erst dann abschätzen, wenn die Datenbank genügend Testdaten enthält. Eine Datenbank mit einigen Hundert Datensätzen befindet sich nach den ersten 3 Abfragen meistens vollständig im RAM, so dass alle Abfragen mit oder ohne Index sehr schnell beantwortet werden. Eine Abfrage-Optimierung lohnt sich nur dann, wenn die Tabellen über 10000 Datensätze enthalten oder die Gesamtgrösse der DB die RAM-Grösse auf dem DB-Server überschreitet.

![Abfragen](./media/Abfragen.png)

### Langsame Abfragen in der Slow Query Log (host-slow.log)

**Slow Query Log kontrollieren (host-slow.log)**:

Dieses Protokoll zeichnet alle langsamen Abfragen samt Ausführungszeit in Textform auf.

`C:\>TYPE C:\...\mysql\data\al27785-slow.log`

### EXPLAIN SELECT verwenden (Ausführungsplan)

Wird einem SELECT-Statement das Schlüsselwort `EXPLAIN` vorangestellt, so erklärt MySQL, wie das `SELECT` ausgeführt würde. Die angegebenen Informationen zeigen, wie und in welcher Reihenfolge die Tabellen verknüpft werden (*Ausführungsplan*). Mit Hilfe von `EXPLAIN` lässt sich erkennen, wo Indexe hinzugefügt werden müssen.

Die zu untersuchende Abfrage ermittelt die Anzahl Titel aller Verlage, deren Name mit 'A' beginnt.

```SQL
mysql> EXPLAIN SELECT COUNT(*)
FROM buchung, person
WHERE buchung.PersID = person.PersID;
```

Die `table`-Spalte zeigt die Tabellen in der Reihenfolge, in der sie gelesen würden. Die `key`-Spalte gibt den Index an, den MySQL benutzen würde und ist NULL, wenn kein Index verwendet wird. Die `rows`-Spalte gibt die Anzahl von Zeilen an, die für die Abfrage voraussichtlich untersucht werden müssen.

![note](../x_res/note.png) Einen Anhaltspunkt über den Suchaufwand erhält man durch Multiplizieren aller Werte in der *rows*-Spalte. Das Resultat sollte möglichst klein sein.

| Optimierungshinweise                                                       |
|----------------------------------------------------------------------------|
| Indexe beschleunigen die Abfrage aber verlangsamen Änderungen              |
| Index auf Primär- und Fremdschlüssel                                       |
| Indexe auf Attribute, die häufig sortiert werden                           |
| NOT und \<\> können nicht optimiert werden                                 |
| Abfragen mit LIKE sind nur optimierbar, wenn % nicht am Musteranfang steht |
| Abfragen mit Funktionen sind nicht optimierbar                             |

Tab. 9.3: Abfrage-Optimierung mit Indizes

Ausführliche Tipps für das Optimieren enthält das MySQL-Manual. Hier eine kleine Hilfe:

![Index_Entcheidung](../x_res/Index_Entcheidung.png)

## Server-Tuning

Server Tuning bezeichnet die optimale *Konfiguration* des DB-Servers, so dass die Hardware möglichst gut genutzt und die SQL-Befehle so schnell wie möglich ausgeführt werden.

![note](../x_res/note.png) Im allgemeinen lohnt sich Server-Tuning nur bei sehr grossen Datenmengen (GByte) und sehr vielen DB-Zugriffen pro Sekunde.

**Überwachungsfenster**:

![Performance](./media/DB-Performance.png)

![Users](./media/UserConnections.png)


### Optimale Speichernutzung

Beim Start des DB-Servers wird ein Teil des Hauptspeichers für bestimmte Aufgaben reserviert, z.B. als Platz zum Sortieren von Daten.

| Wichtige Speicherparameter |                                                               |
|----------------------------|---------------------------------------------------------------|
| `key_buffer_size`            | für Indizes reservierter Speicher (Default: 8M)               |
| `table_cache`                | maximal Anzahl geöffneter Tabellen (Default: 64)              |
| `sort_buffer`                | Buffergrösse zum Sortieren oder Gruppieren (Defaut: 2M)      |
| `read_buffer_size`           | Speicher für sequentielles Lesen (Default: 128K=131072 Bytes) |

Tab. 9.4: Wichtige Konfigurationsparameter für die Speicherverwaltung

### Query Cache

Die Aufgabe des *Query Cache* besteht darin, die Ergebnisse von SQL-Abfragen zu speichern. Wenn später exakt dieselbe Abfrage wieder durchgeführt wird, so kann das fertige Ergebnis verwendet werden.

**a) Query Cache konfigurieren**:

```
\# Query Cache einschalten

query_cache_size = 32M.       <<<<<<<<<<<

# Query Cache Modus 0=Off, 1=On (Default), 2=Demand

query_cache_type = 1

# max. Grösse für eine Abfrage

query_cache_limit = 50K
```

Damit werden 32 MByte RAM reserviert. Es werden nur Abfrage-Resultate gespeichert, die weniger als 50 kByte benötigen, was vermeidet, dass grosse Ergebnisse alle anderen aus dem Cache verdrängen.

**b) Query Cache verändern**:

Der Query Cache kann für eine Verbindung speziell eingestellt, z.B. ausgeschaltet werden.

`mysql> SET query_cache_type=0;`

**c) Query Cache-Status abfragen**:

Mit SHOW STATUS; werden die Werte verschiedener Statusvariablen des Query Cache angezeigt.

`mysql> SHOW STATUS;`

**d) Query Cache leeren**:

Mit folgendem Befehl werden alle Einträge im Cache gelöscht.

`mysql> RESET QUERY CACHE;`

<br>


![](../x_res/Train_D1.png)

Falls Sie folgende Aufgaben oben nicht schon gemacht haben ...

1.  Wählen Sie eine Tabelle mit vielem Einträgen aus, die Sie letzthin öfters benutzt haben. Optimieren Sie diese Tabelle (phpMyAdmin >> Struktur >> Optimiere Tabelle)

2.  Erstellen und löschen Sie einige Indizes auf verschiedene Tabellen.


3.  Konfigurieren und verändern Sie den Query Cache.


4. Studieren Sie den phpMayAdmin-Ratgeber.
   - Welche Kategorieren gibt es?
   - Welche Vorschläge macht er?
   ![Ratgeber](./media/Ratgeber.png)

    


<br><br><br>



---

# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./6T_CheckPoint.md)


---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)