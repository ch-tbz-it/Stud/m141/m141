# phpMyAdmin Ratgeber:

## Mögliche Performance-Probleme

> Der Ratgeber kann Empfehlungen zu Server-Variablen durch Analyse der Server-Status-Variablen geben.
	
> Bitte beachten Sie jedoch, dass diese Empfehlungen auf einfachen Berechnungen und Faustregeln basieren und nicht umbedingt auf Ihrem System funktionieren müssen.
	
> Bevor Sie Änderungen an der Konfiguration vornehmen, seien Sie sich sicher, was Sie ändern und wie Sie die Änderungen rückgängig machen können. Falsche Optimierungen können sehr negative Effekte hervorrufen.
	
> Der beste Weg das System zu optimieren ist es genau eine Einstellung auf einmal zu ändern, die Leistungsfähigkeit der Datenbank zu testen und die Änderungen zurückzunehmen, falls es keine klar messbaren Verbesserungen gab.


1)
Die Betriebszeit beträgt weniger als ein Tag, Leistung-Optimierungen könnten ungenau sein.

Um genauere Durchschnittswerte zu haben, wird empfohlen, den Server mehr als einen Tag laufen zu lassen, bevor dieser Analysierer ausgeführt wird

2) {long\_query\_time} ist auf 10 Sekunden oder mehr gesetzt, somit werden nur langsame Abfragen geloggt, die länger als 10 Sekunden laufen.

Es wird empfohlen, long\_query\_time auf einen niedrigeren Wert zu setzen, abhängig von Ihrer Umgebung. Gewöhnlich wird ein Wert von 1 bis 5 Sekunden vorgeschlagen.

3) Die Überwachung langsamer Anfragen ist deaktiviert.

Loggen von langsamen Abfragen einschalten, indem slow\_query\_log auf 'ON' gesetzt wird. Das hilft beim Erkennen von grauenhaft langsamen Abfragen.

4) Weniger als 80% des Abfragen-Zwischenspeichers werden benutzt.

Das könnte daher kommen, dass query\_cache\_limit zu niedrig ist. Auch das Leeren des Abfragecaches könnte helfen.

5) Die maximale Größe des Ergebnissets im Abfragecache ist 1 MiB, wie standardmäßig.

Verändern von query\_cache\_limit (in der Regel durch erhöhen) kann die Effizienz steigern. Diese Variable bestimmt die maximale Größe eines Abfrageergebnis das in den Abfrage-Cache eingefügt werden soll. Wenn es viele Abfrageergebnisse über 1 MiB gibt die sich auch zwischenspeichern lassen (viele Lesevorgänge, wenig schreibt) dann kann ein erhöhen von query\_cache\_limit die Effizienz erhöhen. Während bei viele Abfrageergebnisse über 1 MiB die nicht sehr gut zwischenspeicherbar sind (oft aufgrund von Aktualisierungen der Tabelle ungültig) ein Erhöhen von query\_cache\_limit die Effizienz reduzieren kann.

6) Es werden zu viele Joins ohne die Benutzung von Indizes durchgeführt.

Dies bedeutet, dass Joins vollständige Tabellenscans durchführen. Indizes für die Spalten zu verwenden, die in den Join-Bedingungen eingesetzt werden, wird die Joins erheblich beschleunigen.

7) Die Lese-Rate des ersten Indexeintrag ist hoch.

Dies bedeutet in der Regel häufig vollständige Indexscans. Vollständige Indexscans sind schneller als Tablenscans aber sie kosten viele CPU-Zyklen in großen Tabellen, wenn diese Tabellen ein große Mengen von Aktualisierungen und Löschungen haben oder hatten. Ein Ausführen von "OPTIMIZE TABLE" auf diese Tabllen kann die Menge verringern und/oder die Geschwindigkeit der vollständige Indexscans beschleunigen. Abgesehen davon können vollständige Indexscans nur durch umschreiben der Abfragen reduziert werden.

8) Die Lese-Rate fester Positionen ist hoch.

Dies deutet darauf hin, dass viele Abfragen Sortieren und/oder vollständige Tabellen-Scan benötigen, einschließlich Join-Abfragen die keine Indizes verwenden. Fügen Sie Indizes hinzu wo zutreffend.

9) Lese-Rate nächste Tabellenzeile ist hoch.

Dies deutet darauf hin, dass viele Abfragen Full Table Scans durchführen. Fügen Sie Indizes hinzu wo zutreffend.

10) Viele temporäre Tabellen werden auf die Festplatte geschrieben, anstelle im Speicher gehalten zu werden.

Erhöhen von max\_heap\_table\_size und tmp\_table\_size können helfen. Jedoch werden einige temporären Tabellen immer unabhängig vom Wert dieser Variablen auf den Datenträger geschrieben. Um diese zu verhindern müssen Sie Ihre Abfragen so umschreiben das es nicht zu diesen Bedingungen kommt (Innerhalb einer temporären Tabelle: Vorhandensein eines BLOB- oder TEXT-Spalte oder eine Spalte größer als 512 Bytes) wie in der MySQL-Dokumentation erwähnt wird

11) MyISAM Schlüssel-Cache (Indize Cache) % benutz ist niedrig.

Sie müssen möglicherweise Ihren key\_buffer\_size verkleinern, überprüfen Sie Ihre Tabellen um zu sehen ob Indizes entfernt wurden oder Überprüfen Sie Abfragen und Erwartungen um zu sehen welche Indizes verwendet werden.
