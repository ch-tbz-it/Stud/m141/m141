![](../x_res/tbz_logo.png)√

# M141 - DB-Systeme in Betrieb nehmen  (7.Tag)

*Autoren: Gerd Gesell, Kellenberger Michael 2024*

**Inhalt**

[TOC]

> ([Lösung 6.Tag](../Loesungen/6T_CP_Loes.md))


# Datenbank mit Testdaten testen

![](../x_res/Learn.png)

 Testvorgehen mit Testdaten

## Vorgehen

**Ausgangslage**

- Lokal DBMS und DB installiert
- Admin user *root* existiert in der DB
- Zwei Test User mit Passwort bekannt
  - Test-Reader
  - Test-Contributor
- Testdaten als CSV Files vorhanden

**Ziel**

- **User** erstellen und testen
  - Test-Reader (nur Leserecht) 
  - Test-Contributor (Lese und Schreibrecht)
- **Schema** und **Tabellen** installieren und Daten laden
- **Test-Reader** hat Zugriff und kann entsprechende Daten lesen
- **Test-Contributor** hat Zugriff und kann entsprechende Daten lesen, schreiben und löschen. Alle anderen Berechtigungen sind nicht freigegeben (Tabellen löschen, Views erstellen, Berechtigungen erteilen usw.)
- **Datenkonsistenz** testen (= logisch korrekter Zustand der Daten)
- **Performance Test** wurden durchgeführt und ein Vergleich ist vorhanden
  - Ohne Index -> Executionplan: Tablescan ersichtlich
  - Mit Index -> Executionplan: Key lookup

**Schrittweise vorgehen**

1. Login mit Test User
   - Nicht möglich, da Datenbank keine User hat
2. Login mit Admin
   - User erstellen
   - Login testen
3. Mit Admin Schema und Tabellen erstellen
4. Mit Admin "Bulk-Import" Tabellen mit Daten befüllen (je 400'000 Datensätze)
5. Mit Admin Berechtigungen für die User konfigurieren -> DB Rollen erstellen
6. Mit Test User einloggen und die Berechtigungen testen
   - Test-Reader darf nur lesen und keine Insert durchführen können
   - Test-Contributor darf lesen, schreiben und löschen. Alles andere nicht erlaubt
7. Datenintegrität sicherstellen: (Siehe [M164 5.Tag](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/5.Tag?ref_type=heads#datenintegrit%C3%A4t))
   - Eindeutigkeit der Datensätze
   - Referenzielle Integrität der Beziehungen
   - Datentypen der Spalten/Attribute
   - Datenbeschränkungen der Spalten/Attribute
   - Validierung der Daten
7. Performance Test:
   - Mit Test-Reader einen JOIN schreiben und irgendeine Zieladresse per ID suchen
     - WICHTIG: Messen
8. Index erstellen nur auf eine Tabelle
9. Test gem. Nr 7 wiederholen
   - Messung vergleichen zu vorher
10. Index erstellen auf die andere Tabelle
11. Test gem. Nr 7 wiederholen
   - Messung vergleichen zu vorher
12. Datenkonsistenz testen
13. Schlussbilanz

---

![](../x_res/Train_R1.png)


> ![](../x_res/note.png) Erstellen Sie ein Protokoll zu den folgenden Punkten. Kann auch direkt in einem SQL-Script erfolgen ( -- Z.B. mit Kommentarzeilen).

## 1 Login mit Test User

Nicht möglich. Warum?

## 2 User erstellen und Login testen

```sql
CREATE USER 'Reader' IDENTIFIED BY '123!';
CREATE USER 'Contributor' IDENTIFIED BY '123!';
```
> MariaDB erstellt damit 4 User, wobei die 2 Localhost-User keine PW haben!  <br>
> Überprüfen Sie das mit phpMyAdmin! <br>
> Vorteil: Auf dem Localhost können Sie sich so ohne PW einloggen... <br>
> Im produktiven Betrieb die 2 Localhost-User wieder löschen!!! <br>

## 3 Schema und Tabellen erstellen

Die beiden Tabellen haben keine PK und Indizes definiert.

Erwartetes Resultat: Schlechte Performance.

```sql
DROP SCHEMA IF EXISTS `myTestDb` ;
CREATE SCHEMA IF NOT EXISTS `myTestDb` DEFAULT CHARACTER SET utf8 ;
USE `myTestDb` ;

DROP TABLE IF EXISTS `Person` ;
DROP TABLE IF EXISTS `Adresse` ;

CREATE TABLE Person (
    Id INT,
    Vorname VARCHAR(255),
    Nachname VARCHAR(255),
    Email VARCHAR(255),
    AdresseId INT
);

CREATE TABLE Adresse (
    Id INT,
    Strasse VARCHAR(255),
    Hausnummer VARCHAR(10),
    PLZ VARCHAR(10),
    Stadt VARCHAR(255),
    Bundesstaat VARCHAR(10)
);
```

## 4 Mit Admin-User "Bulk-Import" Tabellen mit Daten befüllen (je 400'000 Datensätze)

[person.csv](./person.csv) 

[adresse.csv](./adresse.csv)


```sql
LOAD DATA INFILE './person.csv' 
INTO TABLE Person
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;

LOAD DATA INFILE './adresse.csv' 
...
```
## 5 Mit Admin-User Berechtigungen für die User konfigurieren

| Zugriffsmatrix  *DB myTestDb* |           |       |       |    |   |               |       |       |       |
|-----------------------------|-----------|-------|-------|----|---|---------------|-------|-------|-------|
| *Role =\>*        | *Reader* |       |       |    | |  *Contributor* |       |       |       | 
| Tabellen - Attribute        | S         | I     | U     | D  | | S             | I     | U     | D     |   
| person                    | **x**        |       |       |    | | **x**         | **x** | **x** | **x** |   
| adresse                      | **x**     |       |       |    | | **x**         | **x** | **x** | **x** |   

*S = Select, I = Insert, U = Update, D = Delete*

```sql
-- Rollen erstellen und berechtigen
CREATE ROLE 'RoleReader', 'RoleContributor';
GRANT SELECT ON myTestDb.* TO 'RoleReader';
GRANT SELECT, INSERT, UPDATE, DELETE ON myTestDb.* TO 'RoleContributor';

-- Rollen den Benutzern zuweisen
GRANT 'RoleReader' TO 'Reader'@'localhost';
GRANT 'RoleContributor' TO 'Contributor'@'localhost';

-- Berechtigungen neu laden, damit GRANTS wirksam wird
FLUSH PRIVILEGES;
```

Grants bei Test User überprüfen: 

```sql
SHOW GRANTS FOR 'Reader';
SHOW GRANTS FOR 'Contributor';
```

## 6 Mit Test-Usern einloggen und die Berechtigungen testen

Ein paar Test mit entsprechenden Usern gemäss Zugriffsmatrix machen: `SELECT`, `INSERT`, `UPDATE`und `DELETE`.

--> Alle Befehle und Ausgaben ins Protokoll aufnehmen


## 7 Datenintegrität sicherstellen

   - **Datentypen und Validierung**: Die Daten sollten in der Datenbank in den korrekten Datentypen gespeichert werden, um sicherzustellen, dass sie korrekt behandelt werden können. Beispielsweise sollte eine Telefonnummer in einem Feld vom Typ "string" (Zeichenfolge) und nicht vom Typ "integer" (Ganzzahl) gespeichert werden. Vor dem Import alle Attribute (*Stichproben*) und Daten überprüfen und Datentyp allenfalls anpassen!

   - **Eindeutigkeit und Datenkonsistenz**: Jeder Datensatz in der Datenbank sollte eindeutig identifizierbar und dauerhaft sein, um zu verhindern, dass Daten doppelt eingegeben werden (Redundanzen).
   
```sql
SELECT Id FROM table
GROUP BY Id
HAVING COUNT(Id) > 1
```

   - **Referenzielle Integrität**: Wenn eine Tabelle Beziehungen zu anderen Tabellen hat, sollten die Beziehungen immer konsistent bleiben. Das bedeutet, dass z.B. eine Verknüpfung zwischen einem Wohnort und einem Kunden nur dann bestehen kann, wenn der Kunde auch tatsächlich einen Wohnort hat. 

     - Jede ID (bzw. Tabelle) sollte einen **PRIMARY KEY-Constraint** haben
     - Jeder Fremdschlüssel sollte einen **FOREIGN KEY-Constraint** und die korrekten [Beziehungseinstellungen](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/4.Tag?ref_type=heads#beziehung-mit-einschr%C3%A4nkung-constraint-erstellen) haben (**NN**, **UNIQUE INDEX**)
     - Jeder Fremdschlüssel sollte einen **FK-Constraint** haben, um beim Import die *ref. Integrität* sicherzustellen:
   
```SQL
-- Erstellen der Tabelle tbl_Person
CREATE TABLE tbl_Person (
    Person_ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,  -- ID set as PK und NN (UQ via PK-INDEX)
    Name varchar(255),
    Vorname varchar(255),
    Alter INT,
    Ort_FK int NOT NULL,  --c:mc
    CONSTRAINT 'Rel_wohnt' FOREIGN KEY (Ort_FK) REFERENCES tbl_Ort(Ort_ID) -- checks ref. integrity.
) ENGINE=... ;

-- CREATE UNIQUE INDEX `Ort_FK_UNIQUE` ON...; -- not necessary, because of '1:mc'!
```

   Es kann auch sein, dass die eine Beziehung auf redundante Datensätze zeigt, z.B: durch Import von OpenData-Daten. Diese redundanten Beziehungen müssen bereinigt werden. 
   
   **Beispiellösung Bereinigung redundanter Beziehungen**: `tbl_Person -mc----1- tbl_Orte`: Um redundante Ortsbezeichnungen zu bereinigen, könnten Sie zuerst eine *eindeutige* ID für jede Ortsbezeichnung erstellen und dann diese ID in der `tbl_Person` Tabelle *aktualisieren*:
   
   ![FK Bereinigung Ausgangslage](./media/FK_Bereinigung_1.png)
  

```SQL
-- Erstelle eine temporäre Tabelle 'temp_Orte' mit eindeutigen IDs für jede Ortsbezeichnung
CREATE TEMPORARY TABLE temp_Orte AS
  SELECT MIN(Ort_ID) as ID, Ortsbezeichnung
  FROM tbl_Ort
  GROUP BY Ortsbezeichnung;
```
   ![FK Bereinigung Ausgangslage](./media/FK_Bereinigung_2.png)

```SQL
-- Aktualisiere die Tabelle 'tbl_Person', um die neuen IDs zu verwenden
UPDATE tbl_Person SET Ort_FK = (
    SELECT ID FROM temp_Orte WHERE temp_Orte.Ortsbezeichnung = tbl_Ort.Ortsbezeichnung
  ) FROM tbl_Ort
  WHERE tbl_Person.Ort_FK = tbl_Ort.ID;
```

```SQL
-- Entferne redundante Einträge aus der Tabelle 'tbl_Ort'
DELETE FROM tbl_Ort
  WHERE Ort_ID NOT IN (SELECT ID FROM temp_Orte);
```

   ![FK Bereinigung Ausgangslage](./media/FK_Bereinigung_3.png)


   - **Datenbeschränkungen**: Datenbeschränkungen stellen sicher, dass die Daten in der Datenbank gültig sind. Zum Beispiel kann eine Datenbank so eingerichtet werden, dass nur positive Zahlen in einem bestimmten Feld eingegeben werden dürfen oder dass eine E-Mail-Adresse nur in einem bestimmten Format eingegeben werden kann.

```SQL
ALTER TABLE Users
ADD CONSTRAINT CHK_Age CHECK (Age >= 0);
```

```SQL
CREATE FUNCTION dbo.fnValidateEmail(@Email varchar(256))
RETURNS bit
AS
BEGIN
   RETURN CASE 
      WHEN @Email LIKE '%_@__%.__%' THEN 1
      ELSE 0
   END
END;

ALTER TABLE Users
ADD CONSTRAINT CHK_Email CHECK (dbo.fnValidateEmail(Email) = 1);
```
   
**Hinweis**

- Daten**in**konsistenz simulieren: In der Tabelle `Adresse` kommmen mehrere Datensätze doppelt vor (insgesamt 3 Datensätze)


## 8 Performance Test ohne Index

Test-Query schreiben, z.B.

```sql
SELECT * FROM Person person
INNER JOIN Adresse adresse ON adresse.Id = person.AdresseId
WHERE person.id = 2569
```

**Ergebnis** 

Schlechte Performance. bis zu 500ms!

![](../x_res/duration.png)

Executionplan Tablescan ersichtlich!

![](../x_res/executionplan.png)


## 9 Index erstellen nur auf eine Tabelle

```sql
DROP INDEX idx_AddresseId ON Person ;
CREATE INDEX idx_AddresseId ON Person (AdresseId);
```

## 10 Test gem. Nr 8 wiederholen

Executionplan analysieren. Etwas hat sich verändert!

Duration hat sich auch verändert!

## 11 Index erstellen auf die andere Tabelle

```sql
DROP INDEX idx_Id ON Adresse;
CREATE INDEX idx_Id ON Adresse (Id);
```

## 12 Test gem. Nr 8 wiederholen

Executionplan analysieren. Etwas hat sich verändert!

Duration hat sich auch verändert!



## 13 Schlussbilanz

*Schlussfolgerung erstellen.* 

![Hinweis](../x_res/Hinweis.png) Nachfolgende Punkte sind wichtig bei einer Migration:

- Checkliste bzw. Drehbuch von Vorteil bei Migration
- Testprotokolle führen
- Performance überprüfen


<br><br><br>

---

# From Business: Flatfile mit AI-Assistent
Es gibt einige Tools, die den ganzen Import- und Testlauf automatisieren. Hier ein 
Beispiel [Flatfile](https://flatfile.com/)

Schauen Sie die [Demo](https://flatfile.com/demo/) an. (Video 7min)

---

[Lösung](../Loesungen/7T_Protokoll.md)



