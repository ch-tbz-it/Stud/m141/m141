-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 09. Jun 2020 um 10:11
-- Server-Version: 10.1.44-MariaDB-0+deb9u1
-- PHP-Version: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `Blogeinträge`
--

DROP SCHEMA IF EXISTS `Blogeinträge` ;
CREATE SCHEMA  `Blogeinträge` DEFAULT CHARACTER SET utf8 ;
USE `Blogeinträge` ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `BLOGEINTRAG`
--

CREATE TABLE `BLOGEINTRAG` (
  `BlogeintragID` int(11) NOT NULL,
  `Erstellungsdatum` date NOT NULL,
  `Änderungsdatum` date DEFAULT NULL,
  `Titel` varchar(250) NOT NULL,
  `Text` text,
  `Benutzername` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `BLOGEINTRAG`
--

INSERT INTO `BLOGEINTRAG` (`BlogeintragID`, `Erstellungsdatum`, `Änderungsdatum`, `Titel`, `Text`, `Benutzername`) VALUES
(2001, '2018-02-20', NULL, 'Test-Headline', 'In diesem Blogeintrag möchte ich Euch zeigen ...', 'katrin'),
(2002, '2018-02-20', '2018-02-24', 'Test-Headline', 'Heute werde ich über ...', 'jens');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `CHEFREDAKTEUR`
--

CREATE TABLE `CHEFREDAKTEUR` (
  `Benutzername` varchar(50) NOT NULL,
  `Telefonnummer` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `CHEFREDAKTEUR`
--

INSERT INTO `CHEFREDAKTEUR` (`Benutzername`, `Telefonnummer`) VALUES
('jens', '+49 178 3339990');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FAVORITENLISTE`
--

CREATE TABLE `FAVORITENLISTE` (
  `FavoritenlisteID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `FAVORITENLISTE`
--

INSERT INTO `FAVORITENLISTE` (`FavoritenlisteID`) VALUES
(1001),
(1002),
(1003),
(1004);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FAVORITENLISTEenthaltenBLOGEINTRAG`
--

CREATE TABLE `FAVORITENLISTEenthaltenBLOGEINTRAG` (
  `BlogeintragID` int(11) NOT NULL,
  `FavoritenlisteID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `FAVORITENLISTEenthaltenBLOGEINTRAG`
--

INSERT INTO `FAVORITENLISTEenthaltenBLOGEINTRAG` (`BlogeintragID`, `FavoritenlisteID`) VALUES
(2001, 1003),
(2001, 1004),
(2002, 1002);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `NUTZER`
--

CREATE TABLE `NUTZER` (
  `Benutzername` varchar(50) NOT NULL,
  `EMailAdresse` varchar(50) NOT NULL,
  `Geschlecht` char(1) NOT NULL,
  `Geburtsdatum` date NOT NULL,
  `Passwort` varchar(10) NOT NULL,
  `FavoritenlisteID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `NUTZER`
--

INSERT INTO `NUTZER` (`Benutzername`, `EMailAdresse`, `Geschlecht`, `Geburtsdatum`, `Passwort`, `FavoritenlisteID`) VALUES
('jens', 'jensmueller@googlemail.com', 'm', '1984-05-30', 'ferrari', 1001),
('katrin', 'katrin-1990@googlemail.com', 'w', '1990-01-16', 'darkeyes', 1002),
('michael', 'michael_steffens@googlemail.com', 'm', '1989-03-14', 'infotech', 1004),
('natascha', 'natascha_w@gmx.de', 'w', '1987-08-22', 'starwish', 1003);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `REDAKTEUR`
--

CREATE TABLE `REDAKTEUR` (
  `Benutzername` varchar(50) NOT NULL,
  `Vorname` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Vorstellungstext` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `REDAKTEUR`
--

INSERT INTO `REDAKTEUR` (`Benutzername`, `Vorname`, `Name`, `Vorstellungstext`) VALUES
('jens', 'Jens', 'Müller', 'Hey, ich freue mich auf dieser Plattform als ...'),
('katrin', 'Katrin', 'Loos', 'Hi, mein Name ist Katrin und ich bin seit ...');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `BLOGEINTRAG`
--
ALTER TABLE `BLOGEINTRAG`
  ADD PRIMARY KEY (`BlogeintragID`),
  ADD KEY `Benutzername` (`Benutzername`);

--
-- Indizes für die Tabelle `CHEFREDAKTEUR`
--
ALTER TABLE `CHEFREDAKTEUR`
  ADD PRIMARY KEY (`Benutzername`);

--
-- Indizes für die Tabelle `FAVORITENLISTE`
--
ALTER TABLE `FAVORITENLISTE`
  ADD PRIMARY KEY (`FavoritenlisteID`);

--
-- Indizes für die Tabelle `FAVORITENLISTEenthaltenBLOGEINTRAG`
--
ALTER TABLE `FAVORITENLISTEenthaltenBLOGEINTRAG`
  ADD PRIMARY KEY (`BlogeintragID`,`FavoritenlisteID`),
  ADD KEY `FavoritenlisteID` (`FavoritenlisteID`);

--
-- Indizes für die Tabelle `NUTZER`
--
ALTER TABLE `NUTZER`
  ADD PRIMARY KEY (`Benutzername`),
  ADD KEY `FavoritenlisteID` (`FavoritenlisteID`);

--
-- Indizes für die Tabelle `REDAKTEUR`
--
ALTER TABLE `REDAKTEUR`
  ADD PRIMARY KEY (`Benutzername`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `BLOGEINTRAG`
--
ALTER TABLE `BLOGEINTRAG`
  ADD CONSTRAINT `BLOGEINTRAG_ibfk_1` FOREIGN KEY (`Benutzername`) REFERENCES `REDAKTEUR` (`Benutzername`);

--
-- Constraints der Tabelle `CHEFREDAKTEUR`
--
ALTER TABLE `CHEFREDAKTEUR`
  ADD CONSTRAINT `CHEFREDAKTEUR_ibfk_1` FOREIGN KEY (`Benutzername`) REFERENCES `REDAKTEUR` (`Benutzername`);

--
-- Constraints der Tabelle `FAVORITENLISTEenthaltenBLOGEINTRAG`
--
ALTER TABLE `FAVORITENLISTEenthaltenBLOGEINTRAG`
  ADD CONSTRAINT `FAVORITENLISTEenthaltenBLOGEINTRAG_ibfk_1` FOREIGN KEY (`BlogeintragID`) REFERENCES `BLOGEINTRAG` (`BlogeintragID`),
  ADD CONSTRAINT `FAVORITENLISTEenthaltenBLOGEINTRAG_ibfk_2` FOREIGN KEY (`FavoritenlisteID`) REFERENCES `FAVORITENLISTE` (`FavoritenlisteID`);

--
-- Constraints der Tabelle `NUTZER`
--
ALTER TABLE `NUTZER`
  ADD CONSTRAINT `NUTZER_ibfk_1` FOREIGN KEY (`FavoritenlisteID`) REFERENCES `FAVORITENLISTE` (`FavoritenlisteID`);

--
-- Constraints der Tabelle `REDAKTEUR`
--
ALTER TABLE `REDAKTEUR`
  ADD CONSTRAINT `REDAKTEUR_ibfk_1` FOREIGN KEY (`Benutzername`) REFERENCES `NUTZER` (`Benutzername`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
