-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';

-- -----------------------------------------------------
-- Schema buch
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `buch` ;

-- -----------------------------------------------------
-- Schema buch
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `buch` DEFAULT CHARACTER SET utf8 ;
USE `buch` ;

-- -----------------------------------------------------
-- Table `buch`.`Autor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `buch`.`Autor` ;

CREATE TABLE IF NOT EXISTS `buch`.`Autor` (
  `ID_Autor` INT NOT NULL,
  `Vorname` VARCHAR(30) NULL,
  `Nachname` VARCHAR(30) NULL,
  `Alter` VARCHAR(30) NULL,
  `Herkunft` VARCHAR(30) NULL,
  PRIMARY KEY (`ID_Autor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `buch`.`Verlag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `buch`.`Verlag` ;

CREATE TABLE IF NOT EXISTS `buch`.`Verlag` (
  `ID_Verlag` INT NOT NULL,
  `Name` VARCHAR(30) NULL,
  PRIMARY KEY (`ID_Verlag`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `buch`.`Buch`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `buch`.`Buch` ;

CREATE TABLE IF NOT EXISTS `buch`.`Buch` (
  `ID_Buch` INT NOT NULL,
  `Titel` VARCHAR(30) NULL,
  `FS_Autor` INT NOT NULL,
  `FS_Verlag` INT NOT NULL,
  PRIMARY KEY (`ID_Buch`),
  INDEX `REL_Autor_Buch_idx` (`FS_Autor` ASC) VISIBLE,
  INDEX `REL_Verlag_Buch1_idx` (`FS_Verlag` ASC) VISIBLE,
  CONSTRAINT `REL_Autor_Buch`
    FOREIGN KEY (`FS_Autor`)
    REFERENCES `buch`.`Autor` (`ID_Autor`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `REL_Verlag_Buch1`
    FOREIGN KEY (`FS_Verlag`)
    REFERENCES `buch`.`Verlag` (`ID_Verlag`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `buch`.`Laden`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `buch`.`Laden` ;

CREATE TABLE IF NOT EXISTS `buch`.`Laden` (
  `ID_Laden` INT NOT NULL,
  `Adresse` VARCHAR(30) NULL,
  `FS_Buch` INT NOT NULL,
  PRIMARY KEY (`ID_Laden`),
  INDEX `REL_Buch_Laden1_idx` (`FS_Buch` ASC) VISIBLE,
  CONSTRAINT `REL_Buch_Laden1`
    FOREIGN KEY (`FS_Buch`)
    REFERENCES `buch`.`Buch` (`ID_Buch`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
