-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 10. April 2012 um 21:59
-- Server Version: 5.5.8
-- PHP-Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `nahrung`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lmgruppen`
--

CREATE TABLE `lmgruppen` (
  `GruppenNr` int(11) NOT NULL,
  `GruppenName` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`GruppenNr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `lmgruppen`
--

INSERT INTO `lmgruppen` VALUES(1, 'Getreide');
INSERT INTO `lmgruppen` VALUES(2, 'Kartoffeln und Stärke');
INSERT INTO `lmgruppen` VALUES(3, 'Zucker, Sirup, Honig');
INSERT INTO `lmgruppen` VALUES(4, 'Hülsenfrüchte, Nüsse, Kastanien, Kakao');
INSERT INTO `lmgruppen` VALUES(5, 'Gemüse');
INSERT INTO `lmgruppen` VALUES(6, 'Obst');
INSERT INTO `lmgruppen` VALUES(7, 'Fleisch');
INSERT INTO `lmgruppen` VALUES(8, 'Eier und Eikonserven');
INSERT INTO `lmgruppen` VALUES(9, 'Fische und Schaltiere');
INSERT INTO `lmgruppen` VALUES(10, 'Milch und -erzeugnisse  (ohne Butter)');
INSERT INTO `lmgruppen` VALUES(11, 'Öle und Fette');
INSERT INTO `lmgruppen` VALUES(12, 'alkoholische Getränke');
INSERT INTO `lmgruppen` VALUES(13, 'alkoholfreie Getränke');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nwt`
--

CREATE TABLE `nwt` (
  `NWTNR` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `LMName` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `Prot` float NOT NULL,
  `Fett` float NOT NULL,
  `KH` float NOT NULL,
  `Alk` float NOT NULL,
  PRIMARY KEY (`NWTNR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `nwt`
--

INSERT INTO `nwt` VALUES('C116001', 'Weizen Grieß', 9.56, 0.79, 68.91, 0);
INSERT INTO `nwt` VALUES('C213011', 'Weizen Mehl Type 550', 9.84, 1.13, 70.76, 0);
INSERT INTO `nwt` VALUES('C221011', 'Roggen Vollkornmehl', 9, 1.7, 59.7, 0);
INSERT INTO `nwt` VALUES('C141001', 'Gerste Vollkorn', 9.84, 2.1, 64.31, 0);
INSERT INTO `nwt` VALUES('C131001', 'Hafer ganzes Korn', 11.69, 7.09, 59.8, 0);
INSERT INTO `nwt` VALUES('C341001', 'Mais Vollkorn', 8.54, 3.8, 64.66, 0);
INSERT INTO `nwt` VALUES('C352001', 'Reis geschält', 6.83, 0.62, 77.73, 0);
INSERT INTO `nwt` VALUES('K110111', 'Kartoffeln geschält frisch', 2.04, 0.11, 14.81, 0);
INSERT INTO `nwt` VALUES('C446001', 'Mais Stärke', 0.4, 0.08, 85.82, 0);
INSERT INTO `nwt` VALUES('S111000', 'Zucker weiß', 0, 0, 99.8, 0);
INSERT INTO `nwt` VALUES('S150000', 'Sirup', 0.3, 0, 79, 0);
INSERT INTO `nwt` VALUES('S121000', 'Blütenhonig-Mischungen', 0.38, 0, 75.07, 0);
INSERT INTO `nwt` VALUES('H700111', 'Hülsenfrüchte reif frisch', 22.9, 1.44, 41.85, 0);
INSERT INTO `nwt` VALUES('H110601', 'Erdnuß geröstet', 25.63, 49.4, 9.45, 0);
INSERT INTO `nwt` VALUES('H210101', 'Mandel süß frisch', 18.72, 54.1, 3.7, 0);
INSERT INTO `nwt` VALUES('H230121', 'Edelkastanien (Marone) frisch gega', 2.48, 1.9, 35.97, 0);
INSERT INTO `nwt` VALUES('H120000', 'Walnüsse', 14.4, 62.5, 10.6, 0);
INSERT INTO `nwt` VALUES('H130101', 'Haselnuß frisch', 11.96, 61.6, 10.54, 0);
INSERT INTO `nwt` VALUES('S713000', 'Kakaopulver stark entölt', 23.07, 12, 12.63, 0);
INSERT INTO `nwt` VALUES('G341111', 'Rotkohl frisch', 1.5, 0.18, 3.54, 0);
INSERT INTO `nwt` VALUES('G311111', 'Blumenkohl frisch', 2.46, 0.28, 2.34, 0);
INSERT INTO `nwt` VALUES('G332111', 'Rosenkohl frisch', 4.45, 0.34, 3.29, 0);
INSERT INTO `nwt` VALUES('G343111', 'Wirsingkohl frisch', 3, 0.4, 2.41, 0);
INSERT INTO `nwt` VALUES('G331111', 'Kohlrabi frisch', 2, 0.1, 3.7, 0);
INSERT INTO `nwt` VALUES('G312111', 'Broccoli frisch', 3.3, 0.2, 2.51, 0);
INSERT INTO `nwt` VALUES('G342111', 'Weißkohl frisch', 1.37, 0.2, 4.16, 0);
INSERT INTO `nwt` VALUES('G613111', 'Rote Rübe frisch', 1.53, 0.1, 8.38, 0);
INSERT INTO `nwt` VALUES('G620111', 'Mohrrübe frisch', 0.98, 0.2, 4.8, 0);
INSERT INTO `nwt` VALUES('G660111', 'Knollensellerie frisch', 1.7, 0.3, 2.25, 0);
INSERT INTO `nwt` VALUES('G430111', 'Fenchel frisch', 2.43, 0.3, 2.84, 0);
INSERT INTO `nwt` VALUES('G650111', 'Schwarzwurzel frisch', 1.4, 0.4, 1.63, 0);
INSERT INTO `nwt` VALUES('G691111', 'Radieschen frisch', 1.05, 0.14, 2.13, 0);
INSERT INTO `nwt` VALUES('G680111', 'Rettich frisch', 1.05, 0.15, 1.89, 0);
INSERT INTO `nwt` VALUES('G321111', 'Chinakohl frisch', 1.19, 0.3, 1.19, 0);
INSERT INTO `nwt` VALUES('G410111', 'Artischocken frisch', 2.4, 0.12, 2.63, 0);
INSERT INTO `nwt` VALUES('F081111', 'Rhabarber frisch', 0.6, 0.1, 1.36, 0);
INSERT INTO `nwt` VALUES('G230111', 'Mangold frisch', 2.13, 0.3, 2.9, 0);
INSERT INTO `nwt` VALUES('G450111', 'Spargel frisch', 1.9, 0.14, 2.04, 0);
INSERT INTO `nwt` VALUES('G101111', 'Chicoree frisch', 1.3, 0.18, 2.34, 0);
INSERT INTO `nwt` VALUES('G105111', 'Kopfsalat frisch', 1.25, 0.22, 1.06, 0);
INSERT INTO `nwt` VALUES('G102111', 'Endivien frisch', 1.75, 0.2, 0.3, 0);
INSERT INTO `nwt` VALUES('G104111', 'Feldsalat frisch', 1.84, 0.36, 0.7, 0);
INSERT INTO `nwt` VALUES('G109111', 'Zuckerhutsalat frisch', 1.2, 0.22, 1.5, 0);
INSERT INTO `nwt` VALUES('G210111', 'Spinat frisch', 2.52, 0.3, 0.55, 0);
INSERT INTO `nwt` VALUES('G103111', 'Eisbergsalat frisch', 1, 0.2, 1.57, 0);
INSERT INTO `nwt` VALUES('G108111', 'Schnittsalat frisch', 1.3, 0.3, 2.8, 0);
INSERT INTO `nwt` VALUES('G710111', 'Bohnen grün frisch', 2.39, 0.24, 3.2, 0);
INSERT INTO `nwt` VALUES('G760111', 'Erbsen grün frisch', 6.55, 0.48, 12.3, 0);
INSERT INTO `nwt` VALUES('G560111', 'Tomaten frisch', 0.95, 0.21, 2.6, 0);
INSERT INTO `nwt` VALUES('G510111', 'Aubergine frisch', 1.24, 0.18, 2.49, 0);
INSERT INTO `nwt` VALUES('G480111', 'Zwiebeln frisch', 1.25, 0.25, 4.91, 0);
INSERT INTO `nwt` VALUES('G490111', 'Knoblauch frisch', 6.05, 0.12, 28.41, 0);
INSERT INTO `nwt` VALUES('G520111', 'Gurke frisch', 0.6, 0.2, 1.81, 0);
INSERT INTO `nwt` VALUES('G541111', 'Gemüsepaprika grün frisch', 1.17, 0.3, 2.91, 0);
INSERT INTO `nwt` VALUES('G543111', 'Gemüsepaprika rot frisch', 1.3, 0.5, 6.4, 0);
INSERT INTO `nwt` VALUES('G582111', 'Zucchini frisch', 1.6, 0.4, 2.05, 0);
INSERT INTO `nwt` VALUES('G081111', 'Schnittlauch frisch', 3.58, 0.6, 1.6, 0);
INSERT INTO `nwt` VALUES('G440111', 'Palmenherz frisch', 2.5, 0.09, 6, 0);
INSERT INTO `nwt` VALUES('G570111', 'Zuckermais frisch', 3.28, 1.23, 15.72, 0);
INSERT INTO `nwt` VALUES('K701111', 'Champignon frisch', 2.74, 0.24, 0.56, 0);
INSERT INTO `nwt` VALUES('G413902', 'Artischockenboden Konserve, abgetr', 1.84, 0.09, 1.71, 0);
INSERT INTO `nwt` VALUES('G570902', 'Zuckermais Konserve, abgetropft', 3.12, 1.17, 12.63, 0);
INSERT INTO `nwt` VALUES('G760902', 'Erbsen grün Konserve, abgetropft', 6.21, 0.45, 9.84, 0);
INSERT INTO `nwt` VALUES('G450902', 'Spargel Konserve, abgetropft', 1.74, 0.12, 1.58, 0);
INSERT INTO `nwt` VALUES('G345902', 'Sauerkraut Konserve abgetropft', 1.37, 0.27, 0.59, 0);
INSERT INTO `nwt` VALUES('F110111', 'Apfel frisch', 0.34, 0.4, 11.43, 0);
INSERT INTO `nwt` VALUES('F130111', 'Birne frisch', 0.5, 0.3, 12.4, 0);
INSERT INTO `nwt` VALUES('F210111', 'Kirschen frisch', 0.9, 0.3, 13.3, 0);
INSERT INTO `nwt` VALUES('F223111', 'Zwetschge frisch', 0.6, 0.1, 8.8, 0);
INSERT INTO `nwt` VALUES('F220111', 'Pflaumen frisch', 0.6, 0.2, 10.2, 0);
INSERT INTO `nwt` VALUES('F201111', 'Aprikose frisch', 0.9, 0.1, 8.54, 0);
INSERT INTO `nwt` VALUES('F203111', 'Pfirsich frisch', 0.8, 0.1, 8.9, 0);
INSERT INTO `nwt` VALUES('F301111', 'Erdbeere frisch', 0.8, 0.4, 5.5, 0);
INSERT INTO `nwt` VALUES('F300000', 'Beerenobst', 0.7, 0.3, 15.6, 0);
INSERT INTO `nwt` VALUES('F530111', 'Melone frisch', 0.6, 0.2, 8.28, 0);
INSERT INTO `nwt` VALUES('F310111', 'Weintrauben frisch', 0.7, 0.3, 15.6, 0);
INSERT INTO `nwt` VALUES('F503111', 'Banane frisch', 1.15, 0.18, 21.39, 0);
INSERT INTO `nwt` VALUES('F603111', 'Orange frisch', 1, 0.2, 9.19, 0);
INSERT INTO `nwt` VALUES('F606111', 'Mandarine frisch', 0.7, 0.3, 10.1, 0);
INSERT INTO `nwt` VALUES('F601111', 'Zitrone frisch', 0.7, 0.6, 8.08, 0);
INSERT INTO `nwt` VALUES('F604111', 'Grapefruit frisch', 0.6, 0.15, 8.95, 0);
INSERT INTO `nwt` VALUES('F202111', 'Nektarine frisch', 0.9, 0.1, 12.4, 0);
INSERT INTO `nwt` VALUES('F514111', 'Kiwi frisch', 1, 0.63, 10.77, 0);
INSERT INTO `nwt` VALUES('F225111', 'Reineclaude frisch', 0.8, 0.1, 13.5, 0);
INSERT INTO `nwt` VALUES('F501111', 'Ananas frisch', 0.46, 0.15, 13.12, 0);
INSERT INTO `nwt` VALUES('F201902', 'Aprikose Konserve abgetropft', 0.73, 0.08, 17.59, 0);
INSERT INTO `nwt` VALUES('F203902', 'Pfirsich Konserve abgetropft', 0.65, 0.08, 17.79, 0);
INSERT INTO `nwt` VALUES('F501902', 'Ananas Konserve abgetropft', 0.38, 0.12, 20.28, 0);
INSERT INTO `nwt` VALUES('U211011', 'Rind Filet (Lende) mager (ma) = En', 20.86, 6.37, 0, 0);
INSERT INTO `nwt` VALUES('U172111', 'Rind Bratenfleisch (mf) frisch= Br', 19.41, 3.45, 0, 0);
INSERT INTO `nwt` VALUES('U010111', 'Rind Hackfleisch frisch =Hackfleis', 17.01, 8.93, 0, 0);
INSERT INTO `nwt` VALUES('U182011', 'Rind Kochfleisch mittelfett (mf) (', 18.42, 14.38, 0, 0);
INSERT INTO `nwt` VALUES('U261111', 'Rind Schulter (Bug) (ma) frisch', 20.2, 5.3, 0, 0);
INSERT INTO `nwt` VALUES('U100111', 'Rind Fleisch frisch', 19.6, 8.58, 0, 0);
INSERT INTO `nwt` VALUES('U310111', 'Kalb Filet frisch = Plätzli', 21.35, 1.56, 0, 0);
INSERT INTO `nwt` VALUES('U371011', 'Kalb Bratenfleisch mager (ma) = Br', 18.11, 4.78, 0, 0);
INSERT INTO `nwt` VALUES('U461111', 'Kalb Schulter (Bug) (ma) frisch =', 16.34, 14.5, 0, 0);
INSERT INTO `nwt` VALUES('U300111', 'Kalb Fleisch frisch', 19.27, 5.28, 0, 0);
INSERT INTO `nwt` VALUES('U510111', 'Schwein Filet frisch', 22, 2, 0, 0);
INSERT INTO `nwt` VALUES('U522011', 'Schwein Kotelett mittelfett (mf) =', 19.49, 10.9, 0, 0);
INSERT INTO `nwt` VALUES('U541011', 'Schwein Schnitzel mager (ma) = Plä', 22.01, 2.69, 0, 0);
INSERT INTO `nwt` VALUES('U572011', 'Schwein Bratenfleisch mittelfett (', 18.04, 7.79, 0, 0);
INSERT INTO `nwt` VALUES('U501611', 'Schwein Fleisch (ma) geräuchert', 21.52, 5.68, 0, 0);
INSERT INTO `nwt` VALUES('U020111', 'Schwein Hackfleisch frisch', 17.79, 20.06, 0, 0);
INSERT INTO `nwt` VALUES('U661011', 'Schwein Schulter (Bug) mager (ma)', 18.99, 6.85, 0, 0);
INSERT INTO `nwt` VALUES('U500111', 'Schwein Fleisch frisch', 20.4, 8.8, 0, 0);
INSERT INTO `nwt` VALUES('W420011', 'Schwein Schinken (nach Wenk: Vorde', 19.63, 5.64, 0, 0);
INSERT INTO `nwt` VALUES('W140000', 'Salami (nach Wenk: 1. Qualität)', 26.96, 34.65, 0.19, 0);
INSERT INTO `nwt` VALUES('W231100', 'Lyoner Wurst (nach Wenk: Aufschnit', 13.42, 23.21, 0.23, 0);
INSERT INTO `nwt` VALUES('W233000', 'Fleischkäse (nach Wenk: Fleischkäs', 12.37, 23.77, 0.26, 0);
INSERT INTO `nwt` VALUES('U101811', 'Rind Fleisch (ma) gepökelt ungeräu', 38.72, 5.21, 0.83, 0);
INSERT INTO `nwt` VALUES('W400000', 'Schwein Speck und Schinken (nach W', 17.3, 39.88, 0, 0);
INSERT INTO `nwt` VALUES('W220000', 'Bratwurst (nicht umgerötet) (nach', 13.56, 23.46, 0.27, 0);
INSERT INTO `nwt` VALUES('W211111', 'Bockwurst (nach Wenk: Cervelas)', 12.69, 23.21, 0.27, 0);
INSERT INTO `nwt` VALUES('W211211', 'Wiener (nach Wenk: Wienerli)', 12.92, 24.21, 0.18, 0);
INSERT INTO `nwt` VALUES('W000011', 'Wurst (siehe Zeile 227)', 0, 0, 0.19, 0);
INSERT INTO `nwt` VALUES('U700111', 'Schaf Fleisch frisch', 17.16, 17.22, 0, 0);
INSERT INTO `nwt` VALUES('V120111', 'Ziege Fleisch frisch', 19.5, 7.88, 0, 0);
INSERT INTO `nwt` VALUES('V111111', 'Pferd Fleisch (ma) frisch', 21.39, 3, 0.4, 0);
INSERT INTO `nwt` VALUES('V400111', 'Geflügel', 19.9, 9.6, 0, 0);
INSERT INTO `nwt` VALUES('V130111', 'Hauskaninchen Fleisch frisch', 19.3, 7.62, 0, 0);
INSERT INTO `nwt` VALUES('V220111', 'Reh Fleisch frisch', 22.4, 3.55, 0, 0);
INSERT INTO `nwt` VALUES('V531111', 'Rind Leber frisch', 20.45, 3.86, 5.3, 0);
INSERT INTO `nwt` VALUES('V532111', 'Kalb Leber frisch', 20.1, 4.38, 4.59, 0);
INSERT INTO `nwt` VALUES('V533111', 'Schwein Leber frisch', 19.41, 3.31, 2.1, 0);
INSERT INTO `nwt` VALUES('V572111', 'Kalb Niere frisch', 15.76, 4.99, 1, 0);
INSERT INTO `nwt` VALUES('V591111', 'Rind Zunge frisch', 16, 13, 3.68, 0);
INSERT INTO `nwt` VALUES('E110111', 'Hühnerei frisch', 12.9, 11.2, 0.7, 0);
INSERT INTO `nwt` VALUES('T204111', 'Kabeljau frisch Fischzuschnitt', 17.4, 0.67, 0, 0);
INSERT INTO `nwt` VALUES('T311111', 'Flunder frisch Fischzuschnitt', 16.5, 3.2, 0, 0);
INSERT INTO `nwt` VALUES('T410111', 'Lachs frisch', 18.4, 6.34, 0, 0);
INSERT INTO `nwt` VALUES('T420111', 'Forelle frisch Fischzuschnitt', 20.55, 3.36, 0, 0);
INSERT INTO `nwt` VALUES('T501111', 'Karpfen frisch Fischzuschnitt', 18, 4.8, 0, 0);
INSERT INTO `nwt` VALUES('T751111', 'Krabbe klein frisch', 18.6, 1.44, 0.74, 0);
INSERT INTO `nwt` VALUES('T880001', 'Muscheltiererzeugnisse', 7.71, 1.05, 2.89, 0);
INSERT INTO `nwt` VALUES('T102702', 'Hering Konserve in Öl, abgetropft', 16.94, 15.66, 0, 0);
INSERT INTO `nwt` VALUES('T104902', 'Sardelle Konserve, abgetropft', 19.83, 2.27, 0, 0);
INSERT INTO `nwt` VALUES('T105702', 'Sardine Konserve in Öl, abgetropft', 17.03, 10.89, 0, 0);
INSERT INTO `nwt` VALUES('T121702', 'Thunfisch Konserve in Öl, abgetrop', 20.51, 15.68, 0, 0);
INSERT INTO `nwt` VALUES('M111311', 'Kuhmilch Trinkmilch vollfett', 3.3, 3.8, 4.76, 0);
INSERT INTO `nwt` VALUES('M111211', 'Kuhmilch Trinkmilch fettarm = 2.8', 3.4, 2.8, 4.9, 0);
INSERT INTO `nwt` VALUES('M110110', 'Kuhmilch Trinkmilch entrahmt', 3.5, 0.1, 5, 0);
INSERT INTO `nwt` VALUES('M141311', 'Joghurt vollfett', 4, 3.6, 4.5, 0);
INSERT INTO `nwt` VALUES('M713100', 'Quark Magerstufe', 13.5, 0.2, 4, 0);
INSERT INTO `nwt` VALUES('M713500', 'Quark Fettstufe', 9, 10.3, 3.2, 0);
INSERT INTO `nwt` VALUES('M181400', 'Kondensmilch gezuckert 7.5 % Fett', 8.2, 8, 55.5, 0);
INSERT INTO `nwt` VALUES('M882000', 'Vollmilchpulver', 25.3, 26.3, 38, 0);
INSERT INTO `nwt` VALUES('M884000', 'Magermilchpulver', 35.5, 0.9, 51.5, 0);
INSERT INTO `nwt` VALUES('M304600', 'Emmentaler Vollfettstufe', 29, 31.4, 0, 0);
INSERT INTO `nwt` VALUES('M305711', 'Greyerzer Rahmstufe vollfett', 26.9, 32.1, 0, 0);
INSERT INTO `nwt` VALUES('M306600', 'Parmesan Vollfettstufe (Sbrinz)', 28.6, 33.2, 0, 0);
INSERT INTO `nwt` VALUES('M403700', 'Tilsiter Rahmstufe vollfett', 26.1, 30.2, 0, 0);
INSERT INTO `nwt` VALUES('M301711', 'Appenzeller Rahmstufe vollfett', 24.8, 31.7, 0, 0);
INSERT INTO `nwt` VALUES('M600000', 'Weichkäse Mittelw. 6 Weichkäse', 20.2, 23.4, 0, 0);
INSERT INTO `nwt` VALUES('M401500', 'Edamer Fettstufe', 26.1, 23.4, 0, 0);
INSERT INTO `nwt` VALUES('M402600', 'Gouda Vollfettstufe', 25.5, 29.2, 0, 0);
INSERT INTO `nwt` VALUES('M730100', 'Sauermilchkäse Magerstufe', 30, 0.7, 0, 0);
INSERT INTO `nwt` VALUES('M770000', 'Schmelzkäse', 13.2, 30.4, 0.9, 0);
INSERT INTO `nwt` VALUES('M173911', 'Schlagsahne 40 % Fett-> 35% Fett', 2, 34.9, 3.1, 0);
INSERT INTO `nwt` VALUES('M173800', 'Schlagsahne 30 % Fett-> 25 % Fett', 2.45, 26.5, 3.5, 0);
INSERT INTO `nwt` VALUES('M171611', 'Kaffeesahne 15% Fett', 2.7, 15, 3.8, 0);
INSERT INTO `nwt` VALUES('Y780161', 'Käsefondue (6)', 15.41, 24.3, 0.67, 0);
INSERT INTO `nwt` VALUES('Q120000', 'Olivenöl', 0, 99.6, 0.2, 0);
INSERT INTO `nwt` VALUES('Q180000', 'Rüböl (Rapsöl)', 0, 99, 0, 0);
INSERT INTO `nwt` VALUES('Q210000', 'Erdnußöl', 0, 99.4, 0.2, 0);
INSERT INTO `nwt` VALUES('Q270000', 'Sojaöl', 0, 98.6, 0, 0);
INSERT INTO `nwt` VALUES('Q320000', 'Sonnenblumenöl', 0, 99.8, 0, 0);
INSERT INTO `nwt` VALUES('Q510000', 'Erdnußbutter/-mus', 26.1, 50, 12.2, 0);
INSERT INTO `nwt` VALUES('Q550211', 'Kokosfett gehärtet', 0.8, 99, 0.01, 0);
INSERT INTO `nwt` VALUES('Q610000', 'Butter (nach Sieber)', 0.53, 82.3, 0.6, 0);
INSERT INTO `nwt` VALUES('Q640000', 'Butter halbfett - Milchhalbfett (n', 4.9, 51.2, 1.1, 0);
INSERT INTO `nwt` VALUES('Q683011', 'Butterschmalz (nach Sieber)', 0.1, 98.3, 0, 0);
INSERT INTO `nwt` VALUES('Q860000', 'Schweineschmalz/-fett', 0.1, 99.7, 0, 0);
INSERT INTO `nwt` VALUES('Q850000', 'Rinderschmalz/-fett', 0.8, 97, 0, 0);
INSERT INTO `nwt` VALUES('P200000', 'Weißwein / Rotwein', 0.1, 0, 2.6, 8.8);
INSERT INTO `nwt` VALUES('P353111', 'Apfelwein', 0.001, 0, 7.3, 5);
INSERT INTO `nwt` VALUES('P162000', 'Bier Export Hell', 0.5, 0, 3.2, 4.2);
INSERT INTO `nwt` VALUES('P700000', 'Spirituosen', 0, 0, 0, 33.4);
INSERT INTO `nwt` VALUES('F603600', 'Orange Fruchtsaft', 0.92, 0.16, 8.79, 0);
INSERT INTO `nwt` VALUES('F110600', 'Apfel Fruchtsaft', 0.31, 0.33, 10.61, 0.2);
INSERT INTO `nwt` VALUES('F310611', 'Weintrauben Fruchtsaft', 0.63, 0.24, 15.53, 0.2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `verbrauch`
--

CREATE TABLE `verbrauch` (
  `Kennummer` int(11) NOT NULL,
  `GruppenNr` int(11) NOT NULL,
  `Nahrungsmittelarten` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `Verbrauch` float NOT NULL,
  PRIMARY KEY (`Kennummer`),
  KEY `GruppenNr` (`GruppenNr`,`Nahrungsmittelarten`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `verbrauch`
--

INSERT INTO `verbrauch` VALUES(1, 1, 'Hartweizen (in Mehl ber.)', 11.95);
INSERT INTO `verbrauch` VALUES(2, 1, 'Weichweizen (in Mehl ber.)', 51.71);
INSERT INTO `verbrauch` VALUES(3, 1, 'Dinkel (in Mehl berechnet)', 0.57);
INSERT INTO `verbrauch` VALUES(4, 1, 'Roggen (in Mehl berechnet)', 1.25);
INSERT INTO `verbrauch` VALUES(5, 1, 'Mischel (in Mehl berechnet)', 0.43);
INSERT INTO `verbrauch` VALUES(6, 1, 'Gerste (in Mehl berechnet)', 0.72);
INSERT INTO `verbrauch` VALUES(7, 1, 'Hafer (in Mehl berechnet)', 1.01);
INSERT INTO `verbrauch` VALUES(8, 1, 'Mais (in Mehl berechnet)', 1.51);
INSERT INTO `verbrauch` VALUES(9, 1, 'Reis, geschält', 4.83);
INSERT INTO `verbrauch` VALUES(10, 2, 'Kartoffeln,  -produkte', 47.07);
INSERT INTO `verbrauch` VALUES(11, 2, 'Stärke', 0.22);
INSERT INTO `verbrauch` VALUES(12, 3, 'Zucker', 43.13);
INSERT INTO `verbrauch` VALUES(13, 3, 'Sirup', 1.37);
INSERT INTO `verbrauch` VALUES(14, 3, 'Honig', 1.41);
INSERT INTO `verbrauch` VALUES(15, 4, 'Hülsenfrüchte', 1.15);
INSERT INTO `verbrauch` VALUES(16, 4, 'Erdnüsse', 0.87);
INSERT INTO `verbrauch` VALUES(17, 4, 'Mandeln', 1.12);
INSERT INTO `verbrauch` VALUES(18, 4, 'Kastanien', 0.45);
INSERT INTO `verbrauch` VALUES(19, 4, 'Baumnüsse, Haselnüsse', 2.36);
INSERT INTO `verbrauch` VALUES(20, 4, 'Kakao', 5.45);
INSERT INTO `verbrauch` VALUES(21, 5, 'Weiss- u. Rotkabis', 3.62);
INSERT INTO `verbrauch` VALUES(22, 5, 'Blumenkohl', 2.65);
INSERT INTO `verbrauch` VALUES(23, 5, 'Rosenkohl', 0.43);
INSERT INTO `verbrauch` VALUES(24, 5, 'Wirz', 1.02);
INSERT INTO `verbrauch` VALUES(25, 5, 'Kohlrabi', 0.65);
INSERT INTO `verbrauch` VALUES(26, 5, 'Broccoli', 0.92);
INSERT INTO `verbrauch` VALUES(27, 5, 'andere Kohlgewächse', 1.13);
INSERT INTO `verbrauch` VALUES(28, 5, 'Randen', 1.72);
INSERT INTO `verbrauch` VALUES(29, 5, 'Karotten', 8.73);
INSERT INTO `verbrauch` VALUES(30, 5, 'Weisse Rüben', 0.73);
INSERT INTO `verbrauch` VALUES(31, 5, 'Sellerie', 1.34);
INSERT INTO `verbrauch` VALUES(32, 5, 'Fenchel', 1.59);
INSERT INTO `verbrauch` VALUES(33, 5, 'Schwarzwurzeln', 0.04);
INSERT INTO `verbrauch` VALUES(34, 5, 'Radieschen', 0.24);
INSERT INTO `verbrauch` VALUES(35, 5, 'Rettiche', 0.42);
INSERT INTO `verbrauch` VALUES(36, 5, 'andere Wurzel u.Knol', 1.74);
INSERT INTO `verbrauch` VALUES(37, 5, 'Artischocken', 0.21);
INSERT INTO `verbrauch` VALUES(38, 5, 'Rhabarber', 0.32);
INSERT INTO `verbrauch` VALUES(39, 5, 'Mangold', 0.39);
INSERT INTO `verbrauch` VALUES(40, 5, 'Spargeln', 1.2);
INSERT INTO `verbrauch` VALUES(41, 5, 'Treibzichorien', 1.99);
INSERT INTO `verbrauch` VALUES(42, 5, 'Kopfsalat, Lattich', 3.77);
INSERT INTO `verbrauch` VALUES(43, 5, 'Endivien', 2.55);
INSERT INTO `verbrauch` VALUES(44, 5, 'Nüsslisalat', 0.55);
INSERT INTO `verbrauch` VALUES(45, 5, 'Zuckerhut', 0.6);
INSERT INTO `verbrauch` VALUES(46, 5, 'Spinat', 2.75);
INSERT INTO `verbrauch` VALUES(47, 5, 'andere Salate', 5.48);
INSERT INTO `verbrauch` VALUES(48, 5, 'Bohnen', 2.18);
INSERT INTO `verbrauch` VALUES(49, 5, 'Erbsen', 1.03);
INSERT INTO `verbrauch` VALUES(50, 5, 'Tomaten', 9.08);
INSERT INTO `verbrauch` VALUES(51, 5, 'Auberginen', 0.53);
INSERT INTO `verbrauch` VALUES(52, 5, 'Zwiebeln, Knoblauch', 4.79);
INSERT INTO `verbrauch` VALUES(53, 5, 'Lauch', 1.83);
INSERT INTO `verbrauch` VALUES(54, 5, 'Gurken', 3.95);
INSERT INTO `verbrauch` VALUES(55, 5, 'Peperoni grün/rot', 2.16);
INSERT INTO `verbrauch` VALUES(56, 5, 'Zucchetti', 1.71);
INSERT INTO `verbrauch` VALUES(57, 5, 'Schnittlauch Petersilie', 0.46);
INSERT INTO `verbrauch` VALUES(58, 5, 'andere Gemüse', 0.16);
INSERT INTO `verbrauch` VALUES(59, 5, 'Pilze', 1.38);
INSERT INTO `verbrauch` VALUES(60, 5, 'Konserven (in Frischgewicht)', 14.15);
INSERT INTO `verbrauch` VALUES(61, 6, 'Äpfel', 18.45);
INSERT INTO `verbrauch` VALUES(62, 6, 'Birnen', 4.22);
INSERT INTO `verbrauch` VALUES(63, 6, 'Kirschen', 1.61);
INSERT INTO `verbrauch` VALUES(64, 6, 'Zwetschgen und Pflaumen', 0.97);
INSERT INTO `verbrauch` VALUES(65, 6, 'Aprikosen', 2.52);
INSERT INTO `verbrauch` VALUES(66, 6, 'Pfirsiche', 4.43);
INSERT INTO `verbrauch` VALUES(67, 6, 'Erdbeeren', 3.53);
INSERT INTO `verbrauch` VALUES(68, 6, 'Beeren', 3.2);
INSERT INTO `verbrauch` VALUES(69, 6, 'Melonen', 4.64);
INSERT INTO `verbrauch` VALUES(70, 6, 'Trauben', 5.5);
INSERT INTO `verbrauch` VALUES(71, 6, 'Bananen', 9.51);
INSERT INTO `verbrauch` VALUES(72, 6, 'Orangen, Mandarinen', 14.74);
INSERT INTO `verbrauch` VALUES(73, 6, 'Zitronen', 2.61);
INSERT INTO `verbrauch` VALUES(74, 6, 'Grapefruits', 1.47);
INSERT INTO `verbrauch` VALUES(75, 6, 'Anderes frisches Obst', 3.04);
INSERT INTO `verbrauch` VALUES(76, 6, 'Andere Obstkonserven', 4.27);
INSERT INTO `verbrauch` VALUES(77, 7, 'Rind, Filet, Entrecote', 1.36);
INSERT INTO `verbrauch` VALUES(78, 7, 'Rind, Braten', 0.59);
INSERT INTO `verbrauch` VALUES(79, 7, 'Rind, Gehacktes', 2.54);
INSERT INTO `verbrauch` VALUES(80, 7, 'Rind, Siedfleisch', 0.76);
INSERT INTO `verbrauch` VALUES(81, 7, 'Rind, Voressen', 0.67);
INSERT INTO `verbrauch` VALUES(82, 7, 'Rind, übriges', 1.44);
INSERT INTO `verbrauch` VALUES(83, 7, 'Kalb, Plätzli', 0.64);
INSERT INTO `verbrauch` VALUES(84, 7, 'Kalb, Braten', 0.25);
INSERT INTO `verbrauch` VALUES(85, 7, 'Kalb, Voressen', 0.38);
INSERT INTO `verbrauch` VALUES(86, 7, 'Kalb, übriges', 1);
INSERT INTO `verbrauch` VALUES(87, 7, 'Schwein, Filet', 0.35);
INSERT INTO `verbrauch` VALUES(88, 7, 'Schwein, Koteletten', 2.02);
INSERT INTO `verbrauch` VALUES(89, 7, 'Schwein, Plätzli', 1.72);
INSERT INTO `verbrauch` VALUES(90, 7, 'Schwein, Braten', 1.74);
INSERT INTO `verbrauch` VALUES(91, 7, 'Schwein, Geräuchertes', 2.18);
INSERT INTO `verbrauch` VALUES(92, 7, 'Schwein, Geschnetzeltes', 0.56);
INSERT INTO `verbrauch` VALUES(93, 7, 'Schwein, Voressen', 0.91);
INSERT INTO `verbrauch` VALUES(94, 7, 'Schwein, übriges', 1.48);
INSERT INTO `verbrauch` VALUES(95, 7, 'Schinken', 2.54);
INSERT INTO `verbrauch` VALUES(96, 7, 'Salami', 0.6);
INSERT INTO `verbrauch` VALUES(97, 7, 'Aufschnitt', 0.94);
INSERT INTO `verbrauch` VALUES(98, 7, 'Fleischkäse', 1.31);
INSERT INTO `verbrauch` VALUES(99, 7, 'Trockenfleisch', 0.77);
INSERT INTO `verbrauch` VALUES(100, 7, 'übrige Charcuterie', 1.5);
INSERT INTO `verbrauch` VALUES(101, 7, 'Bratwürste', 2.58);
INSERT INTO `verbrauch` VALUES(102, 7, 'Cervelas', 2.96);
INSERT INTO `verbrauch` VALUES(103, 7, 'Wienerli', 2.32);
INSERT INTO `verbrauch` VALUES(104, 7, 'übrige Wurst', 5.03);
INSERT INTO `verbrauch` VALUES(105, 7, 'Schaffleisch', 1.26);
INSERT INTO `verbrauch` VALUES(106, 7, 'Ziegenfleisch', 0.09);
INSERT INTO `verbrauch` VALUES(107, 7, 'Pferdefleisch', 0.63);
INSERT INTO `verbrauch` VALUES(108, 7, 'Geflügel', 10.25);
INSERT INTO `verbrauch` VALUES(109, 7, 'Kaninchen', 0.66);
INSERT INTO `verbrauch` VALUES(110, 7, 'Wild', 0.75);
INSERT INTO `verbrauch` VALUES(111, 7, 'Organteile', 2.61);
INSERT INTO `verbrauch` VALUES(112, 8, 'Eier und Eikonserven', 10.62);
INSERT INTO `verbrauch` VALUES(113, 9, 'Frisch', 3.97);
INSERT INTO `verbrauch` VALUES(114, 9, 'Konserven', 3.77);
INSERT INTO `verbrauch` VALUES(115, 10, 'Vollmilch', 69.7);
INSERT INTO `verbrauch` VALUES(116, 10, 'Standardisierte Milch', 24.05);
INSERT INTO `verbrauch` VALUES(117, 10, 'Magermilch', 3);
INSERT INTO `verbrauch` VALUES(118, 10, 'Joghurt', 16.9);
INSERT INTO `verbrauch` VALUES(119, 10, 'Quark', 1.85);
INSERT INTO `verbrauch` VALUES(120, 10, 'Kondensmilch', 0.29);
INSERT INTO `verbrauch` VALUES(121, 10, 'Vollmilchpulver', 0.4);
INSERT INTO `verbrauch` VALUES(122, 10, 'Magermilchpulver', 2.84);
INSERT INTO `verbrauch` VALUES(123, 10, 'Emmentaler', 1.13);
INSERT INTO `verbrauch` VALUES(124, 10, 'Greyerzer', 1.98);
INSERT INTO `verbrauch` VALUES(125, 10, 'Sbrinz', 0.32);
INSERT INTO `verbrauch` VALUES(126, 10, 'Tilsiter', 0.77);
INSERT INTO `verbrauch` VALUES(127, 10, 'Appenzeller', 0.43);
INSERT INTO `verbrauch` VALUES(128, 10, 'Weichkäse', 3.71);
INSERT INTO `verbrauch` VALUES(129, 10, 'andere Hart- u. HalbhartKäse', 4.9);
INSERT INTO `verbrauch` VALUES(130, 10, 'Magerkäse', 0.15);
INSERT INTO `verbrauch` VALUES(131, 10, 'Schmelzkäse', 1.7);
INSERT INTO `verbrauch` VALUES(132, 10, 'Vollrahm', 3.3);
INSERT INTO `verbrauch` VALUES(133, 10, 'Halbrahm', 1.7);
INSERT INTO `verbrauch` VALUES(134, 10, 'Kaffeerahm', 4.6);
INSERT INTO `verbrauch` VALUES(135, 10, 'Fertigfondue', 0.5);
INSERT INTO `verbrauch` VALUES(136, 11, 'Olivenöl', 0.42);
INSERT INTO `verbrauch` VALUES(137, 11, 'Übrige pfl. Öle, Fette', 13.25);
INSERT INTO `verbrauch` VALUES(138, 11, 'Butter', 5.25);
INSERT INTO `verbrauch` VALUES(139, 11, 'Kalorienverminderte Butter', 0.03);
INSERT INTO `verbrauch` VALUES(140, 11, 'Eingesottene Butter', 0.94);
INSERT INTO `verbrauch` VALUES(141, 11, 'Schweineschmalz', 0.91);
INSERT INTO `verbrauch` VALUES(142, 11, 'Rinderfett', 1.49);
INSERT INTO `verbrauch` VALUES(143, 12, 'Wein', 53.3);
INSERT INTO `verbrauch` VALUES(144, 12, 'Obstwein', 3.9);
INSERT INTO `verbrauch` VALUES(145, 12, 'Bier', 77.2);
INSERT INTO `verbrauch` VALUES(146, 12, 'Branntwein', 4.6);
INSERT INTO `verbrauch` VALUES(147, 13, 'Fruchtsäfte', 13.17);
