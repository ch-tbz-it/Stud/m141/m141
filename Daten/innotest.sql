-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';

-- -----------------------------------------------------
-- Schema innotest
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `innotest` ;

-- -----------------------------------------------------
-- Schema innotest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `innotest` DEFAULT CHARACTER SET utf8mb4 ;
USE `innotest` ;

-- -----------------------------------------------------
-- Table `innotest`.`tbl_konto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `innotest`.`tbl_konto` ;

CREATE TABLE IF NOT EXISTS `innotest`.`tbl_konto` (
  `ID_k` INT NOT NULL,
  `Name` VARCHAR(30) NULL,
  `Saldo` DECIMAL(10,2) NULL,
  PRIMARY KEY (`ID_k`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
