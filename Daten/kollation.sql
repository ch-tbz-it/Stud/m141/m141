-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 27. Feb 2024 um 15:45
-- Server-Version: 10.11.4-MariaDB
-- PHP-Version: 8.2.4


CREATE TABLE `tbl_personen` (
  `ID_person` int(11) NOT NULL,
  `Vorname` varchar(11) NOT NULL,
  `latin1_general_cs` varchar(48) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `latin1_german1_ci` varchar(62) CHARACTER SET latin1 COLLATE latin1_german1_ci NOT NULL,
  `latin1_german2_ci` varchar(64) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  `utf8-german2_ci` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL,
  `utf8_general_ci` varchar(50) NOT NULL,
  `utf8-unicode-ci` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Wohnort` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_personen`
--

INSERT INTO `tbl_personen` (`ID_person`, `Vorname`, `latin1_german2_ci`, `latin1_general_cs`, `latin1_german1_ci`, `utf8-german2_ci`, `utf8_general_ci`, `utf8-unicode-ci`, `Wohnort`) VALUES
(1, 'Skandal', 'Öx', 'Öx', 'Öx', 'Öx', 'Öx', 'Öx', 'Rio de Janeiro'),
(2, 'Herb', 'Ox', 'Ox', 'Ox', 'Ox', 'Ox', 'Ox', 'Hamburg'),
(3, 'Karl-Heinz-', 'Oy', 'Oy', 'Oy', 'Oy', 'Oy', 'Oy', 'irgendwo'),
(4, 'Betrug', 'Oex', 'Oex', 'Oex', 'Oex', 'Oex', 'Oex', 'Rio de Janeiro'),
(5, 'Max', 'öx', 'öx', 'öx', 'öx', 'öx', 'öx', 'Istanbul'),
(6, 'Karla', 'öy', 'öy', 'öy', 'öy', 'öy', 'öy', 'Österreich'),
(7, 'Valentin', 'Öy', 'Öy', 'Öy', 'Öy', 'Öy', 'Öy', 'Gockhausen'),
(8, 'Betrug', 'oex', 'oex', 'oex', 'oex', 'oex', 'oex', 'Rio de Janeiro');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_personen`
--
ALTER TABLE `tbl_personen`
  ADD PRIMARY KEY (`ID_person`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_personen`
--
ALTER TABLE `tbl_personen`
  MODIFY `ID_person` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

