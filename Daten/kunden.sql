-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 12. Mrz 2024 um 19:54
-- Server-Version: 10.11.4-MariaDB
-- PHP-Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `kunden`
--
CREATE DATABASE IF NOT EXISTS `kunden` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `kunden`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `abteilung`
--

DROP TABLE IF EXISTS `abteilung`;
CREATE TABLE IF NOT EXISTS `abteilung` (
  `id_Abteilung` int(11) NOT NULL AUTO_INCREMENT,
  `Bezeichnnug` varchar(20) NOT NULL,
  `Kuerzel` varchar(5) NOT NULL,
  PRIMARY KEY (`id_Abteilung`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `abteilung`
--

INSERT INTO `abteilung` (`id_Abteilung`, `Bezeichnnug`, `Kuerzel`) VALUES
(1, 'Verkauf', 'Vkf'),
(2, 'Management', 'Mng');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunden`
--

DROP TABLE IF EXISTS `kunden`;
CREATE TABLE IF NOT EXISTS `kunden` (
  `id_Kunde` int(11) NOT NULL AUTO_INCREMENT,
  `Vorname` varchar(20) NOT NULL,
  `Nachname` varchar(30) NOT NULL,
  `Adresse` varchar(50) NOT NULL,
  `Nr` varchar(10) NOT NULL,
  `fk_PLZ` varchar(5) NOT NULL,
  PRIMARY KEY (`id_Kunde`),
  KEY `fk_PLZ` (`fk_PLZ`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `kunden`
--

INSERT INTO `kunden` (`id_Kunde`, `Vorname`, `Nachname`, `Adresse`, `Nr`, `fk_PLZ`) VALUES
(1, 'John',     'Deer', '', '', '79111'),
(2, 'Herbert',  'Grönemeier', '', '', '79312'),
(3, 'Sabina',   'Seller', '', '', '79312'),
(4, 'Mary',     'Web', '', '', '79111'),
(5, 'Heinrich', 'Holle', '', '', '79111'),
(6, 'Usal',     'Bolt', '', '', '80995'),
(7, 'Johannes', 'Hartl', '', '', '80995'),
(8, 'Carla',    'Bruni', '', '', '79312'),
(9, 'Ludowika', 'Cortcz', '', '', '79111'),
(10, 'Niemand', '', '', '', '99999');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orte`
--

DROP TABLE IF EXISTS `orte`;
CREATE TABLE IF NOT EXISTS `orte` (
  `id_PLZ` varchar(5) NOT NULL,
  `Bzeichnung` varchar(255) NOT NULL,
  `Einwohnerzahl` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_PLZ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `orte`
--

INSERT INTO `orte` (`id_PLZ`, `Bzeichnung`, `Einwohnerzahl`) VALUES
('20095', 'Hamburg', 2000000),
('79111', 'Freiburg', 280000),
('79312', 'Emmendingen', 40000),
('80995', 'München', 1000000);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `personal`
--

DROP TABLE IF EXISTS `personal`;
CREATE TABLE IF NOT EXISTS `personal` (
  `id_Personal` int(11) NOT NULL AUTO_INCREMENT,
  `Vorname` varchar(30) DEFAULT NULL,
  `Nachname` varchar(30) DEFAULT NULL,
  `Lohn` decimal(8,2) NOT NULL,
  `Adresse` varchar(30) NOT NULL,
  `Nr` varchar(10) NOT NULL,
  `fk_PLZ` int(11) NOT NULL,
  `fk_Abteilung` int(11) NOT NULL,
  PRIMARY KEY (`id_Personal`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `personal`
--

INSERT INTO `personal` (`id_Personal`, `Vorname`, `Nachname`, `Lohn`, `Adresse`, `Nr`, `fk_PLZ`, `fk_Abteilung`) VALUES
(1, 'Sepp', 'Troschel', 112200.00, '', '', 79312, 1),
(2, 'Guido', 'Huber', 139000.00, '', '', 79111, 1),
(3, 'Hanna', 'Feist', 94000.00, '', '', 79312, 2),
(4, 'Susi', 'Rutz', 120000.00, '', '', 79111, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `position`
--

DROP TABLE IF EXISTS `position`;
CREATE TABLE IF NOT EXISTS `position` (
  `id_Position` int(11) NOT NULL AUTO_INCREMENT,
  `fk_Rechung` int(11) DEFAULT NULL,
  `fk_Produkt` int(11) DEFAULT NULL,
  `Anzahl` int(11) NOT NULL,
  PRIMARY KEY (`id_Position`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `position`
--

INSERT INTO `position` (`id_Position`, `fk_Rechung`, `fk_Produkt`, `Anzahl`) VALUES
(1, 1, 2, 3),
(2, 2, 1, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `produkte`
--

DROP TABLE IF EXISTS `produkte`;
CREATE TABLE IF NOT EXISTS `produkte` (
  `id_Produkte` int(11) NOT NULL AUTO_INCREMENT,
  `Bezeichnung` varchar(30) NOT NULL,
  `Preis` decimal(8,2) NOT NULL,
  PRIMARY KEY (`id_Produkte`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `produkte`
--

INSERT INTO `produkte` (`id_Produkte`, `Bezeichnung`, `Preis`) VALUES
(1, 'AcColor 234', 0.00),
(2, 'AcColor 12', 0.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rechnung`
--

DROP TABLE IF EXISTS `rechnung`;
CREATE TABLE IF NOT EXISTS `rechnung` (
  `id_Rechnung` int(11) NOT NULL AUTO_INCREMENT,
  `fk_Kunde` int(11) DEFAULT NULL,
  `fk_Personal` int(11) NOT NULL,
  `R-Datum` date NOT NULL,
  `Rabatt` decimal(3,0) NOT NULL,
  `MwSt` decimal(3,0) NOT NULL,
  PRIMARY KEY (`id_Rechnung`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `rechnung`
--

INSERT INTO `rechnung` (`id_Rechnung`, `fk_Kunde`, `fk_Personal`, `R-Datum`, `Rabatt`, `MwSt`) VALUES
(1, 1, 4, '2024-03-03', 5, 7),
(2, 2, 3, '2024-03-04', 10, 7);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
