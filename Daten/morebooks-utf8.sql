-- *****************************************************************************
-- *
-- *  morebooks.sql : Erstellt Struktur und Daten der Buecher-DB mit 100 Titeln
-- *  -- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- *  Modul        : 141
-- *  Autor, Datum : R. Rueegg, 6.9.2003
-- *  Version      : V1.3
-- * 
-- *  Funktion     : Erstellt die 4 Tabellen autoren, titel, titel_autoren, verlage 
-- *		        und tr�gt 100 Titel ein sowie zugeh�rige Verlage und Autoren.
-- *                 - Fehlende Jahre sind mit NULL eingetragen.
-- *                 - Gleichnamige existierende Tabellen werden gel�scht.
-- *                 - In Tabelle autoren wird pro Datensatz nur 1 Autor eingetragen.
-- *                 - Es sind Autoren und Verlage ohne Titel vorhanden.
-- *                 - Es sind "doppelte" Autoren und Verlage vorhanden.
-- *
-- *  Anwendung    : >mysql datenbank -u user -p password < morebooks.sql
-- *
-- *****************************************************************************

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Create table autoren
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

DROP TABLE IF EXISTS autoren;
CREATE TABLE autoren (
  autor_nr int(11) NOT NULL auto_increment,
  autor varchar(80) NOT NULL default '',
  PRIMARY KEY  (autor_nr)
) ENGINE=InnoDB;

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Insert data into table autoren
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

INSERT INTO autoren VALUES (143,'Agular, Robert R.');
INSERT INTO autoren VALUES (371,'Ammelburger, Dirk');
INSERT INTO autoren VALUES (394,'Anders, Neal');
INSERT INTO autoren VALUES (693,'Atkinson, Leon');
INSERT INTO autoren VALUES (742,'Aulds, Charles');
INSERT INTO autoren VALUES (1427,'Becker');
INSERT INTO autoren VALUES (1432,'Becker R');
INSERT INTO autoren VALUES (1611,'Beneken, Gerd');
INSERT INTO autoren VALUES (1855,'Betz, Friedhelm');
INSERT INTO autoren VALUES (2484,'Born, G�nter');
INSERT INTO autoren VALUES (3007,'Brown, Micah');
INSERT INTO autoren VALUES (3040,'Br�derl, Markus');
INSERT INTO autoren VALUES (3223,'Bulger, Brad');
INSERT INTO autoren VALUES (3262,'Burger');
INSERT INTO autoren VALUES (3267,'B�rger, Michael');
INSERT INTO autoren VALUES (3429,'Buyens, Jim');
INSERT INTO autoren VALUES (3957,'Chung, Dirk');
INSERT INTO autoren VALUES (4352,'Cox, Robert M.');
INSERT INTO autoren VALUES (4633,'Danverio L');
INSERT INTO autoren VALUES (4668,'D��ler, Rolf');
INSERT INTO autoren VALUES (4795,'Debes, Norbert');
INSERT INTO autoren VALUES (5207,'Dittrich, Klaus R.');
INSERT INTO autoren VALUES (5684,'Ebner, Michael');
INSERT INTO autoren VALUES (5940,'Elmasri, Ramez');
INSERT INTO autoren VALUES (6416,'Fellner Stefan');
INSERT INTO autoren VALUES (6481,'Feth, Joscha');
INSERT INTO autoren VALUES (6934,'Franke, Jochen');
INSERT INTO autoren VALUES (7218,'Frisch, Max');
INSERT INTO autoren VALUES (7316,'Gacki, Robert');
INSERT INTO autoren VALUES (7473,'Gatziu, Stella');
INSERT INTO autoren VALUES (7647,'Geppert, Andreas');
INSERT INTO autoren VALUES (7677,'Gerken, Till');
INSERT INTO autoren VALUES (8383,'Greenspan, Jay');
INSERT INTO autoren VALUES (8756,'Gutierrez D');
INSERT INTO autoren VALUES (9218,'Harms F');
INSERT INTO autoren VALUES (9220,'Harms, Florian');
INSERT INTO autoren VALUES (9428,'Hauser, Tobias');
INSERT INTO autoren VALUES (9533,'Hegi, Roland');
INSERT INTO autoren VALUES (10000,'Heuer, Andreas');
INSERT INTO autoren VALUES (10516,'Holzschlag, Molly E.');
INSERT INTO autoren VALUES (11574,'Kabir, Mohammed');
INSERT INTO autoren VALUES (11575,'Kabir, Mohammed J.');
INSERT INTO autoren VALUES (11827,'K�ufer, Mechtild');
INSERT INTO autoren VALUES (12027,'Kennedy, Bill');
INSERT INTO autoren VALUES (12294,'Kerouac, Jack');
INSERT INTO autoren VALUES (12341,'Klaas, Uwe');
INSERT INTO autoren VALUES (12456,'Klewes, Joachim');
INSERT INTO autoren VALUES (12504,'Kloss, Birgit');
INSERT INTO autoren VALUES (12649,'Koch, Daniel');
INSERT INTO autoren VALUES (12698,'Kofler, Michael');
INSERT INTO autoren VALUES (12699,'Kofler, Michael');
INSERT INTO autoren VALUES (12826,'Konopasek, Klemens');
INSERT INTO autoren VALUES (12915,'Kosch, Andreas');
INSERT INTO autoren VALUES (13057,'Krause, J�rg');
INSERT INTO autoren VALUES (13202,'Krizanek, Milan');
INSERT INTO autoren VALUES (13484,'K�rten, Oliver');
INSERT INTO autoren VALUES (14039,'Leierer G');
INSERT INTO autoren VALUES (14041,'Leierer, Gudrun Anna');
INSERT INTO autoren VALUES (14207,'Letzel, Sven');
INSERT INTO autoren VALUES (14511,'Livingston, Dan');
INSERT INTO autoren VALUES (14541,'Lockemann, Peter C.');
INSERT INTO autoren VALUES (14545,'Lockman, David');
INSERT INTO autoren VALUES (15188,'Mann, Markus');
INSERT INTO autoren VALUES (15209,'Mansfield, Richard');
INSERT INTO autoren VALUES (15511,'Matthiessen, G�nter');
INSERT INTO autoren VALUES (15835,'Meier, Andreas');
INSERT INTO autoren VALUES (16123,'Moore, Michael');
INSERT INTO autoren VALUES (16794,'M�ller, Sara');
INSERT INTO autoren VALUES (16913,'Musciano, Chuck');
INSERT INTO autoren VALUES (16943,'N.N.');
INSERT INTO autoren VALUES (17049,'Navathe, Shamkant B.');
INSERT INTO autoren VALUES (17068,'Nefzger, Wolfgang');
INSERT INTO autoren VALUES (18339,'Petkovic, Dusan');
INSERT INTO autoren VALUES (18464,'Piatkowski, Marko');
INSERT INTO autoren VALUES (19014,'P�rner, Heinz A.');
INSERT INTO autoren VALUES (19246,'Ratschiller, Tobias');
INSERT INTO autoren VALUES (19323,'Read, Paul');
INSERT INTO autoren VALUES (20590,'Samar, Richard');
INSERT INTO autoren VALUES (20714,'Sauer, Hermann');
INSERT INTO autoren VALUES (21385,'Schnitker, Michael');
INSERT INTO autoren VALUES (21687,'Scott, Adams');
INSERT INTO autoren VALUES (22524,'Skulschus, Marco');
INSERT INTO autoren VALUES (22887,'Spona, Helma');
INSERT INTO autoren VALUES (23320,'Steyer, Ralph');
INSERT INTO autoren VALUES (23366,'Stocker, Christian');
INSERT INTO autoren VALUES (23399,'Stoll, Rolf');
INSERT INTO autoren VALUES (23400,'Stoll, Rolf D.');
INSERT INTO autoren VALUES (23444,'St�rl, Uta');
INSERT INTO autoren VALUES (23772,'Taglinger, Harald');
INSERT INTO autoren VALUES (23866,'Teague, Jason Cranford');
INSERT INTO autoren VALUES (24199,'Tiemeyer, Ernst');
INSERT INTO autoren VALUES (24273,'Tolksdorf R');
INSERT INTO autoren VALUES (24274,'Tolksdorf, Robert');
INSERT INTO autoren VALUES (24671,'Unterstein, Michael');
INSERT INTO autoren VALUES (25260,'Walker, Michael J.');
INSERT INTO autoren VALUES (25517,'Webster, Steve');
INSERT INTO autoren VALUES (25620,'Weinman, Lynda');
INSERT INTO autoren VALUES (25621,'Weinman, William');
INSERT INTO autoren VALUES (25758,'Wenz, Christian');
INSERT INTO autoren VALUES (25958,'Wiederstein, Marcus');
INSERT INTO autoren VALUES (26127,'Williamson, Heather');
INSERT INTO autoren VALUES (26534,'Wurch, S�ren');
INSERT INTO autoren VALUES (26551,'W�st, Thomas');
INSERT INTO autoren VALUES (26682,'Zandstra, Matt');
INSERT INTO autoren VALUES (26854,'Zimmermann, J�rgen');

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Create table titel
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

DROP TABLE IF EXISTS titel;
CREATE TABLE titel (
  titel_nr int(11) NOT NULL auto_increment,
  verlag_nr int(11) NOT NULL default '0',
  titel varchar(70) NOT NULL default '',
  preis decimal(5,2) default NULL,
  isbn varchar(18) default NULL,
  jahr int(4) default NULL,
  PRIMARY KEY  (titel_nr)
) ENGINE=InnoDB;

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Insert data into table titel
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

INSERT INTO titel VALUES (2,430,'(X)HTML',45.00,'3-8158-2124-X',2001);
INSERT INTO titel VALUES (3,430,'(X)HTML und XML',53.00,'3-8158-2105-3',2000);
INSERT INTO titel VALUES (1335,1511,'AdressPlus Vollversion (d) Win Datenbank, Adressverwaltung COBRA',89.00,'600181533X',NULL);
INSERT INTO titel VALUES (1637,506,'Aktive Datenbanksysteme',44.00,'3-932588-19-3',2000);
INSERT INTO titel VALUES (3066,506,'Architektur von Datenbanksystemen',81.00,'3-89864-170-8',2002);
INSERT INTO titel VALUES (4186,1968,'Backup und Recovery in Datenbanksystemen',59.90,'3-519-00416-X',2001);
INSERT INTO titel VALUES (6071,1839,'Brennpunkt Datenbanken, m. CD-ROM',29.00,'3-908490-45-6',NULL);
INSERT INTO titel VALUES (6625,703,'C#. Das Profihandbuch f�r die Datenbank-Programmierung',84.90,'3-7723-6896-4',2002);
INSERT INTO titel VALUES (7096,1839,'CGI/Perl, JavaScript, HTML, m. CD-ROM',79.00,'3-908491-21-5',NULL);
INSERT INTO titel VALUES (7693,1842,'Client/Server Datenbankentwicklung mit Delphi',78.00,'3-9806738-1-2',1999);
INSERT INTO titel VALUES (8079,1571,'Core MySQL',82.00,'0-13-066190-2',2002);
INSERT INTO titel VALUES (8080,1244,'Core PHP 4 Programmierung',92.00,'3-8272-5809-X',2001);
INSERT INTO titel VALUES (8300,1244,'creative html design.2',88.00,'3-8272-6011-6',2001);
INSERT INTO titel VALUES (8350,1571,'CSS and DHTML',55.00,'0-13-064995-3',2001);
INSERT INTO titel VALUES (8352,1244,'CSS und DHTML',53.00,'3-8272-6294-1',2002);
INSERT INTO titel VALUES (9034,1244,'Das eigene Web mit HTML, CSS und JavaScript',35.00,'3-8272-6306-9',2002);
INSERT INTO titel VALUES (9553,430,'Das gro�e Buch HTML und XML, m. CD-ROM',45.00,'3-8158-1377-8',NULL);
INSERT INTO titel VALUES (11404,1989,'Datenbankprogrammierung mit MySQLund PHP',16.90,'3-8266-7211-9',2002);
INSERT INTO titel VALUES (11457,1989,'DB2 UDB Datenbank-Administration',74.00,'3-8266-0544-6',2001);
INSERT INTO titel VALUES (11626,23,'Delphi 5 Datenbankprogrammierung',88.00,'3-8273-1570-0',2000);
INSERT INTO titel VALUES (11632,23,'Delphi 6 Datenbankprogrammierung',99.00,'3-8273-1785-1',2002);
INSERT INTO titel VALUES (12021,430,'Der Data Becker-F�hrer. HTML 4',14.00,'3-8158-1664-5',2001);
INSERT INTO titel VALUES (12022,430,'Der Data Becker-F�hrer. HTML 4',18.00,'3-8158-2103-7',2000);
INSERT INTO titel VALUES (13764,1839,'Der Smart-Books HTML-Kurs, m. CD-ROM',38.00,'3-908489-58-X',NULL);
INSERT INTO titel VALUES (14745,1693,'DHTML',18.50,'3-499-60071-4',1999);
INSERT INTO titel VALUES (14746,22,'DHTML and CSS for the World Wide Web',41.00,'0-201-73084-7',2001);
INSERT INTO titel VALUES (15036,1989,'Die Apache Administration Bibel',67.90,'3-8266-0896-8',2002);
INSERT INTO titel VALUES (15037,1989,'Die Apache Administration Bibel',73.00,'3-8266-0554-3',1999);
INSERT INTO titel VALUES (16198,1941,'Die gro�e HTML & JavaScript Fibel',35.00,'3-8155-0313-2',NULL);
INSERT INTO titel VALUES (18019,506,'Die Sprache des Web. HTML 4.0',52.00,'3-920993-77-2',NULL);
INSERT INTO titel VALUES (18020,506,'Die Sprachen des Web: HTML und XHTML',56.00,'3-89864-155-4',2001);
INSERT INTO titel VALUES (18021,506,'Die Sprachen des Web: HTML und XHTML',56.00,'3-932588-58-4',2000);
INSERT INTO titel VALUES (19797,1989,'Dynamic HTML',37.00,'3-8266-9000-1',2000);
INSERT INTO titel VALUES (19798,224,'Dynamic HTML',46.00,'3-8287-2027-7',2000);
INSERT INTO titel VALUES (19799,728,'Dynamic HTML browser�bergreifend',63.00,'3-934358-29-2',2001);
INSERT INTO titel VALUES (19804,1244,'Dynamische Webseiten mit Flash und PHP',78.00,'3-8272-6285-2',2002);
INSERT INTO titel VALUES (19805,703,'Dynamische Webseiten mit PHP 4',27.90,'3-7723-7842-0',2002);
INSERT INTO titel VALUES (19806,1839,'Dynamische Websites mit ColdFusion, PHP, IIS 5.0. Apache, Microsoft SQ',79.00,'3-908491-06-1',2001);
INSERT INTO titel VALUES (22300,1571,'Essential CSS and DHTML for Web Professionals',56.00,'0-13-012760-4',1999);
INSERT INTO titel VALUES (24039,2228,'Foundation PHP for Flash',76.00,'1-903450-16-0',2001);
INSERT INTO titel VALUES (26482,23,'Go To PHP 4',73.00,'3-8273-1759-2',2002);
INSERT INTO titel VALUES (27185,23,'Grundlagen von Datenbanksystemen',110.00,'3-8273-7021-3',2002);
INSERT INTO titel VALUES (27911,1868,'H�rder, Theo Datenbanksysteme * Konzepte und Techniken der Implementie',72.00,'3540650407',NULL);
INSERT INTO titel VALUES (29251,872,'HTML / XHTML',16.40,'3-453-18183-2',2001);
INSERT INTO titel VALUES (29252,1989,'HTML / XHTML ge-packt',21.90,'3-8266-0695-7',2001);
INSERT INTO titel VALUES (29261,1299,'HTML 4 XHTML. Das Handbuch',59.90,'3-86063-165-9',2002);
INSERT INTO titel VALUES (29280,1462,'HTML und XHTML',67.90,'3-89721-168-8',2001);
INSERT INTO titel VALUES (30706,1257,'Instant PHP4',107.00,'0-07-217074-3',2001);
INSERT INTO titel VALUES (30762,1842,'InterBase Datenbankentwicklung mit Delphi',67.90,'3-935042-09-4',2001);
INSERT INTO titel VALUES (31811,23,'Java in Datenbanksystemen',73.00,'3-8273-1889-0',2002);
INSERT INTO titel VALUES (31882,1244,'JavaScript und HTML',39.50,'3-8272-5996-7',2001);
INSERT INTO titel VALUES (32087,1244,'Jetzt lerne ich HTML',35.00,'3-8272-5717-4',2001);
INSERT INTO titel VALUES (32096,1244,'Jetzt lerne ich MySQL und PHP',45.00,'3-8272-6202-X',2001);
INSERT INTO titel VALUES (32104,1244,'Jetzt lerne ich PHP 4',53.00,'3-8272-5883-9',2001);
INSERT INTO titel VALUES (35660,1974,'Lernkurs HTML Chip (CHIP)(D)',34.90,'6002036431',NULL);
INSERT INTO titel VALUES (36348,1941,'Linux Apache Server Administrator',78.00,'3-8155-0322-1',2001);
INSERT INTO titel VALUES (36349,1940,'Linux Apache Web Server Administration',83.00,'0-7821-2734-7',2000);
INSERT INTO titel VALUES (39772,1299,'Microsoft SQL Server 2000 Datenbank-Design und -Implementierung',125.00,'3-86063-921-8',2001);
INSERT INTO titel VALUES (39781,1299,'Microsoft SQL Server 7.0 Datenbank Implementierung',158.00,'3-86063-271-X',1999);
INSERT INTO titel VALUES (41216,1989,'MySQL/PHP-Datenbankanwendungen',59.90,'3-8266-0805-4',2001);
INSERT INTO titel VALUES (42681,23,'Objektdatenbanken - Kompakt',63.00,'3-8273-1757-6',2002);
INSERT INTO titel VALUES (42692,506,'Objektorientierte und objektrelationale Datenbanken',40.00,'3-932588-68-1',2000);
INSERT INTO titel VALUES (42696,506,'Objektrelationale und objektorientierte Datenbankkonzepte und -systeme',72.00,'3-89864-124-4',2002);
INSERT INTO titel VALUES (43105,1244,'Oracle 8 Datenbankentwicklung in 21 Tagen',83.00,'3-8272-2017-3',2000);
INSERT INTO titel VALUES (43110,1244,'Oracle 9i Datenbankentwicklung',77.50,'3-8272-6259-3',2002);
INSERT INTO titel VALUES (44564,430,'PHP 4 & MySQL',73.00,'3-8158-2128-2',NULL);
INSERT INTO titel VALUES (44571,430,'PHP 4 und MySQL',73.00,'3-8158-2203-3',2001);
INSERT INTO titel VALUES (44572,430,'PHP 4 und MySQL',63.00,'3-8158-2043-X',NULL);
INSERT INTO titel VALUES (44583,1244,'PHP und MySQL',32.50,'3-8272-6298-4',2002);
INSERT INTO titel VALUES (44584,1989,'PHP und MySQL f�r Kids',32.90,'3-8266-0828-3',2002);
INSERT INTO titel VALUES (45383,23,'Praxisbuch XHTML',88.00,'3-8273-1893-9',2001);
INSERT INTO titel VALUES (45385,1285,'Praxis-Checkliste. Marketing-Datenbank',27.50,'3-89623-188-X',1999);
INSERT INTO titel VALUES (45596,1989,'Professionelles Programmieren mit PHP 4.1',78.00,'3-8266-0799-6',2002);
INSERT INTO titel VALUES (45632,821,'Programmieren lernen in PHP 4',44.50,'3-446-21754-1',2001);
INSERT INTO titel VALUES (45660,356,'Programming HTML 4 (E) Win Volume 1',68.00,'6001571945',NULL);
INSERT INTO titel VALUES (45661,356,'Programming HTML 4 (E) Win Volume 2',68.00,'6001571929',NULL);
INSERT INTO titel VALUES (45662,356,'Programming HTML 4 (E) Win Volume 3',68.00,'6001571910',NULL);
INSERT INTO titel VALUES (45663,356,'Programming HTML 4 (E) Win Volume 4',68.00,'6001571880',NULL);
INSERT INTO titel VALUES (45664,356,'Programming HTML 4 (E) Win Volume 6',68.00,'6001571864',NULL);
INSERT INTO titel VALUES (46942,1868,'Relationale Datenbanken',35.50,'3-540-41468-1',2001);
INSERT INTO titel VALUES (46943,23,'Relationale Datenbanken',53.00,'3-8273-1381-3',1998);
INSERT INTO titel VALUES (46944,23,'Relationale Datenbanken und SQL',53.00,'3-8273-1558-1',2000);
INSERT INTO titel VALUES (48230,728,'SAP-Datenbankadministration mit MS SQL Server 2000',81.90,'3-934358-87-X',2002);
INSERT INTO titel VALUES (52610,2141,'Super HTML 6.0',65.00,'3-8259-2007-0',2002);
INSERT INTO titel VALUES (52631,1974,'SuperHTML 6.0 (CHIP)(D)',59.90,'6002072810',NULL);
INSERT INTO titel VALUES (56726,1244,'unternehmensdatenbanken mit MS SQL Server und Access',83.00,'3-8272-5968-1',2001);
INSERT INTO titel VALUES (56956,1609,'Using HTML 4. Special Edition',61.00,'0-7897-1851-0',1999);
INSERT INTO titel VALUES (57454,23,'Verteilte Komponenten und Datenbankanbindung',78.00,'3-8273-1552-2',2000);
INSERT INTO titel VALUES (57789,1989,'Visual Basic 6 Datenbankprogrammierung f�r Dummies',43.90,'3-8266-2868-3',2000);
INSERT INTO titel VALUES (57810,23,'Visual Basic Datenbankprogrammierung',78.00,'3-8273-1429-1',1999);
INSERT INTO titel VALUES (57835,728,'Visual Basic.NET und Datenbanken',59.90,'3-89842-142-2',2002);
INSERT INTO titel VALUES (58865,1299,'Web Datenbanken mit ASP.NET',67.90,'3-86063-799-1',2002);
INSERT INTO titel VALUES (58886,23,'Webandwendungen mit PHP 4.0 entwickeln',88.00,'3-8273-1730-4',2001);
INSERT INTO titel VALUES (58893,1989,'Webdatenbanken',27.90,'3-8266-8070-7',2002);
INSERT INTO titel VALUES (58894,1989,'Webdatenbanken f�r Dummies',32.90,'3-8266-3010-6',2002);
INSERT INTO titel VALUES (58895,1244,'Web-Datenbanken f�r Windows-Plattformen entwicklen',83.00,'3-8272-5774-3',NULL);
INSERT INTO titel VALUES (58941,430,'Webprogrammierung mit PHP',73.00,'3-8158-2152-5',2001);
INSERT INTO titel VALUES (58945,1941,'Web-Publishing mit HTML',63.00,'3-8155-0220-9',2001);
INSERT INTO titel VALUES (58954,1941,'Webserver Administration Apache MS Technology',58.75,'3815504317',NULL);
INSERT INTO titel VALUES (61115,1459,'Works 2000 Tabellenkalkulation, Datenbank Schulung',29.80,'3-907573-62-5',2001);

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Create table titel_autoren
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

DROP TABLE IF EXISTS titel_autoren;
CREATE TABLE titel_autoren (
  titel_nr int(11) NOT NULL default '0',
  autor_nr int(11) NOT NULL default '0',
  PRIMARY KEY  (titel_nr,autor_nr)
) ENGINE=InnoDB;

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Insert data into table titel_autoren
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

INSERT INTO titel_autoren VALUES (2,11827);
INSERT INTO titel_autoren VALUES (3,9220);
INSERT INTO titel_autoren VALUES (3,12649);
INSERT INTO titel_autoren VALUES (3,13484);
INSERT INTO titel_autoren VALUES (1637,5207);
INSERT INTO titel_autoren VALUES (1637,7473);
INSERT INTO titel_autoren VALUES (3066,5207);
INSERT INTO titel_autoren VALUES (3066,14541);
INSERT INTO titel_autoren VALUES (4186,23444);
INSERT INTO titel_autoren VALUES (6071,4633);
INSERT INTO titel_autoren VALUES (6625,12341);
INSERT INTO titel_autoren VALUES (7096,1432);
INSERT INTO titel_autoren VALUES (7693,12915);
INSERT INTO titel_autoren VALUES (8079,693);
INSERT INTO titel_autoren VALUES (8080,693);
INSERT INTO titel_autoren VALUES (8300,25620);
INSERT INTO titel_autoren VALUES (8300,25621);
INSERT INTO titel_autoren VALUES (8350,14511);
INSERT INTO titel_autoren VALUES (8352,12504);
INSERT INTO titel_autoren VALUES (9034,9428);
INSERT INTO titel_autoren VALUES (9034,25758);
INSERT INTO titel_autoren VALUES (9553,9218);
INSERT INTO titel_autoren VALUES (11404,4668);
INSERT INTO titel_autoren VALUES (11457,19014);
INSERT INTO titel_autoren VALUES (11626,5684);
INSERT INTO titel_autoren VALUES (11632,5684);
INSERT INTO titel_autoren VALUES (12021,3262);
INSERT INTO titel_autoren VALUES (12022,3267);
INSERT INTO titel_autoren VALUES (12022,21385);
INSERT INTO titel_autoren VALUES (14746,23866);
INSERT INTO titel_autoren VALUES (15036,11574);
INSERT INTO titel_autoren VALUES (15037,11575);
INSERT INTO titel_autoren VALUES (18019,24273);
INSERT INTO titel_autoren VALUES (18020,24274);
INSERT INTO titel_autoren VALUES (18021,24274);
INSERT INTO titel_autoren VALUES (19797,15188);
INSERT INTO titel_autoren VALUES (19798,16943);
INSERT INTO titel_autoren VALUES (19799,26127);
INSERT INTO titel_autoren VALUES (19804,6481);
INSERT INTO titel_autoren VALUES (19805,6934);
INSERT INTO titel_autoren VALUES (19806,13202);
INSERT INTO titel_autoren VALUES (22300,3007);
INSERT INTO titel_autoren VALUES (22300,14511);
INSERT INTO titel_autoren VALUES (24039,25517);
INSERT INTO titel_autoren VALUES (26482,371);
INSERT INTO titel_autoren VALUES (27185,5940);
INSERT INTO titel_autoren VALUES (27185,17049);
INSERT INTO titel_autoren VALUES (29251,2484);
INSERT INTO titel_autoren VALUES (29252,143);
INSERT INTO titel_autoren VALUES (29252,3957);
INSERT INTO titel_autoren VALUES (29261,17068);
INSERT INTO titel_autoren VALUES (29280,12027);
INSERT INTO titel_autoren VALUES (29280,16913);
INSERT INTO titel_autoren VALUES (30706,394);
INSERT INTO titel_autoren VALUES (30706,4352);
INSERT INTO titel_autoren VALUES (30706,25260);
INSERT INTO titel_autoren VALUES (30762,12915);
INSERT INTO titel_autoren VALUES (31811,3040);
INSERT INTO titel_autoren VALUES (31811,18339);
INSERT INTO titel_autoren VALUES (31882,23320);
INSERT INTO titel_autoren VALUES (32087,23772);
INSERT INTO titel_autoren VALUES (32096,7316);
INSERT INTO titel_autoren VALUES (32096,14207);
INSERT INTO titel_autoren VALUES (32104,26682);
INSERT INTO titel_autoren VALUES (36348,742);
INSERT INTO titel_autoren VALUES (36349,742);
INSERT INTO titel_autoren VALUES (41216,3223);
INSERT INTO titel_autoren VALUES (41216,8383);
INSERT INTO titel_autoren VALUES (42681,10000);
INSERT INTO titel_autoren VALUES (42692,15835);
INSERT INTO titel_autoren VALUES (42692,26551);
INSERT INTO titel_autoren VALUES (42696,7647);
INSERT INTO titel_autoren VALUES (43105,14545);
INSERT INTO titel_autoren VALUES (43110,4795);
INSERT INTO titel_autoren VALUES (43110,14545);
INSERT INTO titel_autoren VALUES (44571,14041);
INSERT INTO titel_autoren VALUES (44571,23400);
INSERT INTO titel_autoren VALUES (44572,14039);
INSERT INTO titel_autoren VALUES (44583,22524);
INSERT INTO titel_autoren VALUES (44583,25958);
INSERT INTO titel_autoren VALUES (44584,1855);
INSERT INTO titel_autoren VALUES (45383,12649);
INSERT INTO titel_autoren VALUES (45385,12456);
INSERT INTO titel_autoren VALUES (45596,20590);
INSERT INTO titel_autoren VALUES (45596,23366);
INSERT INTO titel_autoren VALUES (45632,13057);
INSERT INTO titel_autoren VALUES (46942,15835);
INSERT INTO titel_autoren VALUES (46943,20714);
INSERT INTO titel_autoren VALUES (46944,15511);
INSERT INTO titel_autoren VALUES (46944,24671);
INSERT INTO titel_autoren VALUES (48230,19323);
INSERT INTO titel_autoren VALUES (56726,12826);
INSERT INTO titel_autoren VALUES (56726,24199);
INSERT INTO titel_autoren VALUES (56956,10516);
INSERT INTO titel_autoren VALUES (57454,1611);
INSERT INTO titel_autoren VALUES (57454,26854);
INSERT INTO titel_autoren VALUES (57789,15209);
INSERT INTO titel_autoren VALUES (57810,12699);
INSERT INTO titel_autoren VALUES (57835,6416);
INSERT INTO titel_autoren VALUES (58865,3429);
INSERT INTO titel_autoren VALUES (58886,7677);
INSERT INTO titel_autoren VALUES (58886,19246);
INSERT INTO titel_autoren VALUES (58893,22887);
INSERT INTO titel_autoren VALUES (58894,16794);
INSERT INTO titel_autoren VALUES (58894,18464);
INSERT INTO titel_autoren VALUES (58894,26534);
INSERT INTO titel_autoren VALUES (58895,8756);
INSERT INTO titel_autoren VALUES (58941,23399);
INSERT INTO titel_autoren VALUES (58954,1427);
INSERT INTO titel_autoren VALUES (61115,9533);

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Create table verlage
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

DROP TABLE IF EXISTS verlage;
CREATE TABLE verlage (
  verlag_nr int(11) NOT NULL auto_increment,
  verlag varchar(60) NOT NULL default '',
  PRIMARY KEY  (verlag_nr)
) ENGINE=InnoDB;

-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
-- Insert data into table verlage
-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --

INSERT INTO verlage VALUES (22,'Addison-Wesley');
INSERT INTO verlage VALUES (23,'Addison-Wesley Verlag');
INSERT INTO verlage VALUES (224,'BHV B�rohandels- und Verlags mbh');
INSERT INTO verlage VALUES (356,'CBTechnologies');
INSERT INTO verlage VALUES (430,'Data Becker GmbH');
INSERT INTO verlage VALUES (506,'Dpunkt Verlag f�r digitale Tech-');
INSERT INTO verlage VALUES (703,'Franzis-Verlag GmbH');
INSERT INTO verlage VALUES (612,'Edition 8');
INSERT INTO verlage VALUES (728,'Galileo Press GmbH');
INSERT INTO verlage VALUES (821,'Hanser Verlag');
INSERT INTO verlage VALUES (871,'Heyne Verlag');
INSERT INTO verlage VALUES (872,'Heyne Verlag');
INSERT INTO verlage VALUES (1244,'Markt & Technik');
INSERT INTO verlage VALUES (1257,'McGraw Hill - Hill Book Co.');
INSERT INTO verlage VALUES (1285,'Metropolitan Verlag GmbH');
INSERT INTO verlage VALUES (1299,'Microsoft Press (Deutschland)');
INSERT INTO verlage VALUES (1459,'Optobyte AG Computersysteme');
INSERT INTO verlage VALUES (1462,'O\'Reilly');
INSERT INTO verlage VALUES (1511,'Pearson Education (Schweiz)');
INSERT INTO verlage VALUES (1571,'Prentice-Hall International');
INSERT INTO verlage VALUES (1609,'Que Corporation');