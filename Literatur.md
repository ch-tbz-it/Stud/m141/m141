![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

1.  Kofler M.: MySQL. Addison-Wesley. 2003. ISBN 3-8273-2046-1
2.  [http://www.apachefriends.de](http://www.apachefriends.de/)
3.  <http://www.mysql.de>
4.  <http://dev.mysql.com/doc/refman/5.1/de/index.html> das Handbuch in deutsch
5.  <http://www.w3schools.com>
6.  <http://selfhtml.teamone.de>
7.  <http://www.phpmyadmin.net>
8.  <http://www.php.net>
9.  <http://www.apache.org>
10. <http://www.convert-in.com-> ... einfach mal selber weitersuchen.
11. Informationssysteme und Datenbanken; Carl August Zehnder; vdf Hochschulverlag AG Zürich; ISBN 3-7281-3002-8; Preis ca. 47,- CHF
12. Theorie und Praxis relationaler Datenbanken; René Steiner; vieweg-Verlag; ISBN 3-528-25427-0; Preis der neueren Ausgabe ca. 35,- CHF
13. Herdt-Verlag; Datenbanken aus der Reihe ECDL Modul 5; Preis ca. 22,- CHF
14. Objektorientierte Datenbanken; John G. Hughes; Hanser Verlag; ISBN 3-446-16583-5
15. Der Software-Entwicklungsprozess; Andreas Frick; Hanser Verlag; ISBN 3-446-17777-9
16. Datenmodellierung und Datenbankentwurf; Josef L. Staud; Springer Verlag 2005; ISBN 3-540-20577-2; Preis 76,50 CHF
17. Database System Concepts; Silberschatz, Korth, Sudarshan; Verlag McGraw-Hill; ISBN 0-07-295886-3; Preis 109,- CHF für die „fifth edition“ – seit 01/10 gibt es die 6th edition
