![](../x_res/tbz_logo.png)

# M141 Lösungen 1.Tag Einführung, DB-Modelle, XAMPP


1.  Welches ist die heute am häufigsten verwendete Datenbank-Art?

    - [ ] Hierarchische Datenbank

    - [**X**] Relationale Datenbank

    - [ ] Objektorientierte Datenbank

    - [ ] Netzwerkförmige Datenbank

2.  Welche Komponenten sind in einem DB-Server enthalten?

    - [**X**] 1 oder mehrere Datenbanken

    - [ ] 1 oder mehrere Datenbank-Anwendungen

    - [**X**] Datenbank-Management-System (DBMS)

    - [ ] Formulare, Reports und Abfragen

3.  Bei welchen der folgenden Fabrikate handelt es sich um eine relationale Datenbank?

    - [**X**] Oracle

    - [ ] Couch-DB

    - [**X**] MySQL

    - [**X**] MariaDB

    - [ ] Mongo-DB

    - [**X**] MS Access

4.  Welches sind Beispiele für Aufgaben eines DB-Clients?

    - [ ] speichert die eigentlichen Daten

    - [**X**] stellt dem Benutzer ein User-Interface für den Datenzugriff zur Verfügung

    - [ ] verwaltet Benutzer und Passworte und gewährleistet damit die Sicherheit der Datenbank

    - [**X**] leitet die Befehle des Benutzers an den DB-Server weiter

5.  Welches sind Client-Komponenten von MySQL?

    - [ ] mysqld

    - [ ] my.ini

    - [**X**] mysql

    - [**X**] phpMyAdmin
    
1.  Wie heisst die Server-Komponente von MySQL?

    - [ ] phpMyAdmin

    - [ ] WinMySQLAdmin

    - [ ] mysql

    - [**X**] mysqld

2.  Beschreiben Sie den Begriff Client/Server-Modell.

    *Client: Nimmt Formulare / Anfragen von User entgegen, AS.: Verteilt Anfragen, stellt 2 Tier Modell mit Client der über eine Verbindung mit Server (= DBMS) kommuniziert*

3.  Welche Vorteile hat die Client/Server-Architektur gegenüber einer Desktop-DB?

    *Entfernter Zugriff über Netz möglich   
    Multiuserfähig*

4.  **Wie werden die Daten in einer relationalen Datenbank abgespeichert?**

    *Nicht redundant, ref. Integrität*   
      
    *In Tabellen und Beziehungen, mindestens in 3.Normalform (siehe Modul 164)*

5.  Was sind die Vorteile, wenn ein DB-Server die referentielle Datenintegrität unterstützt?

    *Verhindert inkonsistente Datensätze (Widerspruchsfreiheit der Daten & Beziehungen)*

6.  Welches sind die 4 Gruppen von NoSQL-Datenbanken, die zurzeit relevant sind?

    *Dokumentenorientierte Datenbanken (MongoDB, CouchDB; Cassandra, Redis …)*   
      
    *Grafdatenbanken (z. B. Neo4j)*   
      
    *Big-Table-Datenbanken (z. B. Doug Judd von Hypertable Inc., Big Table von Google)*   
      
    *Key-Value-Datenbanken*
    
12.  Was bedeutet **DBaaS**? Erklären Sie anhand eines Beispiels. °

    *DBaaS (auch bekannt als verwalteter Datenbankservice) ist ein  Cloud-Computing-Service, der es den Benutzern ermöglicht, auf ein Cloud-Datenbanksystem zuzugreifen und dieses zu nutzen, ohne eigene Hardware erwerben und einrichten, eigene Datenbanksoftware installieren oder die Datenbank selbst verwalten zu müssen.*  
    
13.  Was sind die Vorteile eines RDBMS gegenüber anderen DB-Modellen? 

    *Strukturiertheit (Normalform), flexibel, skalierbar, Datenintegrität bei Veränderungen, Datenintegrität und -konsistenz*    
    
14.  DB-Server starten und stoppen

    *KnowHow*
	
15. DB-Server prüfen

    *KnowHow*


