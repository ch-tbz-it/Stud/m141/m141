![](../x_res/tbz_logo.png)

# M141 Lösungen 2.Tag

## DB-Server und XAMPP

Bei den folgenden Fragen treffen eine oder mehrere Antworten zu.

1.  Wie kann der MySQL-Server gestartet werden?

    - [ ] Start von mysql.exe im CMD-Fenster

    - [ **X** ] Start von mysqld.exe im CMD-Fenster

    - [ **(X)** ] über MySQL-Workbench, **falls als Dienst installiert**

    - [ ] Eingabe von localhost als URL im Browser

    - [ **X** ] NET START mysql (im DOS-Fenster)

    - [ **(X)** ] mit dem Dienstmanager von Windows, **falls als Dienst installiert**

2.  Welche Informationen erhalten Sie, wenn Sie im Konsolenfenster den Befehl *status* eingeben?

    - [ **X** ] Version des Konsolenprogramms

    - [ **X** ] Betriebszeit des Servers

    - [ **X** ] Version des Servers

    - [ ] Betriebszeit des Monitors

3.  Welche Daten befinden sich im Verzeichnis datadir (z. B. C:…\\mysql\\data)?

    - [ **X** ] Protokoll-Dateien (Log-Files)

    - [ **X** ] Fehlerprotokolle

    - [ ] die ausführbaren MySQL-Programme, z.B. mysql.exe

    - [ **X** ] Datenbanken

4.  Wie prüfen Sie, ob der MySQL-Server läuft?

    - [ **X** ] mit dem Dienst-Manager von Windows, **falls als Dienst installiert**

    - [ **X** ] mit dem GUI-Tool Administrator

    - [ ] durch Eingabe des Befehls *status* im DOS-Fenster

    - [ ] mit dem Task-Manager von Windows (Prozess)

1.  Wie testen Sie die Installation des DB-Servers?

    **Statusabfrage des Servers   
    phpMyAdmin Status; Workbench Adminbereich**

1.  Wie überprüfen Sie die Laufzeit des DB-Servers?

    **Status; Parameter: UpTime; Info im phpMyAdmin; MySQL-Workbench**

1.  Wozu verwenden Sie das Programm mysql.exe? Wie starten Sie es?

    **Dient als Klient zur Datenbankabfrage      
    Im CMD / WPS Fenster**

2.  Notieren Sie 3 Informationen des status-Befehls mit ihrer Bedeutung.

    **Uptime / Version / akt. DB-User / Verbindung**

3.  Nennen Sie 2 wichtige Verzeichnisse der MySQL-Installation mit ihrem Inhalt.

    **\data: beinhaltet MySQL Datenbanken inkl. User- und Rechtmanagement   
    \bin: beinhaltet ausführbare Tools und DB-Programme**

4.  Was ist der Inhalt der my.ini – Datei?

    **my.ini: Konfiguratonseinstellungen für MySQL-Server 
    Unterschied my.ini/my.cnf (WIN/ LINUX-MAC)**
    


## Codierung und Kollation

1.  Welche Aussagen treffen zum Thema Codierung zu?

    - [ ] Ein Datenbankserver erkennt die Codierung einer Datei automatisch

    - [ **X** ] Codierung ist eine Vereinbarung zwischen dem Nutzer und dem System.

    - [ **X** ] Die Codierung legt fest, welche binäre Bitkombination zu welchem Zeichen gehört.

    - [ ] ANSI- und ASCII-Codierung ist dasselbe **nur die ersten ca. 96 Zeichen sind gleich**

    - [ **X** ] Der Unicode-Zeichensatz hat 32Bit Codelänge

    - [ **X** ] UTF bedeutet Unicode Transformation Format

    - [ ] UTF-8 hat nur 8Bit lange Zeichen aus dem Unicode-Zeichensatz, **16 / 32 Bit auch möglich**

2.  Welche Aussagen treffen zum Thema Byte Order Mark zu?

    - [ ] Ein BOM kann in Dateien jeglicher Art gesetzt werden.

    - [ **X** ] Wenn das UTF-8-BOM "ï»¿" bei einem Text-Editor sichtbar ist, erkennt er *es* nicht!

    - [ **(X)** ] Bei UTF-8 nutzt ein BOM nichts, da es nur 8 Bits zur Codierung verwendet! **Nutzt nichts, da Reihenfolge nicht verändert wird!**

    - [ **X** ] UTF-8 und UTF-16 verwenden unterschiedliche BOMs!

3.  Welche Aussagen treffen zum Thema Kollation zu?

    - [ ] 'utf8_general_cs' ist die Standard-Einstellung bei MySQL.

    - [ **X** ] In der DIN-Normierung zur deutschen Kollation werden zwei Varianten zur Umlauthandhabung angeboten.

    - [ ] Die Endung "_ci" gibt an, dass die Sortierung die Gross-/Kleinschreibweise unterscheidet.

    - [ **X** ] Seit MySQL 5.5.3 sollte [utf8mb4](https://dev.mysql.com/doc/refman/5.5/en/charset-unicode-utf8mb4.html) anstelle von utf8 verwendet werden. **Ist Standard**

    - [ **X** ] In der Konfig-Datei (my.ini) kann die UFT8-Codierung als Standard angegeben werden.

    - [ ] Eine Kollationseinstellung gilt für die ganze Tabelle (Entität).

## Daten importieren

1.  Mit welchem Befehl kontrollieren Sie die Struktur einer Tabelle?

    - [ ] SHOW DATABASES;

    - [ **X** ] SHOW CREATE TABLE *tabellenname*;

    - [ **X** ] DESC *tabellenname*; **Ist Abkürzung, nicht in Scripts verwenden!**

    - [ **X** ] DESCRIBE *tabellenname*;

    - [ ] SELECT * FROM *tabellenname*;

    - [ ] SHOW TABLE *tabellenname*;
    



