![](../x_res/tbz_logo.png)

# M141 Lösungen 3.Tag

## Tabellentypen und Transaktionen

Siehe auch `Demo_Transaktionen.sql`! (--> LP)

1.  Wie bezeichnet man die Ausführung mehrerer DB-Operationen in einem einzigen Schritt?

    - [ ] Referentielle Integrität

    - [ ] Replikation

    - [ **X** ] Transaktion

    - [ ] Storage Procedure

2.  Warum sollen Locks möglichst schnell freigegeben werden?

    - [ ] damit das DBMS nicht zu stark belastet wird

    - [ **X** ] damit andere DB-Anwender nicht lange warten müssen

    - [ ] damit niemand die Daten ändern kann

    - [ ] damit möglichst viele Benutzer gleichzeitig auf die DB zugreifen können

3.  Welches ist das Standard-Tabellenformat von MySQL (MariaDB)?

    - [ **X** ] InnoDB

    - [ ] MyISAM

    - [ ] ARIA

    - [ ] ISAM

4.  Wann verwenden Sie das InnoDB-Tabellenformat?

    - [ ] wenn möglichst schnell auf die Daten zugegriffen werden muss

    - [ ] wenn auf gar keinen Fall ein Datenverlust vorkommen darf

    - [ **X** ] wenn viele Benutzer gleichzeitig Daten ändern

    - [ ] wenn bei sehr vielen Daten nicht beliebig viel Speicherplatz vorhanden ist

5.  Was trifft auf den sog. Tablespace zu?

    - [ ] Datei, welche die Daten der entsprechenden Tabelle enthält (\*.MYD)

    - [ ] Datei, welche Beschreibung, Daten und Indexe einer Tabelle enthält

    - [ **X** ] Datei, welche alle InnoDB-Tabellen enthält (**virtueller Speicher**)

    - [ **X** ] wird nach Erreichen von 10/12 MB automatisch vergrössert (falls autoextend eingeschaltet)
    
6.  Mit welchen Befehlen werden Transaktionen gesteuert?

    - [ ] UNLOCK TABLES;

    - [ **X** ] COMMIT; oder ROLLBACK;

    - [ ] ALTER TABLE ... TYPE= ...;

    - [ **X** ] BEGIN; oder START TRANSACTION;

7.  Was trifft auf das Locking bei Transaktionen auf InnoDB-Tabellen zu?

    - [ ] in Transaktionen kommt Table locking zur Anwendung

    - [ **X** ] es wird Row locking angewendet

    - [ ] es werden alle Datensätze der entsprechenden Tabelle(n) gesperrt

    - [ **X** ] es werden nur die gerade bearbeiteten Datensätze gesperrt

8.  Welches sind Vorteile der InnoDB-Tabellen gegenüber MyISAM-Tabellen?

    **InnoDB: unterstützt Transaktionen, ref. Integrität., Row Locking Sicherer**

9.  In welchen Dateien wird die MyISAM-Tabelle KUNDEN gespeichert?

    **In 3 Tabellen aufgeteilte DB: \*.FRM, \*.MYD und \*.MYI**

10.  Notieren Sie den SQL-Befehl, der die InnoDB-Tabelle BESTELLUNGEN erstellt.

     **CREATE TABLE bestellungen ( ... ) ENGINE=InnoDB;**
     
11.  Welche Locking-Art ist a) bei MyISAM-Tabellen b) bei InnoDB-Tabellen möglich?

     **(a) MyISAM: Table Locking und (b) InnoDB: RowLevel Locking.**

12.  Beschreiben Sie den Begriff Datenbank-Transaktion!

     **Eine gruppierte SQL-Kommandofolge die entweder als Ganzes oder gar nicht ausgeführt werden. Die Transaktion sperrt die angewählten DS und arbeitet auf einer Kopie der DB-Table.**

13.  Beschreiben Sie die Bedeutung von I in der Abkürzung ACID.

     **Sind 4 notwendige Eigenschaften zur Ausführung von Transaktionen: I steht für Isoliertheit.**

14.  Wie stellen Transaktionen bei einem DB-Server-Crash die Datenkonsistenz sicher? (Schwierig)

     **Sie werden als „nicht ausgeführt“ erkannt und werden als Ganzes neu ausgeführt. In der Transaktionslog sind Details zum Status festgehalten. (Siehe auch Tag 6 Data Recovering.**

15. Mit welcher Locking-Art wartet ein SELECT-Befehl, bis alle Transaktionen auf die angeforderte Tabelle entsperrt sind? 

    **SELECT ... LOCK IN SHARE MODE; <br> Die von `SELECT` gefundenen Datensätze werden mit einem so genannten `Shared Lock` gesperrt. Ein `Shared Lock` ist nicht ganz so stark wie ein `Exclusive Lock`. Die Daten können von anderen Anwendern ebenfalls nicht verändert werden, lassen sich aber mit `SELECT ... LOCK IN SHARE MODE` lesen.** 
    
16. Wie muss Autocommit gesetzt werden, damit jeder SQL-Befehl zu einer Transaktion gehört und damit explizit mit COMMIT abgeschlossen werden muss, damit er ausgeführt wird?    
    
    **`SET AUTOCOMMIT=0;` stellt den Autocommit-Modus aus.**
****