![](../x_res/tbz_logo.png)

# M141 Lösungen 4.Tag

## Datenbank-Sicherheit

1.  Was bedeutet der Begriff Authentifizierung im Zusammenhang mit einem DB-Server?

    - [ ] Prüfung der Privilegien des Benutzers

    - [ **X** ] Antwort auf die Frage Wer?

    - [ **X** ] Identitätsprüfung

    - [ ] Antwort auf die Frage Was?

2.  Wann werden Änderungen im Zugriffssystem von MySQL wirksam?

    - [ ] sofort nach Eingabe der Änderung

    - [ **X** ] nach dem Befehl FLUSH PRIVILEGES

    - [ **X** ] nach dem Neustart des DB-Servers

    - [ ] nach dem Befehl GRANT

3.  Was bewirkt der SQL-Befehl GRANT ... ON ... TO ...;

    - [ **X** ] Privileg(ien) erteilen

    - [ ] Privileg(ien) wegnehmen

    - [ **X** ] User erstellen, falls noch nicht vorhanden

    - [ ] User löschen

4.  Mit welchem Befehl werden Privilegien kontrolliert?

    - [ ] REVOKE ... ON ... FROM ;

    - [ ] SELECT user, host, password FROM user ;

    - [ ] SHOW TABLES;

    - [ **X** ] SHOW GRANTS FOR ... ;

5.  Welches sind die beiden wichtigsten DCL-Befehle (data control)?

    - [ ] SELECT

    - [ **X** ] REVOKE

    - [ ] DELETE

    - [ **X** ] GRANT
    
1.  Was ist nötig, dass Benutzer "meier" keinen Zugang mehr auf den DB-Server hat.

    - [ ] in Systemtabelle user für diesen Benutzer jedes Privileg auf "N" setzen

    - [ **X** ] mit DELETE FROM user WHERE user = 'meier'; und FLUSH PRIVILEGES;

    - [ ] in allen Systemtabellen für diesen Benutzer jedes Privileg auf "N" setzen

    - [ ] dem Benutzer das GRANT-Privileg (Grant_priv) wegnehmen

1.  Erklären Sie den Begriff "Autorisierung" im Zusammenhang mit einem DB-Server.

    **Bestimmt WAS auf einer Tabelle ausgeführt werden darf. Prüft die Berechtigung des Klienten, welche SQL-Befehle Zugriff erhalten...**   
      

2.  Wann wird das Schlüsselwort IDENTIFIED BY verwendet?

    **Um bei einem GRANT-Statement ein Passwort mit anzugeben (wird somit gesetzt)**

3.  Ergänzen Sie den Befehl REVOKE ... ON ... FROM ... ; mit eigenen Angaben.

    **REVOKE SELECT, INSERT ON db.tabelle FROM user@host <br> REVOKE ALL ON db.tabelle FROM \*@%**  
      
4.  Beschreiben Sie den Begriff der MySQL-Testdatenbank.

    **Jeder Benutzer, der sich beim MySQL-Server anmeldet, kann Testdatenbanken <br>cerstellen. Voraussetzung ist, dass der DB-Name mit den Buchstaben test beginnt. <br>
Diese Datenbanken können von allen gelesen, gelöscht und auch verändert werden; sie sind also völlig ungeschützt.**   

5.  Mit welchem Befehl ändern Sie das Passwort von Benutzer Meier auf "abc123"?

    **SET PASSWORD FOR ’meier’ = Password(’abc123’) <br> FLUSH PRIVILEGES**   

6.  Geben Sie eine Erklärung für folgende Fehlermeldung.  
    
    ```  
    GRANT USAGE ON \*.\* TO abc IDENTIFIED BY 'a12';  
    ERROR 1045: Access denied for user: '@127.0.0.1'
    ```
    
    **User ‚abc’ hat keine Zugriffsberechtigung vom eingeloggten Host aus**   
      

7.  Korrigieren Sie den folgenden Befehl:  
    
    ```  
    REVOKE ALL FROM ''@localhost;  
    ERROR 1064: You have an error
    ```
    
    **REVOKE ALL PRIVILEGES FROM ’’@localhost; // ’’ = anonymous !!!**


