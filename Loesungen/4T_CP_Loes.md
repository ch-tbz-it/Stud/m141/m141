![](../x_res/tbz_logo.png)

# M141 Lösungen 4.Tag

## DB-Server im LAN

1.  Welcher Befehl testet die Verbindung zum Server-Rechner mit Adresse 139.79.124.97?

    - [ ] mysql -h 139.79.124.97

    - [ ] ipconfig

    - [ **X** ] ping 139.79.124.97

    - [ ] mysqladmin -h 139.79.124.97 -u root -p ping

2.  Wozu wird der Parameter -h bei MySQL verwendet?

    - [ ] bewirkt die Abfrage des Passworts

    - [ ] bewirkt die Verbindung als bestimmter Benutzer

    - [ ] Angabe der Adresse des Client-Rechners

    - [ **X** ] Angabe der Adresse des Server-Rechners

3.  Was bewirkt der Befehl 'mysqldump -h 139.79.124.97 hotel \> datei.txt'?

    - [ **X** ] Backup der DB hotel in die Datei datei.txt auf Adresse 139.79.124.97

    - [ **X** ] Backup der angegebenen DB auf dem Server mit der IP-Adresse 139.79.124.97

    - [ ] Restore der Datenbank hotel auf dem Server mit der Adresse 139.79.124.97

    - [ ] Ausführen des SQL-Skripts datei.txt auf Adresse 139.79.124.97 auf die DB hotel

4.  Welche Aufgabe hat der ODBC-Driver?

    - [ ] passt die SQL-Befehle dem entsprechenden DB-Server an

    - [ **X** ] ermöglicht das Erstellen und Konfigurieren von ODBC-Datenquellen (DSN)

    - [ **X** ] ermöglicht den einheitlichen Zugriff einer Applikation auf verschiedene Datenbanken

    - [ ] ermöglicht den Zugriff einer Applikation auf eine bestimmte DB

5.  Wie greifen Sie vom Konsolenfenster auf einen DB-Server mit Adresse 139.79.124.97 zu?

    - [ ] mysqladmin -h 139.79.124.97

    - [ ] mysql -h 139.79.124.97 hotel \< hotel.bkp

    - [ **X** ] mysql -h 139.79.124.97 -u root -p

    - [ ] ping 139.79.124.97

1.  Welche Aufgaben hat der DB-Server im Gegensatz zum DB-Client?

    **Stellt Daten(-sätze) bereit und regelt deren Zugriffe   
    Bildet Datenstruktur mittels Tabellen und Beziehungen ab.**

2.  Weshalb benutzt man MS Access z.B. zusammen mit einem MySQL-Server?

    **MS Access hat eine ausgereifte Benutzeroberfläche mit Masken/Formulare   
    MySQL ist eine effizienter Multi-User-Server.**

3.  Wie bestimmen Sie die IP-Adresse des Server-Rechners?

    **Allg.: URL und Port kennen <br>
    Im Sub-Netz: Portscan `nmap -p 3306 172.16.17.0/24` <br> 
    Localhost: `ipconfig` oder `hostname` / Port 3306**

4.  Wie prüfen Sie, ob der DB-Server auf Adresse 139.79.124.97 läuft?

    **mysqladmin -h 139.79.124.97 ping –u Benutzername -p**

5.  Welcher Befehl führt das SQL-Skript xy.sql auf die DB hotel auf Adresse 139.79.124.97 aus?

    **mysql -h 139.79.124.97 -u remote -p hotel \< H:\\xy.sql**

      
