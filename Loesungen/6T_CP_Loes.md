![](../x_res/tbz_logo.png)

# M141 Lösungen 6.Tag

## Server konfigurieren 

1.  Auf welche Arten können Konfigurationsparameter definiert werden?

    - [ ] mit einem INSERT-Befehl

    - [ ] durch Eintrag auf der Kommandozeile

    - [ **X** ] durch Eintrag in einer Konfigurationsdatei

    - [ ] durch Eintrag in einem Logfile

2.  Welcher Konfigurationsparameter legt fest, wo die Log-Dateien abgelegt werden?

    - [ **X** ] basedir

    - [ **X** ] datadir

    - [ **X** ] log-bin

    - [ ] logdir

3.  Mit welchem Eintrag beginnen die Server-Parameter in der Konfigurationsdatei?

    - [ ] [mysql]

    - [ ] [WinMySQLadmin]

    - [ ] [mysqldump]

    - [ **X** ] [mysqld]

4.  Wozu kann der DB-Client mysqlshow verwendet werden?

    - [ ] Backup erstellen

    - [ **X** ] DB-Schema anzeigen

    - [ ] Verbindung zum DB-Server testen

    - [ ] Inhalt einer Protokolldatei anschauen

5.  Mit welchem Log-File bestimmen Sie den letzten Start des MySQL-Servers?

    - [ **X** ] Error Log

    - [ ] Update Log

    - [ ] Query Log

	 - [ ] Transaction Log
	 
6.  Welcher Eintrag im Konfigurationsfile schaltet die Protokollierung aller User-Login ein?

    - [ ] log-bin

    - [ ] log-slow-queries

    - [ **X** ] log °?

    - [ ] log-error=C:/log/err.log

7.  Wie restaurieren Sie nach einem Server-Ausfall eine DB vollständig?

    - [ **X** ] Einlesen des letzten Backup

    - [ ] Verwenden der Option --opt beim Erstellen des Backup

    - [ ] Einlesen des Query-Log

    - [ **X** ] Einlesen aller Update-Logs in der richtigen Reihenfolge (mit Hilfe von mysqlbinlog)

8.  Wie erreichen Sie, dass Änderungen in der Konfigurationsdatei wirksam werden?

    **Konfig-Datei abspeichern und Server mysqld.exe neu starten**

9.  Durch welche Daten wird der von einer DB benötigte Speicherplatz bestimmt?

    **Nutzdaten (Datensätze), Indizes, Systemdaten**

10.  Wozu wird das Logging (Protokollierung) verwendet?

    **Monitoring (Fehler), Sicherheit(Backup), Optimierung(von Abragen),   
    Replikation(Synchronisierung), Transaktionen(nach DB-Absturz)**

11.  In welcher Log-Datei finden Sie, den Anwender, der bestimmte Daten löschte?

    **Query-Log: Zeichnet jeden User und dessen Aktivitäten (Queries )auf**

12.  Welche Informationen finden Sie im Slow Query Log?

    **Zeichnet DB-Abfragen auf die den grössten Aufwand verursachen (\>10s)  
    Hinweise zur Verbesserung der Server-Performance**

13.  Geben Sie für jede Protokolldatei an, wie Sie deren Inhalt kontrollieren.

    **Error.log, Query.log, SlowQuery.log Texteditor   
    Update.log mysqlbinlog.exe; Transaktions-logs nur für den Server**

14.  Wie beeinflusst der Parameter --opt beim Erstellen eines Backup das Tabellenlocking?

    **Beim Erstellen der Insert-Befehle im Backup-Script werden diese mit Lock-Befehlen „eingepackt“ und die Tabellen werden fürs Lesen gesperrt (--lock-tables)**

15. Beschreiben Sie das Vorgehen, um Daten von MySQL nach ORACLE zu migrieren.

    **Tabellen aus MySQL exportieren mit SELECT \* INTO OUTFILE und in ORACLE   
    importieren; Auf Delimiter, Datumsformate und Kollation (Zeichenformate) achten**

16. Beschreiben Sie eine praktische Anwendung für den READ-Lock.

    **Gewährleistet die Integrität der Datenbank beim Erstellen eines Backups**
      

# Optimierung

1.  Welche Möglichkeiten können die Geschwindigkeit eines DB-Server verbessern?

    - [ ] Indexe möglichst vermeiden

    - [ **X** ] Serverparameter einstellen

    - [ ] Transaktionen verwenden

    - [ **X** ] Locks verwenden

2.  Wie werden Daten schneller in eine DB-Tabelle geladen?

    - [ ] durch Komprimieren der Daten vor der Übertragung

    - [ **X** ] durch Verwenden des Parameters --opt beim Erstellen des Backup-Skripts

    - [ **X** ] durch Importieren der Daten aus einer Textdatei

    - [ ] durch Verwenden von vielen INSERT-Befehlen

3.  Was trifft auf den Befehl OPTIMIZE TABLE zu?

    - [ **X** ] entfernt nicht genutzten Speicherplatz aus MyISAM-Tabellendateien

    - [ ] ist auf MyISAM- und InnoDB-Tabellen anwendbar

    - [ **X** ] wird angewendet bei Tabellen, die häufig abgefragt werden

    - [ **X** ] defragmentiert DB-Dateien

4.  Wie finden Sie langsame DB-Abfragen?

    - [ **X** ] mit EXPLAIN SELECT

    - [ ] im Query Log

    - [ **X** ] im Slow Query Log

    - [ ] im Error Log

5.  Welche Aussagen betreffend DB-Optimierung sind korrekt?

    - [ ] Abfragen, die LIKE enthalten, können immer optimiert werden

    - [ **X** ] Indexe beschleunigen Abfragen

    - [ **X** ] Indexe werden allgemein auf Schlüsselattribute gelegt

    - [ ] durch Indexe werden DB-Einträge und -änderungen schneller
    
6.  Wann verwenden Sie den Befehl EXPLAIN?

    - [ ] um Daten schneller in die DB zu laden

    - [ ] immer im Zusammenhang mit SELECT

    - [ **X** ] um langsame Abfragen zu finden

    - [ **X** ] um zu erkennen, wie sich ein Index auf die Geschwindigkeit einer Abfrage auswirkt

7.  Welches sind Gründe für die Verwendung eines Index?

    - [ **X** ] um das Eintragen von Daten in Tabellen bei Unique-Attributen zu beschleunigen

    - [ **X** ] um DB-Abfragen zu beschleunigen

    - [ ] um das Ändern von Daten zu verlangsamen

    - [ **X** ] um einmalige Werte zu gewährleisten

8.  Nennen Sie Ziele der DB-Optimierung?

    **Performance verbessern, Speicherplatz einsparen   
    Portabilität ermöglichen**

9.  Was wird optimiert, um die Geschwindigkeit eines DB-Servers zu verbessern?

    **Such-, Schreib- und Lesevorgänge: Daten auf mehrere Festplatten verteilen <br> (Optimierung von SQL-Scripts und Speicherplatz)**

10.  Mit welchen 2 prinzipiellen Massnahmen werden DB-Abfragen beschleunigt?

    **Parameter --opt, LOAD DATA INFILE: schnelleres Ausführen von SQL-Scripts <br>  
OPTIMIZE TABLE: entfernt nicht genutzen Tabellenspeicherplatz, Indizes**

11.  Beschreiben Sie kurz, wie Sie den Befehl EXPLAIN verwenden.

    **Der Präfix vor SELECT erklärt wie dieser Befehl ausgeführt werden würde   
     Gibt Auskunft wie die Tabellen verknüpft werden zeigt wo Indizes eingefügt werden sollten**

12.  Wozu wird der Befehl OPTIMIZE TABLE angewendet?

    **Entfernt nicht genutzen Tabellenspeicherplatz, der durch Mutationen entstand**

13.  Wie werden SELECT-Befehle optimiert?

    **Indem Indexe auf häufig verwendete Attribute und auf Schlüssel erstellt werden**

14.  Wie viele DB-Tabellen können standardmässig gleichzeitig geöffnet sein?

    **table\_cache = 64**

15.  Wie schalten Sie den Query Cache ein bzw. aus?

    **„query\_cache\_size = XXM“ in die Konfigutrationsdatei einfügen   
    query\_cache\_type = 0 oder 1;**
    
16. Hier sind die Beschreibungen der MariaDB-CLI-Tools:

	1. **mysql.exe**: Dies ist der MariaDB-Befehlszeilenclient. Er ermöglicht es Ihnen, SQL-Anfragen interaktiv oder über ein Skript auszuführen¹².
	
	2. **mysqladmin.exe**: Dieses Tool ist ein Administrationsprogramm für den mysqld-Daemon. Es kann verwendet werden, um zu überwachen, was die MariaDB-Clients tun (Prozessliste), Nutzungsstatistiken und Variablen vom MariaDB-Server zu erhalten, Datenbanken zu erstellen/löschen, Protokolle (Logs), Statistiken und Tabellen zurückzusetzen (Flush), laufende Abfragen zu beenden und den Server zu stoppen (Shutdown)⁵⁶⁷.
	
	3. **mysqlbinlog.exe**: Dieses Tool wird verwendet, um die Ereignisse im Binärprotokoll des MariaDB-Servers in Klartext anzuzeigen¹⁶¹⁷¹⁸.
	
	4. **mysqlcheck.exe**: Dieses Tool ermöglicht es Ihnen, Tabellen auf Fehler zu überprüfen, zu reparieren, zu analysieren und zu optimieren²⁹[^30^]³¹.
	
	5. **mysqld.exe**: Dies ist der MariaDB-Server. Es handelt sich um einen einzelnen mehrbenutzerfähigen Daemon, der SQL-Anfragen entgegennimmt und diese ausführt⁸⁹[^10^]¹¹¹².
	
	6. **mysqldump.exe**: Dieses Tool ist ein Backup-Programm, das zum Dumpen einer Datenbank oder einer Sammlung von Datenbanken für Backup oder Transfer zu einem anderen Datenbankserver verwendet wird¹⁹[^20^]²¹.
	
	7. **mysqlimport.exe**: Dieses Tool lädt Tabellen aus Textdateien in verschiedenen Formaten²²²³²⁴.
	
	8. **mysqlshow.exe**: Dieses Tool kann verwendet werden, um schnell zu sehen, welche Datenbanken existieren, ihre Tabellen oder die Spalten oder Indizes einer Tabelle¹³¹⁴¹⁵.
	
	9. **mysqlslap.exe**: mysqlslap ist ein Diagnoseprogramm, das entwickelt wurde, um die Clientlast für einen MariaDB-Server zu emulieren und die Zeitmessung jeder Phase zu melden. Es funktioniert so, als ob mehrere Clients auf den Server zugreifen³²³³³⁴³⁵.
	
	10. **mysql\_install\_db.exe**: Dieses Tool initialisiert das MariaDB-Datenverzeichnis und erstellt die Systemtabellen, die es enthält. Es initialisiert den Systemtablespace und die zugehörigen Datenstrukturen, die zur Verwaltung von InnoDB-Tabellen benötigt werden²⁵²⁶²⁷²⁸.
	
	
	11. **mysql\_plugin.exe**: Dieses Tool wird verwendet, um Plugins zu aktivieren oder zu deaktivieren²³.



	1. **mariabackup.exe**: Mariabackup ist ein Open-Source-Tool, das von MariaDB bereitgestellt wird, um physische Online-Backups von InnoDB, Aria und MyISAM-Tabellen durchzuführen⁴⁷. Für InnoDB sind "heiße Online"-Backups möglich⁴⁷. Es wurde ursprünglich von Percona XtraBackup 2.3.8 abgezweigt⁴⁷. Es ist auf Linux und Windows verfügbar⁴⁷. Dieses Tool bietet eine produktionsreife, nahezu nicht blockierende Methode zum Erstellen vollständiger Backups auf laufenden Systemen⁴⁷. Weitere Informationen zu den Optionen und der Verwendung von mariabackup.exe finden Sie in der MariaDB-Dokumentation⁴⁸.
	
	
	
	Quelle: Unterhaltung mit Copilot, 6.6.2024
	(1) MySQL unter Windows – Teil 1: Installation & Konfiguration. https://blog.ordix.de/mysql-unter-windows-teil-1-installation-konfiguration.
	(2) How do you use MySQL's source command to import large files in windows. https://stackoverflow.com/questions/6163694/how-do-you-use-mysqls-source-command-to-import-large-files-in-windows.
	(3) 6.5.2 mysqladmin — A MySQL Server Administration Program. https://dev.mysql.com/doc/refman/8.0/en/mysqladmin.html.
	(4) 8.7. mysqladmin — Client für die Verwaltung eines MySQL Servers. http://download.nust.na/pub6/mysql/doc/refman/5.1/de/mysqladmin.html.
	(5) mysqladmin: connect to server at 'localhost' failed. https://stackoverflow.com/questions/30004588/mysqladmin-connect-to-server-at-localhost-failed.
	(6) 4.6.7 mysqlbinlog — Utility for Processing Binary Log Files. https://dev.mysql.com/doc/refman/5.7/en/mysqlbinlog.html.
	(7) mysqlbinlog - How to read a mysql binlog - Stack Overflow. https://stackoverflow.com/questions/30609025/how-to-read-a-mysql-binlog.
	(8) MySQL Binary Logs - MySQL Tutorial. https://www.mysqltutorial.org/mysql-administration/mysql-binary-logs/.
	(9) 6.5.3 mysqlcheck — A Table Maintenance Program. https://dev.mysql.com/doc/refman/8.0/en/mysqlcheck.html.
	(10) A Guide to mysqlcheck for Optimal Performance - MySQL Tutorial. https://www.mysqltutorial.org/mysql-administration/mysqlcheck/.
	(11) How to Check and Repair MySQL database Using Mysqlcheck. https://dba.stackexchange.com/questions/267788/how-to-check-and-repair-mysql-database-using-mysqlcheck.
	(12) mysqld performance high cpu and memory usage - Stack Overflow. https://stackoverflow.com/questions/45720283/mysqld-performance-high-cpu-and-memory-usage.
	(13) mysqld.exe Windows Prozess - Was ist das? - file.net. https://www.file.net/prozess/mysqld.exe.html.
	(14) Mysqld.exe: Anleitung (kostenloser Download) - EXE Files. https://www.exefiles.com/de/exe/mysqld-exe/.
	(15) Mysqld.exe memory usage - Database Administrators Stack Exchange. https://dba.stackexchange.com/questions/53336/mysqld-exe-memory-usage.
	(16) How to Fix the High CPU Usage of Mysqld.exe? - GeeksforGeeks. https://www.geeksforgeeks.org/how-to-fix-the-high-cpu-usage-of-mysqld-exe/.
	(17) MySQL :: MySQL 5.7 Reference Manual :: 4.5.4 mysqldump — A Database .... https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html.
	(18) die Verwendung von mysqldump :: blog.bartlweb. https://blog.bartlweb.net/2010/05/die-verwendung-von-mysqldump/.
	(19) So sichern und wiederherstellen Sie die MySQL -Datenbank mit MySQldump .... https://www.hostwinds.de/tutorials/backup-restore-mysql-databases-using-mysqldump.
	(20) MySQL :: MySQL 8.0 Reference Manual :: 6.5.5 mysqlimport — A Data .... https://dev.mysql.com/doc/refman/8.0/en/mysqlimport.html.
	(21) importing a csv file into mysql from the command line using mysqlimport .... https://stackoverflow.com/questions/33488885/importing-a-csv-file-into-mysql-from-the-command-line-using-mysqlimport-command.
	(22) How To Upload Data to MySQL tables using mysqlimport - The Geek Stuff. https://www.thegeekstuff.com/2008/10/import-and-upload-data-to-mysql-tables-using-mysqlimport/.
	(23) Display Database, Table, and Column Information - MySQL. https://dev.mysql.com/doc/refman/8.0/en/mysqlshow.html.
	(24) MySQL SHOW DATABASES: List All Databases in MySQL. https://www.mysqltutorial.org/mysql-administration/mysql-show-databases/.
	(25) MySQL :: MySQL 8.3 Reference Manual :: 15.7.7 SHOW Statements. https://dev.mysql.com/doc/en/show.html.
	(26) MySQL :: MySQL 8.0 Reference Manual :: 6.5.8 mysqlslap — A Load .... https://dev.mysql.com/doc/refman/8.0/en/mysqlslap.html.
	(27) mysql - mysqlslap access denied on windows8 - Stack Overflow. https://stackoverflow.com/questions/22218940/mysqlslap-access-denied-on-windows8.
	(28) MySQL :: MySQL 8.4 Reference Manual :: 6.5.7 mysqlslap — A Load .... https://dev.mysql.com/doc/refman/8.2/en/mysqlslap.html.
	(29) How To Measure MySQL Query Performance with mysqlslap. https://www.digitalocean.com/community/tutorials/how-to-measure-mysql-query-performance-with-mysqlslap.
	(30) mysql_install_db.exe - MariaDB Knowledge Base. https://mariadb.com/kb/en/mysql_install_dbexe/.
	(31) 4.4.2 mysql_install_db — Initialize MySQL Data Directory. https://dev.mysql.com/doc/refman/5.7/en/mysql-install-db.html.
	(32) MariaDB - mysql_install_db.exe - Runebook.dev. https://runebook.dev/zh/docs/mariadb/mysql_install_dbexe/index.
	(33) mysql_install_db.exe - MariaDB - W3cubDocs. https://docs.w3cub.com/mariadb/mysql_install_dbexe/.
	(34) MySQL :: Download MySQL Installer. https://dev.mysql.com/downloads/installer/.
	(35) Was ist MySQL? Eine einfache Erklärung - IONOS. https://www.ionos.de/digitalguide/server/knowhow/was-ist-mysql/.
	(36) undefined. https://dev.mysql.com/doc/refman/5.6/en/mysqlbinlog.html.
	(37) undefined. https://github.com/alibaba/canal.
	(38) undefined. https://github.com/monothorn/mysql-parser.
	(39) undefined. https://github.com/noplay/python-mysql-replication.
	
	(41) Mariabackup Overview - MariaDB Knowledge Base. https://mariadb.com/kb/en/mariabackup-overview/.
	(42) Options for mariabackup in MariaDB Community Server 10.2. https://mariadb.com/docs/server/ref/cs10.2/cli/mariabackup/.
	(43) Physical backup with myrocks_hotbackup - GitHub. https://github.com/facebook/mysql-5.6/wiki/Physical-backup-with-myrocks_hotbackup.
	(44) myrocks_hotbackup(1) - Linux manual page - man7.org. https://www.man7.org/linux/man-pages/man1/myrocks_hotbackup.1.html.
	(45) MariaDB Backup — MariaDB Documentation. https://mariadb.com/docs/server/data-operations/backups/community-server/mariadb-backup/.
	(46) MariaDB Backups Overview for SQL Server Users. https://mariadb.com/kb/en/mariadb-backups-overview-for-sql-server-users/.
	(47) Back Up and Restore MariaDB Databases From the Command line. https://www.linuxbabe.com/mariadb/how-to-back-up-mariadb-databases-from-the-command-line.
	(48) Getting Started with MyRocks - MariaDB Knowledge Base. https://mariadb.com/kb/en/getting-started-with-myrocks/.
	(49) Mariabackup - MariaDB Knowledge Base. https://mariadb.com/kb/en/mariabackup/.
	(50) Mariabackup Options - MariaDB Knowledge Base. https://mariadb.com/kb/en/mariabackup-options/.
	(51) undefined. https://github.com/facebook/wdt.
    