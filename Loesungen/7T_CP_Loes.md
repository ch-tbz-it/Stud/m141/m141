![](../x_res/tbz_logo.png)

# M141 Lösungen 7.Tag

## Testen 

1.  Auf welche Arten können Konfigurationsparameter definiert werden?

    - [ ] mit einem INSERT-Befehl

    - [ ] durch Eintrag auf der Kommandozeile

    - [ ] durch Eintrag in einer Konfigurationsdatei

    - [ ] durch Eintrag in einem Logfile
