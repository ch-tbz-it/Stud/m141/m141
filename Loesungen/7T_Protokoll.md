![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen  (7.Tag)

[TOC]

## 1 Login mit Test User

Nicht möglich. Warum?

```BATCH
c:>\..\mysql -u Reader -p
	ERROR 1045 (28000): Access denied for user 'Reader'@'localhost' 
```
* Test-User existiert (noch) nicht

## 2 Test User erstellen und Login testen

Zwei Test User mit Passwort erstellen:  (Hier `localhost` und `%`!!!)

```sql
CREATE USER 'Reader' IDENTIFIED BY '123!'; 
CREATE USER 'Contributor' IDENTIFIED BY '123!';
```

```BATCH
PS C:\..\> mysql -u Reader -p
	Enter password: ************
	Welcome to the MariaDB monitor.  Commands end with ; or \g.
	Your MariaDB connection id is 692
	Server version: 10.11.4-MariaDB-log mariadb.org binary distribution
	
	Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.
	
	Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> Show Databases;
	+--------------------+
	| Database           |
	+--------------------+
	| information_schema |
	+--------------------+
	1 row in set (0.001 sec)

MariaDB [(none)]> Exit



PS C:\Users\michael> mysql -u Contributor -p
Enter password: *****************
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 696
Server version: 10.11.4-MariaDB-log mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show Databases;
	+--------------------+
	| Database           |
	+--------------------+
	| information_schema |
	+--------------------+
	1 row in set (0.001 sec)

MariaDB [(none)]> EXIT

```

> MariaDB erstellt damit vier User, wobei die zwei Localhost-User keine PW haben! ° <br>
> überprüfen Sie das mit phpMyAdmin! --> Siehe Punkt 5! <br>
> Vorteil: Auf dem Localhost können Sie so ohne PW einloggen... <br>
> Für den produktiven Betrieb die zwei Localhost-User wieder löschen!!! <br>

## 3 Schema und Tabellen erstellen




```BATCH
PS C:\..\> mysql -u root -p
	Enter password: ************
```

```sql
DROP SCHEMA IF EXISTS `myTestDb` ;
CREATE SCHEMA IF NOT EXISTS `myTestDb` DEFAULT CHARACTER SET utf8mb4;
USE `myTestDb` ;

DROP TABLE IF EXISTS `Person` ;
DROP TABLE IF EXISTS `Adresse` ;

CREATE TABLE Person (
    Id INT,
    Vorname VARCHAR(255),
    Nachname VARCHAR(255),
    Email VARCHAR(255),
    AdresseId INT
);

CREATE TABLE Adresse (
    Id INT,
    Strasse VARCHAR(255),
    Hausnummer VARCHAR(10),
    PLZ VARCHAR(10),
    Stadt VARCHAR(255),
    Bundesstaat VARCHAR(10)
);
```

```SQL
show Tables;
	+--------------------+
	| Tables_in_mytestdb |
	+--------------------+
	| adresse            |
	| person             |
	+--------------------+
	2 rows in set (0.001 sec)
```

Die beiden Tabellen haben keine PK und Indizes definiert.
Erwartetes Resultat: *Schlechte Performance*!


## 4 Mit Admin-User "Bulkload" Tabellen mit Daten befüllen (je 400'000 Datensätze)

```BATCH
PS C:\xampp\mysql\data> dir *.csv

    Directory: C:\xampp\mysql\data

	Mode                 LastWriteTime         Length Name
	----                 -------------         ------ ----
	-a---          27.03.2024    20:40       20237114 adresse.csv
	-a---          27.03.2024    20:40       19665449 person.csv

```


```sql
use mytestdb;
select * from adresse;
	Empty set (0.001 sec)


LOAD DATA INFILE './person.csv' 
INTO TABLE Person
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;
	Query OK, 400000 rows affected (3.734 sec)
	Records: 400000  Deleted: 0  Skipped: 0  Warnings: 0
	
	
select * from person WHERE ID=2000;
	+------+---------+----------+-------------+-----------+
	| Id   | Vorname | Nachname | Email       | AdresseId |
	+------+---------+----------+-------------+-----------+
	| 2000 | Steve   | Brock    | jal@padi.tl |      2000 |
	+------+---------+----------+-------------+-----------+
	1 row in set (0.160 sec)	
	

LOAD DATA INFILE './adresse.csv' 
INTO TABLE Adresse
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS;

	Query OK, 400003 rows affected (3.536 sec)
	Records: 400003  Deleted: 0  Skipped: 0  Warnings: 0
	
	
select * from adresse WHERE ID=2000;
	+------+------------------+------------+-------+---------+-------------+
	| Id   | Strasse          | Hausnummer | PLZ   | Stadt   | Bundesstaat |
	+------+------------------+------------+-------+---------+-------------+
	| 2000 | Nimbum Boulevard | 38         | 93312 | Kuheago | VA          |
	+------+------------------+------------+-------+---------+-------------+
	1 row in set (0.157 sec)
```

## 5 Mit Admin-User Berechtigungen für die User konfigurieren


```sql
-- Rollen erstellen und berechtigen
CREATE ROLE 'RoleReader', 'RoleContributor';
GRANT SELECT ON myTestDb.* TO 'RoleReader';
GRANT SELECT, INSERT, UPDATE, DELETE ON myTestDb.* TO 'RoleContributor';

-- Rollen den Benutzern zuweisen
GRANT 'RoleReader' TO 'Reader'@'localhost';
GRANT 'RoleContributor' TO 'Contributor'@'localhost';

-- Berechtigungen neu laden damit GRANTS wirksam wird
FLUSH PRIVILEGES;
```

Grants überprüfen: 

```sql
SHOW GRANTS FOR 'Reader';
+-------------------------------------------------------------------------------------------------------+
| Grants for Reader@%                                                                                   |
+-------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `Reader`@`%` IDENTIFIED BY PASSWORD '*EA431C49EA44F36FCBC7D4D3762D0258E4AA6211' |
+-------------------------------------------------------------------------------------------------------+
	1 row in set (0.000 sec)

SHOW GRANTS FOR 'Contributor';
+------------------------------------------------------------------------------------------------------------+
| Grants for Contributor@%                                                                                   |
+------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `Contributor`@`%` IDENTIFIED BY PASSWORD '*73F5444B12A5B161CD212Buse 6B1D21281D0D5D22BC' |
+------------------------------------------------------------------------------------------------------------+
	1 row in set (0.000 sec)
```

![](./Benutzer_Rollen.png)

![](./RoleContributor.png)

![](./RoleReader.png)

## 6 Mit Test-User einloggen und die Berechtigungen testen

Aufgabe mit zwei zusätzlichen Klienten testen: 

![](./3Clients.png)

### Reader 

Berechtigungen bei Test User `Reader` überprüfen: 

```sql
show Grants;
+--------------------------------------------+
| Grants for Reader@localhost                |
+--------------------------------------------+
| GRANT `RoleReader` TO `Reader`@`localhost` |
| GRANT USAGE ON *.* TO `Reader`@`localhost` |
+--------------------------------------------+
	2 rows in set (0.000 sec)

SET ROLE RoleReader;
	Query OK, 0 rows affected (0.000 sec)

show Grants;
+----------------------------------------------+
| Grants for Reader@localhost                  |
+----------------------------------------------+
| GRANT `RoleReader` TO `Reader`@`localhost`   |
| GRANT USAGE ON *.* TO `Reader`@`localhost`   |
| GRANT USAGE ON *.* TO `RoleReader`           |
| GRANT SELECT ON `mytestdb`.* TO `RoleReader` |
+----------------------------------------------+
	4 rows in set (0.000 sec)

-- SELECT
use mytestdb;
select * FROM adresse JOIN person WHERE person.AdresseID = Adresse.ID AND Adresse.ID = 2000;
+------+------------------+------------+-------+---------+-------------+------+---------+----------+-------------+-----------+
| Id   | Strasse          | Hausnummer | PLZ   | Stadt   | Bundesstaat | Id   | Vorname | Nachname | Email       | AdresseId |
+------+------------------+------------+-------+---------+-------------+------+---------+----------+-------------+-----------+
| 2000 | Nimbum Boulevard | 38         | 93312 | Kuheago | VA          | 2000 | Steve   | Brock    | jal@padi.tl |      2000 |
+------+------------------+------------+-------+---------+-------------+------+---------+----------+-------------+-----------+
	1 row in set (0.291 sec)

--UPDATE
UPDATE adresse SET Hausnummer=39 WHERE Id=2000;
	ERROR 1142 (42000): UPDATE command denied to user 'Reader'@'localhost' for table `mytestdb`.`adresse`
```

### Contributor 

Berechtigungen bei Test User `Contributor` überprüfen: 

```SQL
 SHOW GRANTS;
+--------------------------------------------------------------------------------------------------------------------+
| Grants for Contributor@localhost                                                                                   |
+--------------------------------------------------------------------------------------------------------------------+
| GRANT `RoleContributor` TO `Contributor`@`localhost`                                                               |
| GRANT USAGE ON *.* TO `Contributor`@`localhost` IDENTIFIED BY PASSWORD '*A4B6157319038724E3560894F7F932C8886EBFCF' |
+--------------------------------------------------------------------------------------------------------------------+
	2 rows in set (0.000 sec)

SET ROLE RoleContributor;
	Query OK, 0 rows affected (0.000 sec)

SHOW GRANTS;
+--------------------------------------------------------------------------------------------------------------------+
| Grants for Contributor@localhost                                                                                   |
+--------------------------------------------------------------------------------------------------------------------+
| GRANT `RoleContributor` TO `Contributor`@`localhost`                                                               |
| GRANT USAGE ON *.* TO `Contributor`@`localhost` IDENTIFIED BY PASSWORD '*A4B6157319038724E3560894F7F932C8886EBFCF' |
| GRANT USAGE ON *.* TO `RoleContributor`                                                                            |
| GRANT SELECT, INSERT, UPDATE, DELETE ON `mytestdb`.* TO `RoleContributor`                                          |
+--------------------------------------------------------------------------------------------------------------------+
	4 rows in set (0.000 sec)

-- SELECT
select * FROM adresse JOIN person WHERE person.AdresseID = Adresse.ID AND Adresse.ID = 100;
+------+--------------+------------+-------+----------+-------------+------+---------+----------+----------------+-----------+
| Id   | Strasse      | Hausnummer | PLZ   | Stadt    | Bundesstaat | Id   | Vorname | Nachname | Email          | AdresseId |
+------+--------------+------------+-------+----------+-------------+------+---------+----------+----------------+-----------+
|  100 | Dimce Street | 13         | 56453 | Onocinut | NM          |  100 | Tony    | Logan    | fici@tolwav.tm |       100 |
+------+--------------+------------+-------+----------+-------------+------+---------+----------+----------------+-----------+
	1 row in set (0.305 sec)

--UPDATE
UPDATE adresse SET Hausnummer=14 WHERE Id=100;
	Query OK, 1 row affected (0.179 sec)
	Rows matched: 1  Changed: 1  Warnings: 0

select * FROM adresse JOIN person WHERE person.AdresseID = Adresse.ID AND Adresse.ID = 100;
+------+--------------+------------+-------+----------+-------------+------+---------+----------+----------------+-----------+
| Id   | Strasse      | Hausnummer | PLZ   | Stadt    | Bundesstaat | Id   | Vorname | Nachname | Email          | AdresseId |
+------+--------------+------------+-------+----------+-------------+------+---------+----------+----------------+-----------+
|  100 | Dimce Street | 14         | 56453 | Onocinut | NM          |  100 | Tony    | Logan    | fici@tolwav.tm |       100 |
+------+--------------+------------+-------+----------+-------------+------+---------+----------+----------------+-----------+
	1 row in set (0.303 sec)

```



## 7 Performance Test ohne Index

Test-Query schreiben, z.B.

```sql
DESCRIBE Adresse;
[EXPLAIN] SELECT * FROM Person p
    INNER JOIN Adresse a ON a.Id = p.AdresseId
    WHERE p.id = 2569;

+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
| Id   | Vorname | Nachname | Email             | AdresseId | Id   | Strasse      | Hausnummer | PLZ   | Stadt  | Bundesstaat |
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
| 2569 | Sam     | Warren   | itcipuw@ahocar.it |      2569 | 2569 | Vube Terrace | 03         | 30123 | Mohuki | NM          |
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
	1 row in set (9.464 sec)
```

[Manual DESCRIBE](https://mariadb.com/kb/en/describe/)

[Manual EXPLAIN](https://mariadb.com/kb/en/explain/)


**Ergebnis:** 

Sehr schlechte Performance:  **9s**! (Raspberry Pi 4 mit MariaDB 10.3)


## 8 Index erstellen nur auf eine Tabelle

```sql
DROP INDEX idx_AddresseId ON Person ;
CREATE INDEX idx_AddresseId ON Person (AdresseId);
```

## 9 Test gem. Nr 7 wiederholen


```SQL
DESCRIBE Person;
[EXPLAIN] SELECT * FROM Person p
    INNER JOIN Adresse a ON a.Id = p.AdresseId
    WHERE p.id = 2569;
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
| Id   | Vorname | Nachname | Email             | AdresseId | Id   | Strasse      | Hausnummer | PLZ   | Stadt  | Bundesstaat |
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
| 2569 | Sam     | Warren   | itcipuw@ahocar.it |      2569 | 2569 | Vube Terrace | 03         | 30123 | Mohuki | NM          |
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
	1 row in set (1.189 sec)
```

Duration hat sich auch verändert: **ca. 8x schneller!**

## 10 Index erstellen auf die andere Tabelle

```sql
DROP INDEX idx_Id ON Adresse;
CREATE INDEX idx_Id ON Adresse (Id);
```

## 11 Test gem. Nr 7 wiederholen

```SQL
DESCRIBE Adresse;
[EXPLAIN] SELECT * FROM Person p
    INNER JOIN Adresse a ON a.Id = p.AdresseId
    WHERE p.id = 2569;
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
| Id   | Vorname | Nachname | Email             | AdresseId | Id   | Strasse      | Hausnummer | PLZ   | Stadt  | Bundesstaat |
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
| 2569 | Sam     | Warren   | itcipuw@ahocar.it |      2569 | 2569 | Vube Terrace | 03         | 30123 | Mohuki | NM          |
+------+---------+----------+-------------------+-----------+------+--------------+------------+-------+--------+-------------+
	1 row in set (0.159 sec)
```

Duration hat sich auch verändert: **ca. 60x schneller!**

## 12 Datenkonsistenz testen

Dateninkonsistenz testen: 
1) Primary Key in der Tabelle Adresse setzen

```SQL
ALTER TABLE adresse ADD PRIMARY KEY (ID);
	Records: 0  Duplicates: 3  Warnings: 0
```

--> In der Tabelle Adresse kommmen mehrere Datensätze doppelt vor (insgesamt 3 Datensätze)


2) Fehler erscheint, was ist zu tun?

Doppelte Datensätze finden:

```sql
SELECT Id FROM Adresse
GROUP BY Id
HAVING COUNT(Id) > 1;

+--------+
| Id     |
+--------+
|  44738 |
| 133344 |
| 234426 |
+--------+
	3 rows in set (0.128 sec)

select * from Adresse WHERE ID=44738;
+-------+------------+------------+-------+---------+-------------+
| Id    | Strasse    | Hausnummer | PLZ   | Stadt   | Bundesstaat |
+-------+------------+------------+-------+---------+-------------+
| 44738 | Weodu Path | 09         | 20203 | Mobimha | WI          |
| 44738 | Weodu Path | 09         | 20203 | Mobimha | WI          |
+-------+------------+------------+-------+---------+-------------+
	2 rows in set (0.001 sec)
```

3) Fehler beheben bzw. redundanten Datensatz löschen


```SQL
DELETE FROM Adresse WHERE ID=44738  Limit 1;
DELETE FROM Adresse WHERE ID=133344 Limit 1;
DELETE FROM Adresse WHERE ID=234426 Limit 1;
```

4) Primary Key in der Tabelle Adresse nochmals versuchen zu setzen

```SQL
ALTER TABLE adresse ADD PRIMARY KEY (ID);
	Query OK, 0 rows affected (3.205 sec)
	Records: 0  Duplicates: 0  Warnings: 0

ALTER TABLE Person ADD PRIMARY KEY (ID);
	Query OK, 0 rows affected (3.256 sec)
	Records: 0  Duplicates: 0  Warnings: 0

```

## 13 Schlussbilanz

...

![Note](../x_res/Hinweis.png) Was hier nicht getestet wurde, aber bzgl. Konsistenz wichtig wäre: **Constraint auf die Beziehung Person-Adresse**. Beim Setzen des Contraint müssten evtl. falsche FK-Wert (Person.AdresseID) angepasst werden!
