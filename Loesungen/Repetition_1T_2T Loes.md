![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


## Lösungen Repetition zu 1. & 2.Tag

|                                        | **Werkzeuge**            |                   |                    |                 |           |                        |                        |            |
|----------------------------------------|--------------------------|-------------------|--------------------|-----------------|-----------|------------------------|------------------------|------------|
| **Administrationsaufgaben**            | Dienst-Manager (Windows) | CMD- / WPS-Fenster | XAMPP ControlPanel | mysql (Konsole) | mysqldump | Task-Manager (Windows) | phpMyAdmin / Workbench | Texteditor |
| 1) ein Backup erstellen                |                          |                   |                    |                 | X         |                        | X                      |            |
| 2) DB aus Backup wiederherstellen      |                          |                   |                    | X               |           |                        | X                      |            |
| 3) DB-Benutzer erstellen u. verwalten  |                          |                   |                    | X               |           |                        | X                      |            |
| 4) DB-Server starten                   | X\*                      | X                 | X                  |                 |           | X\*                    |                        |            |
| 5) DB-Server stoppen                   | X\*                      | X                 | X                  |                 |           | X\*                    |                        |            |
| 6) Daten in Tabellen kontrollieren     |                          |                   |                    | X               |           |                        | X                      |            |
| 7) SQL-Skript anpassen oder erstellen  |                          |                   |                    |                 |           |                        | X                      | X          |
| 8) eine neue Datenbank erstellen       |                          |                   |                    | X               |           |                        | X                      |            |
| 9) Konfigurationsdateien anschauen     |                          | X                 | X                | (X)             |           |                        | (X)                    | X          |
| 10) prüfen, ob der Server läuft        | X                        | (X)               | X                  | (X)             |           | X                      | (X)                    |            |
| 11) Server-Installation testen         |                          | X                 | X                  | X               |           |                        | X                      |            |
| 12) Server-Status abfragen             |                          |                   |                    | X               |           |                        | X                      |            |
| 13) SQL-Befehle ausführen              |                          |                   |                    | X               |           |                        | X                      |            |
| 14) SQL-Skript ausführen               |                          |                   |                    | X               |           |                        | X                      |            |
| 15) Tabellen erstellen und verwalten   |                          |                   |                    | X               |           |                        | X                      | (X)        |
| 16) Daten in Tabellen eintragen        |                          |                   |                    | X               |           |                        | X                      | (X)        |
| 17) Welches ist ein DB-Client?         |                          |                   |                    | X               |           |                        | X                      |            |

\* unter Vorbehalt, ( ) nicht direkt
