![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen

## Lösung SQL-Befehle


| **Tätigkeit**                         | **SQL-Befehl**                                               | **Grp** | **![Achtung](../x_res/caution.png)** |
|---------------------------------------|--------------------------------------------------------------|---------|-----------------------------------------------------|
| 1) alle Daten einer Tabelle anzeigen  | `SELECT \* FROM 'tabelle';`                                    | DQL/DRL |                                                     |
| 2) Datenbank auswählen                | `USE 'db';`                                                    |   -   |                                                     |
| 3) eine neue Datenbank erstellen      | `CREATE DATABASE 'db';`                                        | DDL    |                                                     |
| 4) eine neue Tabelle erstellen        | `CREATE TABLE ’tabelle’ (attrib1 typ, attrib2 typ);`           | DDL    |                                                     |
| 5) eine Tabelle löschen               | `DROP TABLE ’tabelle’;`                                        | DDL    | !!!                                                 |
| 6) Tabellenstruktur kontrollieren     | `DESCRIBE ’tabelle’;`                                          | DDL     |                                                     |
| 7) Datenbanken anzeigen               | `SHOW DATABASES;`                                              | DQL      |                                                     |
| 8) Tabellen einer DB anzeigen         | `SHOW TABLES;`                                                 | DQL     |                                                     |
| 9) Daten in eine Tabelle eintragen    | `INSERT INTO 'tbl’ (attrib1, attrib2)  VALUES (wert1, wert2);` | DML     |                                                     |
| 10) Daten in einer Tabelle ändern     | `UPDATE ’tbl’ SET attrib=attrib+wert WHERE condition;`         | DML     |                                                     |
| 11) Daten in einer Tabelle löschen    | `DELETE FROM ’tbl’ WHERE condition;`                           | DML     | !!!                                                 |
| 12) Spalte in einer Tabelle löschen   | `ALTER ’tbl’ DROP COLUMN attrib;`                              | DDL     | (!)                                                 |
