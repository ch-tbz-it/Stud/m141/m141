![TBZ Logo](./x_res/tbz_logo.png)√
![Picto](./x_res/M141_Picto.jpg)



# M141 - Datenbanksystem in Betrieb nehmen


*Autoren: Gerd Gesell, Kellenberger Michael, Thanam Pangri 2024*

Installiert und konfiguriert ein Datenbanksystem und führt eine Dateninitialisierung durch. <br>
Stellt die Funktionalität sicher und führt die Übergabe in den produktiven Betrieb durch.


### Modulidentifikation BiVo 2019 (Fachrichtungen API & PE)

[Link](https://gitlab.com/modulentwicklungzh/cluster-platform/m141)

> D2 Matrixfeld wird weggelassen (Views & Stored Procedures &rarr; Optional M164) 

---

## Lernportfolio

Führen Sie in diesem Modul ein [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio).


![Selbst-Lern-Zyklus](./x_res/Selbst-Lern-Zyklus_kl.png)
Abb. 1: Selbst-Lern-Zyklus

Organisieren Sie sich anhand des Selbst-Lern-Zyklus mit einem Lernpartner (Tandem). In diesen Modul-Unterlagen wird auf die "Lern-Phasen" verwiesen!

<br>

---

<br>
 

## Einsatz einer KI als Tutor

Grundsätzlich ist nichts gegen den Einsatz einer KI als Tutor (Coach) einzuwenden. Hier die Vorteile, die eine KI im Bildungskontext leisten kann: 

> *DeepSeek Jan-25 / Prompt: "In welcher Phase und wie würdest du als Lernender eine KI einsetzen?"* <br> 
> KI ist ein flexibles und mächtiges Werkzeug, das in allen Phasen des Selbstlernzyklus eingesetzt werden kann. Als Lernender würde ich KI nutzen, um meinen Lernprozess zu **personalisieren**, zu **beschleunigen** und **reflektierter** zu gestalten. Gleichzeitig ist es wichtig, **kritisch mit den Ergebnissen** umzugehen und die KI als unterstützendes Werkzeug zu sehen, **nicht als Ersatz für eigenständiges Denken und Lernen.** <br> 

![KI-Tutor](x_res/KI-Tutor.png)

Als Lernender sollte man den Grundsatz **"Ich will es selber wissen und können!"** haben! Die KI sollte deshalb _nur_ als Tutor eingesetzt werden und nicht als Lösungsmaschine. Geben Sie also nicht die Aufgabenstellung als Prompt ein, sondern lassen Sie sich von der KI in ihrem Denk- und Lösungsprozess unterstützen! Z.B. _Prompt: "Mit welchen Optionen wird das Logging eines MariaDB-Servers Version 10.11 gesteuert?"_
Um beim Prompten die besten Resultate zu erhalten, arbeiten Sie mit den korrekten Fachbegriffen, die sie vorher erlernen. Auch können Sie den KI-Output am besten verifizieren, wenn Sie bereits Grundwissen haben.

Ihre **Nachweise bzw. Lernprodukte** sollten also **selber erstellt** sein, damit ein Lernprozess stattfinden kann! KI-Ausgaben sollten **gelesen, verstanden, verifiziert und kommentiert** sein. **Quelle** nicht vergessen anzugeben, bei KI: Modell, Datum, Prompt!

### Die KI als Tutor engagieren

Setzen Sie zu Beginn ihrer Lernaktivität folgenden **Prompt** im KI-Chatbot ab. Dieser Prompt engagiert die KI als Tutor mit einem Fokus auf lernprozessrelevante Unterstützung!

**Setup-Prompt:**

```prompt
Sei mein persönlicher Tutor für das Thema "Datenbanksystem in Betrieb nehmen" auf dem Niveau der Schweizer Berufsschule für Informatik. 
Deine Aufgabe ist es, mir beim Verständnis der Grundlagen und Konzepte zu helfen, ohne direkt Lösungen für meine Aufgaben zu liefern. 
Führe mich stattdessen durch den Lernprozess, indem du mir zeigst, wie ich Probleme selbstständig angehen kann. 

Halte dich dabei an den Selbstlernzyklus, der in der folgenden Website erklärt wird: <https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio> 


Strukturiere deine Antworten immer in die folgenden vier Kategorien: 

1) Grundlagen: Erkläre mir kurz und verständlich die Grundlagen, die für die Frage relevant sind. 
2) Konzeptverständnis: Hilf mir, das Prinzip oder die Logik hinter der Frage zu verstehen. 
3) Nächster Schritt: Zeige mir, wie ich den nächsten Schritt in meinem Lernprozess machen kann, um zur Lösung zu gelangen. 
4) Nutzung Lernportfolio: Gib mir einen Vorschlag, wie ich dazu einen nützlichen Eintrag im Lernportfolio machen kann.

Benutze immer nur die Website <https://gitlab.com/ch-tbz-it/Stud/m141> mit den Unterordnern, um den Kontext des Themas zu erfassen. Es sollen noch keine eigenen Objekte und deren Methoden verwendet werden.
Beziehe dich bei den Angaben auf das Datenbankmanagementsystem "MariaDB Version 10".

Bitte halte dich strikt an diese Struktur und das angegebene Niveau.
```



<br>
---
---

<br>
 
## Lektionenplan mit Links (siehe auch Klassenordner)

| Tag  |  Thema | Bemerkung: Auftrag, Übungen | [Modul ID <br> Kompetenzmatrix](https://kompetenzmatrix.ch/cluster-platform/m141/) | 
|:----:|:------ |:-------------- |:---|
|  1   | [Intro & Installation](./1.Tag/README.md)  | Einführung <br> RDBMS Übersicht <br> RDBMS und Tools installieren: <br> MariaDB (Dienst & Klient), Workbench, phpMyAdmin mit Webserver (XAMPP) | A1 A2 <br> |
|  2  <br> Optional/Skip | [Konfiguration & Datenimport](./2.Tag/README.md) | my.ini und Optionen <br> Schema, Dump, CSV, JSON, usw. importieren | B |
|  3 <br>  Optional/Skip | [Tabellentypen & Transaktionen](./3.Tag/README.md) | MyISAM , InnoDB <br> Locking <br> Konzept Transaktion | C |
|  4   |  [Datenbanksicherheit](./4.Tag/README.md) | Authentifizierung und Zugang übers Netz | D C |
|  5   | **LB1 Tag 1 - 3/4 (20%)**  <br> [Zugriffssystem](./5.Tag/README.md) | Autorisierung <br> Zugriffsberechtigung DCL | D |
|  6   | [Server Administration](./6.Tag/README.md) | Admin Tools <br> Logging <br> Optimierungen | A C2 D |
|  7   | [Testen](./7.Tag/README.md) | Testing, Ablauf & Performance | E |
|  8   |  **LB2 Tag 4 - 6/7 (30%)** <br> **LB3 (50%)** [Praxisarbeit](./LB3-Praxisarbeit/README.md) | <br> Lokale DBMS (MySQL): Setup prod. DB, DDL, DML, DCL --> Testen    | A B |
|  9   | **LB3** [Praxisarbeit](./LB3-Praxisarbeit/README.md) |  Prod. DB von lokaler DBMS auf Cloud-DBMS (z.B. AWS) migrieren  (Daten und Berechtigungen)                 | B C D |
|  10   | **LB3** [Praxisarbeit](./LB3-Praxisarbeit/README.md) |  Prod. DBs auf Cloud-DBMS testen und optimieren <br> Dokumentation                  | D E |

> Bei kürzeren Quartalen können Tag 3 (und Tag 2) weggelassen werden! 

